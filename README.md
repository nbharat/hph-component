This project is the component library of HPH IT.

# Available Component

In the project, you can use:

#### [`Core`](#Core)
#### [`Colors`](#Colors)
#### [`Icons`](#Icons)
#### [`GlobalStyle`](#GlobalStyle)

## Installation
`npm i hph-component styled-components`

---

### Import
```javascript
import {Core, GlobalStyle, Colors, Icons} from 'hph-component';
```
---
### Core<a name="Core"></a>
The core component is wrapped in Core.


[`Theme`](#Theme) [`Drawer`](#Drawer) [`DrawerItem`](#DrawerItem) [`TitleBar`](#TitleBar)
[`SmartText`](#SmartText) [`ToggleSwitch`](#ToggleSwitch) [`Checkbox`](#Checkbox)
[`Radio`](#Radio) [`RadioGroup`](#RadioGroup) [`MessageBar`](#MessageBar) 
[`Dialog`](#Dialog) [`DateRangePicker`](#DateRangePicker)

#### Theme<a name="Theme"></a>
Use styled-components theme provider, wrap all component in ThemeProvider <br>
Specific colors can be called by importing {Colors} from Theme.
```javascript
<ThemeProvider theme={Core.Theme.Primary}>
    ...
</ThemeProvider>
```

#### Drawer<a name="Drawer"></a>
```javascript
<Core.Drawer
    items={drawerItem}                  //object, jsx element
    zoom={isZoom}                       //boolean control zoom
    left={isLeft}                       //boolean left drawer or right drawer
    handle={label}                      //label of current page
/>
```

#### Drawer Item<a name="DrawerItem"></a>
```javascript
<Core.DrawerItem 
    content={content}                   //string, display in drawer
    onClick={onDrawerItemClick}         //function, execute when item click
/>
```
#### TitleBar<a name="TitleBar"></a>
```javascript
<Core.TitleBar
    project={project}                   //string, project
    system={system}                     //string, system
    version={version}                   //optional string, app version 
/>
```
#### SmartText<a name="SmartText"></a>
```typescript jsx
interface ValueType {
    value: string;
    notUseIcon?: boolean;
}

const options: SmartTextOptions<ValueType>[] = [
    {label: 'option1', value: {value: 'option1Value', notUseIcon: true}},
    {label: 'option2', value: {value: 'option2Value'}},
    {label: 'option3', value: {value: 'option3Value'}},
    {label: 'option3', value: {value: 'option3Value'}},
];

<Core.SmartText
    options={options}               //OptionsType, dropdown options
    label={'label'}                 //string, smart text label
    onValueChanged={(v) => {}}      //function, handle value change
    multiple={false}                //boolean, is multiple input 
    width={200}                     //number, width
    freeText={false}                //boolean, is free text allowed
    useIcon={false}
    labelStyle={{"fontSize": "14px"}}
    input={''}
    value={null}
    onInputChanged={inputChangeCallBack}

/>
```
###### Props
| Name              | Type                            | Default       | Description                                                         |
| ----------------- | ------------------------------- | ------------- | ------------------------------------------------------------------- |
| options           | SmartTextOptions[]              |               |                                                                     |
| label             | string                          |               | Can be empty string                                                 |
| value             | SmartTextOptions| []            | null          | Value, control the selection                                        |
| input             | string (optional)               | Empty String  |                                                                     |
| onValueChanged    | func                            |               |                                                                     |
| onInputChanged    | func                            |               |                                                                     |
| multiple          | bool (optional)                 | false         | If `true`, multiple options can be chose.                           |
| width             | string (optional)               | 100%          | Width in pixels.                                                    |
| freeText          | bool (optional)                 | false         | If `true`, no dropdown displayed.                                   |
| isLoading         | bool (optional)                 | false         | If `true`, loading icon appears.                                    |
| useIcon           | bool (optional)                 | true          | If `false`, no icon displayed. Usually used with `freeText={true}`. |
| textTransform     | Enum (optional)                 | `none`        | `none`, `capitalize`, `lowercase`, `uppercase`                      |
| backgroundMode    | bool (optional)                 | false         | If `true`, background styling applied.                              |
| disabled          | bool (optional)                 | false         | If `true`, disabled styling applied. Input field unable to edit.    |
| errorMessage      | string (optional)               |               | React.FocusEvent<HTMLInputElement>                                  |
| onInputBlur       | func (optional)                 |               | If not empty, apply invalid styling.                                |
| labelStyle        | React.CSSProperties (optional)  |               |                                                                     |
| inputWrapperStyle | React.CSSProperties (optional)  |               |                                                                     |
| styledInputStyle  | React.CSSProperties (optional)  |               |                                                                     |
| errorMessageStyle | React.CSSProperties (optional)  |               |                                                                     |
| iconStyle         | React.CSSProperties (optional)  |               |                                                                     |

#### ToggleSwitch<a name="ToggleSwitch"></a>
```javascript
<Core.ToggleSwitch
    optionLabels={["On", "Off"]}
    toggled={true}
    onToggle={() => void(0)}
    disabled
/>
```
###### Props
| Name          | Type                      | Default       | Description                             |
| ------------- | ------------------------- | ------------- | --------------------------------------- |
| optionLabels  | string array (optional)   | ["On", "Off"] | Width of component in pixel. <br/> Can be empty but required to adjust height and width. |
| toggled       | bool (optional)           | false         | If `true`, the checkbox will be checked.|
| disabled      | bool (optional)           | false         | If `true`, the checkbox will be disabled.|
| onToggle      | function (optional)       |               |                                          |
| small         | bool   (optional)         | true          | Size of the switch.                      |

#### Checkbox<a name="Checkbox"></a>
```javascript
const labelStyle = {
    style: {
        color: "red"
    }
};
<Core.Checkbox
    id={"001"}
    label={"Checkbox"}
    checked={false}
    onChange={handleChange}
    disabled
    wrapperStyle={{style: { margin: "5px" }}}
    inputStyle={}
    labelStyle={labelStyle}
/>
```
###### Props
| Name          | Type              | Default  | Description                             |
| ------------- | ----------------- | -------- | --------------------------------------  |
| id            | string (required) |          | The id of the of the `input` element    |
| label         | string (optional) |          | Label text                              |
| checked       | bool (optional)   | false    | If `true`, the component is checked.    |
| onChange      | func              |          | Callback fired when the state is changed. <br /><br /> `function(event: object) => void`<br />event: The event source of the callback. You can pull out the new checked state by accessing `event.target.checked` (boolean). 
| disabled      | bool (optional)   | false    | If `true`, the checkbox will be disabled. |
| wrapperStyle  | object (optional) |          | HTML Attributes applied to the wrapper of Checkbox and label. |
| inputStyle    | object (optional) |          | HTML Attributes applied to the `input` element.|
| labelStyle    | object (optional) |          | HTML Attributes applied to the label text. |

#### Radio<a name="Radio"></a>
Radio buttons are normally presented in radio groups.<br />
Radio group must have share the same name `name={"radio-button-demo"}` to be treated as a group. 
Only one radio button in a group can be selected at the same time.


The `value` attribute defines the unique value associated with each radio button. The value is not shown to the user,
```javascript
<div>
    <Core.Radio
        id={"0005"}
        label={"Apple"}
        value={"apple"}
        name={"radio-button-demo"}
        checked={state.selectedRadioValue === "apple"}
        onChange={handleRadioButtonChange}
    />
    <Core.Radio
        id={"0006"}
        label={"Orange"}                          
        value={"orange"}
        name={"radio-button-demo"}
        checked={state.selectedRadioValue === "orange"}
        onChange={handleRadioButtonChange}
    />
</div>
```
###### Props
| Name          | Type              | Default  | Description                             |
| ------------- | ----------------- | -------- | --------------------------------------  |
| id            | string (required) |          | The id of the of the `input` element    |
| label         | string (optional) |          | Label text                              |
| value         | string (required) |          | The value of the component.             |
| name          | string (optional) |          | Name attribute of the `input` element.  |
| checked       | bool (optional)   | false    | If `true`, the component is checked.    |
| onChange      | func              |          | Callback fired when the state is changed. <br /><br /> `function(event: object) => void`<br /> event: The event source of the callback. You can pull out the new value by accessing `event.target.value` (string). You can pull out the new checked state by accessing `event.target.checked` (boolean).
| size          | number            | 16       | The size of the radio in px             |
| disabled      | bool (optional)   | false    | If `true`, the radio will be disabled.  |

#### RadioGroup<a name="Radio"></a>
When using RadioGroup, `name` `onChange` attribute is not required for Radio component.
```javascript
 <RadioGroup
    selectedValue={state.buttonGroupValue}
    onChange={onClickRadioButton}
>
    <Radio id={"105"} label={"Kiwi"} value={"kiwi"} />
    <Radio id={"106"} label={"Lychee"} value={"lychee"} />
    <Radio id={"107"} label={"Mango"} value={"mango"} />
</RadioGroup>
```
###### Props
| Name              | Type                            | Default  | Description                          |
| ----------------- | ------------------------------- | -------- | ------------------------------------ |
| name              | string (optional)               |          |                                      |
| label             | string (optional)               |          | Label of radio button group.         |
| selectedValue     | string                          |          | Value of the selected radio button.  |
| onChange          | func                            |          | Callback fired when a radio button is selected.<br /><br />  `function(event: object) => void`<br /> event: The event source of the callback. You can pull out the new value by accessing `event.target.value` (string).
| children          | ReactElement[]                  |          | The content of the component         |
| size              | number (optional)               |          | The size of the radio in px          |
| errorMessage      | string (optional)               |          | Error message                        |
| labelStyle        | React.CSSProperties (optional)  |          |                                      |
| errorMessageStyle | React.CSSProperties (optional)  |          |                                      |

#### Dialog<a name="Dialog"></a>
```javascript
<Core.Dialog
    width={270}
    height={100}
    PositionLeft={172}
/>
```
###### Props
| Name            | Type              | Default  | Description                              |
| --------------- | ----------------- | -------- | ---------------------------------------  |
| children        | ReactElement[]    |          | The content of the component             |
| width           | number (required) |          | width for panel content                  |
| height          | number (required) |          | height for panel content                 |
| padding         | number (optional) | 0        | padding for panel content                |
| dialogPosition | number (optional) | 40       | right position of arrow                  |
| PositionLeft    | number (optional) | 196      | left position of the whole dialog panel |


#### DateRangePicker<a name="DateRangePicker"></a>
```javascript
<Core.DateRangePicker
    numberOfMonths = {2}
    horizontalMonthPadding = {15}
    enableOutsideDays = {false}
    minimumNights = {-100000} //magic to select endDate before startDate when this value is negative, see "react-dates" github issue #761
    minSelectDate = {7}
    maxSelectDate={31}
    daySize = {26}
    onDatesChange={({ startDate, endDate }: any) => {
        if (dateRange.startDate !== startDate) {
            endDate = null;
        }
        if (startDate !== null && endDate !== null) {
            if (startDate.diff(endDate)>0) {
                const tempDate = startDate;
                startDate = endDate;
                endDate = tempDate;
            }
        }
        setDateRange({startDate, endDate});
    }}
    defaultFocusedInput={"startDate"}
    startDate={moment()}
    endDate={moment().add(14, 'd')}/>
```
###### Props
| Name                   | Type                                    | Default | Description                                                 |
| ---------------------- | --------------------------------------- | ------- | ----------------------------------------------------------  |
| numberOfMonths         | number  (required)                      |         | number of showing months                                    |
| horizontalMonthPadding | number  (required)                      |         | horizontal padding                                          |
| enableOutsideDays      | boolean (required)                      |         | enable outside days of the month                            |
| minimumNights          | number  (optional)                      | 0       | select endDate before startDate when this value is negative |
| minSelectDate          | number  (optional)                      | 0       | minimum date can be selected                                |
| maxSelectDate          | number  (optional)                      | 0       | maximum date can be selected                                |
| daySize                | number  (optional)                      | 26      | padding of calendar day                                     |
| verticalBorderSpacing  | number  (optional)                      | 0       | vertical margin of week                                     |
| onDatesChange          | func    (required)                      |         | function on dates change of controlled component            |
| defaultFocusedInput    | "startDate", "endDate", null (required) |         | focus on startDate or endDate when initialised              |
| startDate              | Moment  (required)                      |         | startDate of controlled component                           |
| endDate                | Moment  (required)                      |         | endDate of controlled component                             |


#### MessageBar<a name="MessageBar"></a>
```javascript
<Core.MessageBar
    mode={'error'}
    isOpen={false}
    closeMessageBar={() => {}}
    message={'message'}
    onConfirm={() => {}}
    onRecover={() => {}}
    errorCountDownFlag={0}
    errorDuration={20}
/>
```
###### Props
| Name               | Type              | Default  | Description                                                       |
| ------------------ | ----------------- | -------- | ----------------------------------------------------------------- |
| mode        	     | string (required) |          | The theme of Message Bar (error or warning)                       |
| isOpen             | boolean (required)|          | If `true`, Message Bar will be displayed                          |
| closeMessageBar    | func (required)   |          | To set the param isOpen to false                                  |
| message            | string (required) |          | The Content of Message Bar                                        |
| onConfirm          | func (optional)	 |          | Warning Mode: Call confirm() when button is clicked               |
| onRecover 		 | func (optional)	 |          | Warning Mode: Call recover() when button is clicked               |
| errorCountDownFlag | number (optional) | 0        | Error Mode: The flag to trigger the reset of auto disappear timer |
| errorDuration      | number (optional) | 20       | Error Mode: The duration(second) of auto disappear timer          |

---
### Colors<a name="Colors"></a>
Company standard colors are stored in array form
```javascript
    Color.PrimaryBlack,
    Color.PrimaryBlue,
    Color.PrimaryDark,
    Color.PrimaryGrey,
    Color.PrimaryWhite,
    Color.SkyBlue,
    Color.HorizonBlue,
    Color.AquaGreen,
    Color.UnrayYellow,
    Color.SunsetOrange,
    Color.Sapphire,
    Color.Apatite,
    Color.Emerald,
    Color.Amber,
    Color.Ruby,
    Color.AlertRed
```

---

### Icons<a name="Icons"></a>
Icons are converted to React Component, also can use the original svg file

#### Icon Component <a name="IconComponent"></a>
```javascript
    <Icons.IconAdd />
```
#### Original Svg File <a name="SVGIcon"></a>
```javascript
    import IconCursorUpDown from 'hph-component/dist/svg/icon/Icon-cursor-updown.svg';
```
---
### GlobalStyle<a name="GlobalStyle"></a>
Font Style can globally apply to applications by adding the GlobalStyle
Generally will add the GlobalStyle Component in App
```javascript
    return(
        <Fragment>
            <GlobalStyle/>
            ...
        </Fragment>
    )
```
