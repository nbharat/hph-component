import { SliderRange } from "./RangeSlider";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
 
  label: "label",
  valuetxt: "2,8",
  valuetxt0:"4",
  valuetxt1:"8",
  value: []=[2,6],
  min: 0,
  max:  8,
  orientation: "horizontal",
  step: 1, 
  className: "hph-Slider",
  disabled:false,
  disabledtxt:false,
  disabledInputBox: false,
  errorMessage: "errorMsg"
  
};
describe('SliderRange', () => {
    test("hph-Slider empty", () => {
        render(<SliderRange />);
    });

    test("hph-Slider errorMessage", () => {
        render(<SliderRange errorMessage="Error" />);
    });

    test("hph-Slider errorMessage", () => {
        render(<SliderRange min={1} valuetxt0="2" max={4} />);
    });

    test("hph-Slider errorMessage", () => {
        render(<SliderRange valuetxt1="8" max={4} />);
    });

    test("hph-Slider", () => {
        render(<SliderRange 
            label={Props.label}
            valuetxt={""}
            valuetxt0={"4"}
            valuetxt1={"8"}
            value={Props.value}
             min={Props.min}
            max={Props.max}
            orientation={"horizontal"}
            step={Props.step}
            className={Props.className}
            disabled={Props.disabled}
            disabledtxt={Props.disabledtxt}
            disabledInputBox={Props.disabledInputBox} 
        />);
        const container = document.querySelector('.hph-Slider');
       
        if (container) {            
            expect(container).toHaveClass("hph-Slider");
        }
    });
    test("hph-Slider vertical", () => {
        render(<SliderRange 
            label={Props.label}
            valuetxt={"2"}
            valuetxt0={Props.valuetxt0}
            valuetxt1={Props.valuetxt1}
            value={Props.value}
             min={4}
            max={Props.max}
            orientation={"vertical"}
            step={Props.step}
            className={Props.className}
            disabled={Props.disabled}
            disabledtxt={Props.disabledtxt}
            disabledInputBox={Props.disabledInputBox} 
        />);
        const container = document.querySelector('.hph-Slider');
       
        if (container) {            
            expect(container).toHaveClass("hph-Slider");
        }
    });
    test("value", () => {

        render(<SliderRange 
            label={Props.label}
            valuetxt={"1,"}
            valuetxt0={Props.valuetxt0}
            valuetxt1={undefined}
            value={Props.value}
             min={Props.min}
            max={Props.max}
            orientation={"horizontal"}
            step={Props.step}
            className={Props.className}
            disabled={Props.disabled}
            disabledtxt={Props.disabledtxt}
            disabledInputBox={Props.disabledInputBox} 
        
        />)
        
        const container = document.querySelector('.hph-Slider');
      
        if (container) {            
            expect(container).toHaveClass("hph-Slider");
        }
    });
   
    test("bubble", () => {

        render(<SliderRange 
            label={Props.label}
            valuetxt={Props.valuetxt}
            valuetxt0={Props.valuetxt0}
            valuetxt1={Props.valuetxt1}
            value={Props.value}
             min={Props.min}
            max={Props.max}
            orientation={"horizontal"}
            step={Props.step}
            className={"bubble"}
            disabled={Props.disabled}
            disabledtxt={Props.disabledtxt}
            disabledInputBox={Props.disabledInputBox} 
        
        />)
        
        const container = document.querySelector('.bubble');
      
        if (container) {            
            expect(container).toHaveClass("bubble");
        }
    });

 
    test("hph-Slider onChange ", () => {
        render(<SliderRange 
            label={Props.label}
            valuetxt={Props.valuetxt}
            valuetxt0={Props.valuetxt0}
            valuetxt1={Props.valuetxt1}
            value={[4,8]}
             min={Props.min}
            max={4}
            orientation={"vertical"}
            step={Props.step}
            className={Props.className}
            disabled={Props.disabled}
            disabledtxt={Props.disabledtxt}
            disabledInputBox={Props.disabledInputBox} 
            errorMessage={""}
        />);
        const container = document.querySelector('.hph-Slider');
        
        if (container) {
            (container as HTMLElement).onchange;
            expect(container).toHaveClass('hph-Slider');
        }
    });
    
    test("max<value  ", () => {

        render(<SliderRange 
            label={Props.label}
            valuetxt={Props.valuetxt}
            valuetxt0={Props.valuetxt0}
            valuetxt1={Props.valuetxt1}
            value={[4,12]}
             min={Props.min}
            max={Props.max}
            orientation={"horizontal"}
            step={Props.step}
            className={Props.className}
            disabled={Props.disabled}
            disabledtxt={Props.disabledtxt}
            disabledInputBox={Props.disabledInputBox} 
        
        />)
        
        const container = document.querySelector('.hph-Slider');
      
        if (container) {            
            expect(container).toHaveClass("hph-Slider");
        }
    });
});

