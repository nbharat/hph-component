import React from 'react';
import  { SliderWrapper, StyledSlider, SliderLabel, SliderControl, SliderInner, SliderInput, SliderRangeInput } from '../styled/slider.styled';

type SliderOrientationType = 'horizontal' | 'vertical';
type Sliderdisabled = true | false;
type SliderValueType = [number, number];

interface SliderChangeParams {
  originalEvent: React.SyntheticEvent;
  value: SliderValueType;
}
interface SliderSlideEndParams extends SliderChangeParams { }

export interface PropssliderRange {
  /**
  * Id Unique identifier of the element.
 */
  id?: string,
  /**
  * slider 0 to 100 or range [0,100]	Value of the component.
 */
  value?: SliderValueType,
  /**
  * slider 0 to 100 or range [0,100]	Value of the component.
 */
  valuetxt?: string,
  /**
  * slider 0 to 100 or range [0,100]	Value of the component.
 */
  valuetxt0?: string,
  /**
  * slider 0 to 100 or range [0,100]	Value of the component.
 */
  valuetxt1?: string,
  /**
  * 0 Mininum boundary value.
 */
  min?: number | 0,
  /**
  * 100	Maximum boundary value.
 */
  max?: number | 0,
  /**
  * horizontal	Orientation of the slider, valid values are horizontal and vertical.
 */
  orientation?: SliderOrientationType,
  /**
 * 1	Step factor to increment/decrement the value.
*/
  step?: number,
  /**
 * 	When speficed, allows two boundary values to be picked.
*/
  range?: boolean,
  /**
 * 	Inline style of the component.
*/
  style?: object,
  /**
 * 	Style class of the element.
*/
  className?: string,
  /**
  * 	false	When present, it spe`cifies that the component should be input text disabled.
 */
  disabledtxt?: Sliderdisabled,
  /**
  * 	false	When present, it spe`cifies that the component should be disabled.
 */
  disabled?: Sliderdisabled,
  /**
  * 	false	When present, it spe`cifies that the component should be disabled.
 */
  disabledInputBox?: Sliderdisabled,
  /**
* 		Index of the element in errorMessage.
*/
  errorMessage?: string,
  /**
* 		label of the element in label display.
*/
  label?: string,
  /**
 * 		Index of the element in tabbing order.
*/
  tabIndex?: number,
  /**
  * Establishes relationships between the component and label(s) where its value should be one or more element IDs.
 */
  ariaLabelledBy?: string,
  onChange?(e: SliderChangeParams): void,
  onSlideEnd?(e: SliderSlideEndParams): void,
}

const sortvalue = (val1:any)=> {  
  return val1.sort((a, b) => a > b ? 1 : -1);
};

class SliderRange extends React.Component<PropssliderRange, {}, any>{  
  
  setBubble = (value: any, minv: any, maxv: any) => {

    minv = value == undefined ? minv : value < minv ? value : minv;
    maxv = value == undefined ? maxv : value > maxv ? value : maxv;
    const val = value == undefined ? minv : value;
    const min = minv ? minv : 0;
    const max = maxv ? maxv : 100;
    const newVal = Number(((val - min) * 100) / (max - min));
    return newVal || '0';
  };

  state = {
    id: this.props.id,
    label: this.props.label,
    value: this.props.value || [2, 4],
    valuetxt: this.props.valuetxt,
    valuetxt0: this.props.valuetxt0 || 2,
    valuetxt1: this.props.valuetxt1 || 8,
    min: this.props.min || 0,
    max: this.props.max || 8,
    orientation: this.props.orientation,
    step: this.props.step || 1,
    range: true,
    style: {},
    className: '',
    disabled: this.props.disabled,
    disabledInputBox: this.props.disabledInputBox,
    disabledtxt: this.props.disabledtxt || true,
    tabIndex: 0,
    ariaLabelledBy: '',
    errorMessage: this.props.errorMessage,
    data:[],
  };

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        step: this.props.step,
        label: this.props.label,
        valuetxt0: (this.props.valuetxt == '' || this.props.valuetxt == undefined) ? this.state.valuetxt0 : this.props.valuetxt?.includes(',') ? this.props.valuetxt?.toString().replace(',', '-').split('-')[0] : this.props.valuetxt,
        valuetxt1: ((this.props.valuetxt == '' || this.props.valuetxt == undefined) ? this.state.valuetxt1 : this.props.valuetxt?.includes(',') ? this.props.valuetxt?.toString().replace(',', '-').split('-')[1] : this.props.valuetxt1) === undefined ? this.props.min : this.props.valuetxt == '' ? this.state.valuetxt1 : this.props.valuetxt?.includes(',') ? this.props.valuetxt?.toString().replace(',', '-').split('-')[1] == '' ? 0 : this.props.valuetxt?.toString().replace(',', '-').split('-')[1] : this.props.valuetxt1,
        value: [Number(this.props.min) > Number(this.state.valuetxt0) ? this.state.min : Number(this.state.valuetxt0), Number(this.props.max) < Number(this.state.valuetxt1) ? Number(this.state.max) : Number(this.state.valuetxt1)],
        min: Number(this.props.min),
        max: Number(this.props.max),
        orientation: this.props.orientation,
        range: this.props.range,
        disabled: this.props.disabled,
        disabledInputBox: this.props.disabledInputBox,
        disabledtxt: this.props.disabledtxt,
        errorMessage: this.props.errorMessage,
       
      });
    }
  }

  componentDidMount() {
    this.setState({
      step: this.props.step || 1,
      label: this.props.label || 'label',
      valuetxt0: (this.props.valuetxt == '' || this.props.valuetxt == undefined) ? this.state.valuetxt0 : this.props.valuetxt?.includes(',') ? this.props.valuetxt?.toString().replace(',', '-').split('-')[0] : this.props.valuetxt || 4,
      valuetxt1: ((this.props.valuetxt == '' || this.props.valuetxt == undefined) ? this.state.valuetxt1 : this.props.valuetxt?.includes(',') ? this.props.valuetxt?.toString().replace(',', '-').split('-')[1] : this.props.valuetxt1) === undefined ? this.props.max : this.props.valuetxt == '' ? this.state.valuetxt1 : this.props.valuetxt?.includes(',') ? this.props.valuetxt?.toString().replace(',', '-').split('-')[1] : this.props.max || 8,
      value: [Number(this.props.min) > Number(this.state.valuetxt0) ? this.props.min : Number(this.state.valuetxt0), Number(this.state.max) < Number(this.state.valuetxt1) ? Number(this.state.max) : Number(this.state.valuetxt1)],
      min: this.props.min || 0,
      max: this.props.max || 8,
      orientation: this.props.orientation || 'horizontal',
      range: this.props.range,
      disabled: this.props.disabled,
      disabledInputBox: this.props.disabledInputBox,
      disabledtxt: this.props.disabledtxt,
      errorMessage: this.props.errorMessage || '',
      
    });
  }

  render() {
    return (
      <>
      <SliderWrapper vertical={this.state.orientation == 'vertical' ? true : false}>
          <SliderLabel>{this.state.label}</SliderLabel>
          <SliderControl>
            <SliderInner>
              <StyledSlider
                id={this.state.id}
                min={this.state.min}
                max={this.state.max}
                orientation={this.state.orientation}
                step={this.state.step}
                style={this.state.style}
                className={this.state.className}
                disabled={this.state.disabled}
                tabIndex={this.state.tabIndex}
                ariaLabelledBy={this.state.ariaLabelledBy}
                value={sortvalue([Number(this.state.min) > Number(this.state.valuetxt0) ? this.state.min :
                  Number(this.state.max) < Number(this.state.valuetxt0) ? Number(this.state.max) : Number(this.state.valuetxt0), 
                Number(this.state.max) < Number(this.state.valuetxt1) ? Number(this.state.max) :
                  Number(this.state.min) > Number(this.state.valuetxt1) ? Number(this.state.min) : Number(this.state.valuetxt1)])}
                          onChange={(e) => this.setState({ value: e.value, valuetxt0: e.value[0], valuetxt1: e.value[1] }) 
                } range={true} />
              {
                this.state.orientation == 'horizontal' ? <output style={{ left: `${this.setBubble(this.state.valuetxt0, this.state.min, this.state.max)}%` }}>{this.state.valuetxt0.toString() || 0}</output> : <output style={{ bottom: `${this.setBubble(this.state.valuetxt0.toString(), this.state.min, this.state.max)}%` }}>{this.state.valuetxt0.toString()}</output>
              }
              {
                this.state.orientation == 'horizontal' ? <output style={{ left: `${this.setBubble(this.state.valuetxt1, this.state.min, this.state.max)}%` }}>{this.state.valuetxt1.toString() || 0}</output> : <output style={{ bottom: `${this.setBubble(this.state.valuetxt1.toString(), this.state.min, this.state.max)}%` }}>{this.state.valuetxt1.toString()}</output>
              }
              <ul>
                <li>{this.state.min}</li>
                <li>{(this.state.min + this.state.max) / 2}</li>
                <li>{this.state.max}</li>
              </ul>
              <em>{this.state.errorMessage}</em>
            </SliderInner>
            <SliderInput errMsg={this.state.errorMessage ? true : false}>
              {!this.state.disabledtxt ?
                <SliderRangeInput>
                  <input 
                    disabled={this.state.disabled || this.state.disabledInputBox}
                    value={this.state.valuetxt0?.toString()}
                    onChange={(e: any) => 
                      this.setState({
                      value: [Number(e.target.value), 
                        this.state.valuetxt1],
                      valuetxt0: e.target.value,
                    })
                    } /> <span>-</span>
                  <input 
                    disabled={this.state.disabled || this.state.disabledInputBox}
                    value={this.state.valuetxt1?.toString()}
                    onChange={(e: any) => this.setState({
                      value: [this.state.valuetxt0, Number(e.target.value)],
                      valuetxt1: e.target.value,
                    })
                    } />
                </SliderRangeInput>

                : ''}
            </SliderInput>
          </SliderControl>
      </SliderWrapper>
      </>
    );
  }
}
export { SliderRange };         