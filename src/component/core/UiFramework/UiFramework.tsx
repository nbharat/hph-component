import React from 'react';
import { StyledMainWrapper, StyledVertical, StyledHorizontal, StyledInnerElement, StyledParentLabel, StyledMainContent, StyledSidebar, SidebarActionBar, StyledTableActionButton, Sidebarheader, SidebarTitle, SidebarCaption,  StyledColoum, StyledTitle, StyledElement } from './../styled/uiFramework.styled';import { HPHRadio } from '../RadioButton/RadioButton';
import { InputDropdown } from '../InputDropdown/InputDropdown';
import { InputField } from '../InputField/InputField';
import { HPHTable } from '../Table/Table';
import { IconButton } from '../IconButtons/IconButtons';
import { HPHCheckbox } from '../CheckBox/CheckBox';
import { GroupedButtons } from '../GroupButton/GroupButton';
import { HPHDragAndDrop } from '../DragAndDrop/DragAndDrop';
export interface UiFrameworkProps {
  id?: string | 'UIFramework',
  isActive?:boolean;
  ToggleSidebarOpen?: (e: any) => void,
  ToggleSidebarClose?: (e: any) => void,
  onRadioChange1?: (e: any) => void,
  onRadioChange2?: (e: any) => void,
  onRadioChange3?: (e: any) => void,
  onCheckChange?: (e: any) => void,
  radio1?: any,
  radio2?: any,
  radio3?: any,
  check?: any,
  horizontalCheckbox?:any[],
  verticalCheckbox?:any[],
  Table1Columns?:any[],
  Table2Columns?:any[],
  Table3Columns?:any[],
  Table4Columns?:any[],
  groupButtonValue?:any,
  groupButtonValue1?:any,
  onGroupButtonChange?: (e: any) => void,
  onGroupButtonChange1?: (e: any) => void,
  tableColumns?:any,
  [x:string]: any;
}

const UiFramework: React.FC<UiFrameworkProps> = ({
  id = 'UIFramework',
  isActive = false,
  radio1,
  radio2,
  radio3,
  onRadioChange1,
  onRadioChange2,
  onRadioChange3,
  ToggleSidebarOpen,
  ToggleSidebarClose,
  onCheckChange,
  check,
  horizontalCheckbox = [],
  verticalCheckbox = [],
  Table1Columns = [],
  Table2Columns = [],
  Table3Columns = [],
  Table4Columns = [],
  groupButtonValue,
  onGroupButtonChange,
  groupButtonValue1,
  onGroupButtonChange1,
  tableColumns = [],
}) : any => {
 
  return (
          <StyledMainWrapper id={id}>            
            <StyledMainContent>
              <StyledColoum style={{ ...isActive ? { paddingRight:'20px' } : {} }} >
                <StyledElement>
                  <StyledTitle>timeline</StyledTitle>
                  <InputDropdown
              errorMessage=""
            field="dropdownLabel"
            inputType="freeText"
            label="Week start from"
            mode="single"
            onChange={() => {}}
            options={[
              {
                dropdownLabel: 'Sunday',
                tagLabel: 'O1',
                value: 'option1',
              },
              {
                dropdownLabel: 'Monday',
                tagLabel: 'O2',
                value: 'option2',
              },
              {
                dropdownLabel: 'Tuesday',
                tagLabel: 'O3',
                value: 'option3',
              },
              {
                dropdownLabel: 'Wednesday',
                tagLabel: 'O4',
                value: 'option4',
              },
              {
                dropdownLabel: 'Thursday',
                tagLabel: 'O5',
                value: 'option5',
              },
              {
                dropdownLabel: 'Friday',
                tagLabel: 'O6',
                value: 'option6',
              },
              {
                dropdownLabel: 'Saturday',
                tagLabel: 'O7',
                value: 'option7',
              },
            ]}
            virtualScrollerOptions={{
              delay: 0,
              lazy: false,
              showLoader: true,
            }}
            width="250px"
            />
            <StyledInnerElement>
              <StyledParentLabel>First Week of a Year</StyledParentLabel>
              <HPHRadio
                  options={[
                    { inputId: 1, key: 'A', name: '1 January' },
                    { inputId: 2, key: 'B', name: 'First 4 days week' },
                    { inputId: 3, key: 'C', name: 'First full week' },
                  ]}
                  orientation="horizontal"
                  checked={radio1}
                  onChange={onRadioChange1}
                />
            </StyledInnerElement>
            </StyledElement> 
            <StyledElement>
              <StyledTitle>Historical Estimation</StyledTitle>
              <InputField
              errorMessage=""
              label="Last Number of Voyage"
              toolTipText="Help Icon"
              type="text"
              width="255px"
            />
            <StyledHorizontal>
              <StyledParentLabel>Including</StyledParentLabel>
              {
                horizontalCheckbox.map((category) => {
                  return (
                        <div key={category.key} className="field-checkbox">
                          <HPHCheckbox 
                          inputId={category.key} 
                          name="category" 
                          value={category} 
                          checked = {check.some((item:any) => item.key === category.key)}
                          onChange={onCheckChange}  
                          label={category.name}>
                          </HPHCheckbox>
                          
                        </div>
                  );
                })
            }
            </StyledHorizontal>
            </StyledElement>
            <StyledElement>
              
              <StyledTitle>Vessel Clearance</StyledTitle>
            
              <GroupedButtons errorMessage=""
                label=""
                showIcon={false}
                value={groupButtonValue}
                onChange={onGroupButtonChange}
                multiple={false}
                list={[
                  {
                    name: '% of LOA',
                    value: '1',
                  },
                  {
                    name: 'Fixed Hours',
                    value: '2',
                  },
                  {
                    name: 'Fixed Length',
                    value: '3',
                  },
                  {
                    name: 'Fixed by LOA',
                    value: '4',
                  },
                ]}></GroupedButtons>
              <StyledInnerElement>
              {groupButtonValue == '1' ? <InputField
              errorMessage=""
              label="% of Vessel LOA"
              type="text"
              width="255px"
            /> : ''}
            {groupButtonValue == '2' ? <InputField
              errorMessage=""
              label="Fixed Hours"
              type="text"
              width="255px"
            /> : ''}
            {groupButtonValue == '3' ? <InputField
              errorMessage=""
              label="Fixed Length(m)"
              type="text"
              width="255px"
            /> : ''}
              {groupButtonValue == '4' ? 
              <>
              <StyledTableActionButton>
                <IconButton
                  idIcom='Table1'
                  fileName="Icon-add"
                  size="small"
                  toolTipText='Add'
                  toolTipArrow={false}
                  onClick={ToggleSidebarOpen}
                />
              </StyledTableActionButton>  
              <div style={{ width: '100%', height: '200px' }}>
              <HPHTable
                columns={Table1Columns}
                data={[
                  {
                    id:1,
                    equation:'>',
                    loa:'330',
                    clearance:'16',
                  },
                  {
                    id:2,
                    equation:'~',
                    loa:'300',
                    clearance:'16',
                  },
                  {
                    id:3,
                    equation:'<',
                    loa:'150-170',
                    clearance:'21',
                  },
                  {
                    id:4,
                    equation:'=',
                    loa:'120-190',
                    clearance:'5',
                  },
                  {
                    id:5,
                    equation:'>',
                    loa:'110-180',
                    clearance:'5',
                  },
                  {
                    id:6,
                    equation:'<',
                    loa:'80-100',
                    clearance:'5',
                  },
                  {
                    id:7,
                    equation:'~',
                    loa:'120-150',
                    clearance:'5',
                  },
                ]}
                rows="5"
                showPaginator={false}
                editable={false}
              />
              </div> </> : ''}
              </StyledInnerElement>
             
            </StyledElement>
              </StyledColoum>

              <StyledColoum style={{ ...isActive ? { paddingRight:'20px' } : {} }} >
              <StyledElement>
                <StyledTitle> ETB Delay Color
              <IconButton
              idIcom='Table2'
                fileName="Icon-add"
                size="small"
                toolTipText='Add'
                toolTipArrow={false}
                onClick={ToggleSidebarOpen}
              /></StyledTitle>
               <div style={{ width: '100%', height: '150px', overflow: 'auto' }}>
                <HPHTable
                  columns={Table2Columns}
                  data={[
                    {
                      id:1,
                      equation:'>',
                      hours:'20',
                      color:'#EE7523',
                    },
                    {
                      id:1,
                      equation:'>',
                      hours:'20',
                      color:'#EE7523',
                    },
                    {
                      id:2,
                      equation:'~',
                      hours:'18-20',
                      color:'#DB4D59',
                    },
                    {
                      id:3,
                      equation:'<',
                      hours:'18-20',
                      color:'#ffffff',
                    },
                    {
                      id:4,
                      equation:'=',
                      hours:'18-20',
                      color:'#ffffff',
                    },
                    
                  ]}
                  rows="5"
                  showPaginator={false}
                  editable={false}
                  colorPickerDisabled={true}
                />
            </div>
              </StyledElement>
              <StyledElement>
                <StyledTitle>
                Proforma Advance / Delay Color
              <IconButton
              idIcom='Table3'
                fileName="Icon-add"
                size="small"
                toolTipText='Add'
                toolTipArrow={false}
                onClick={ToggleSidebarOpen}
              />
                </StyledTitle>
                <div style={{ width: '100%', height: '150px', overflow: 'auto' }}>
              <HPHTable
                columns={Table3Columns}
                data={[
                  {
                    id:1,
                    type:'Advance',
                    equation:'>',
                    hours:'20',
                    color:'#C2EDFF',
                  },
                  {
                    id:2,
                    type:'Delay',
                    equation:'~',
                    hours:'18-20',
                    color:'#54BBAB',
                  },
                  {
                    id:3,
                    type:'Delay',
                    equation:'<',
                    hours:'18-20',
                    color:'#ffffff',
                  },
                  {
                    id:4,
                    type:'Delay',
                    equation:'=',
                    hours:'18-20',
                    color:'#ffffff',
                  },
                  
                ]}
                rows="5"
                showPaginator={false}
                editable={false}
                colorPickerDisabled={true}
              />
            </div>
              </StyledElement>
              <StyledElement>
                <StyledTitle>
                Highlight on VBS
              <IconButton
              idIcom='Table4'
                fileName="Icon-add"
                size="small"
                toolTipText='Add'
                toolTipArrow={false}
                onClick={ToggleSidebarOpen}
              />
                </StyledTitle>
                <div style={{ width: '100%', height: '150px', overflow: 'auto' }}>
              <HPHTable
              
                columns={Table4Columns}
                data={[
                  {
                    id:1,
                    type:'Target Service',
                    condition:'Not specified',
                    color:'#FFC627',
                  },
                  {
                    id:2,
                    type:'Special Arrangement',
                    condition:'Golden service',
                    color:'#009BDE',
                  },
                  {
                    id:3,
                    type:'Port Name',
                    condition:'HIT',
                    color:'#54BBAB',
                  },
                
                  
                ]}
                rows="5"
                showPaginator={false}
                editable={false}
                colorPickerDisabled={true}
              />
            </div>
              </StyledElement>
              </StyledColoum>

              <StyledColoum style={{ ...isActive ? { paddingRight:'20px' } : {} }} >
                <StyledElement>
                  <StyledTitle>
                  Voyage Display
                  </StyledTitle>
                  <StyledParentLabel>Color by</StyledParentLabel>
                  <HPHRadio
                  options={[
                    { inputId: 1, key: 'A', name: 'Shoping Line' },
                    { inputId: 2, key: 'B', name: 'Line Service' },
                  ]}
                  orientation="horizontal"
                  checked={radio2}
                  onChange={onRadioChange2}/> 
                  <StyledInnerElement>
                    <StyledParentLabel>Row 1 Information</StyledParentLabel>
                    <HPHRadio
                  options={[
                    { inputId: 1, key: 'A', name: 'Vessel Name' },
                    { inputId: 2, key: 'B', name: 'Co/Vsl/Voy' },
                  ]}
                  orientation="horizontal"
                  checked={radio3}
                  onChange={onRadioChange3}/> 
                  </StyledInnerElement>
                  <StyledInnerElement>
                    <StyledParentLabel>Row2 Information</StyledParentLabel>
                    <HPHDragAndDrop orientation="horizontal"
                     selectedOption={[
                       {
                         id: 1,
                         label: 'Home Berth',
                         order: 1,
                         value: 'Home Berth',
                         visible: true,
                       },
                       {
                         id: 2,
                         label: 'Co/Vsl/Voy',
                         order: 2,
                         value: 'Co/Vsl/Voy',
                         visible: true,
                       },
                       {
                         id: 3,
                         label: 'SOA',
                         order: 3,
                         value: 'SOA',
                         visible: true,
                       },
                     ]}
                    unselectedOption={[
                      {
                        id: 4,
                        label: 'Terminal Service',
                        order: 4,
                        value: 'Terminal Service',
                        visible: true,
                      },
                      {
                        id: 5,
                        label: 'Line Service',
                        order: 5,
                        value: 'Line Service',
                        visible: true,
                      },
                      {
                        id: 6,
                        label: 'Others',
                        order: 6,
                        value: 'Others',
                        visible: true,
                      },
                    ]}>

                    </HPHDragAndDrop>
                  </StyledInnerElement>
                  <StyledInnerElement>
                    <StyledParentLabel>Other Informations</StyledParentLabel>
                    <StyledVertical>
                    {
                      verticalCheckbox.map((category) => {
                        return (
                              <div key={category.key} className="field-checkbox">
                                <HPHCheckbox 
                                inputId={category.key} 
                                name="category" 
                                value={category} 
                                checked = {check.some((item:any) => item.key === category.key)}
                                onChange={onCheckChange}  
                                label={category.name}>
                                </HPHCheckbox>
                              
                              </div>
                        );
                      })
                  }
                    </StyledVertical>
                  </StyledInnerElement>
                </StyledElement>
              </StyledColoum>
            </StyledMainContent>
            { isActive ? <StyledSidebar>
              <Sidebarheader>
                  <SidebarTitle>
                    Add Vessel Clearance by LOA
                    <SidebarCaption>
                      All fields are required unless indicated as 'optional'
                    </SidebarCaption>  
                  </SidebarTitle>
                  <SidebarActionBar>
                    <IconButton
                      fileName="Icon-cross"
                      size="small"
                      toolTipText='Close'
                      toolTipArrow={false}
                      onClick={ToggleSidebarClose}
                    />
                    <IconButton
                      fileName="Icon-tick"
                      size="small"
                      toolTipText='Save'
                      toolTipArrow={false}
                      onClick={ToggleSidebarClose}
                    />
                  </SidebarActionBar>
              </Sidebarheader>
             { tableColumns.map((item:any)=>{
               if (item.field == 'equation'){
                 return (<StyledInnerElement><GroupedButtons errorMessage=""
                 label=""
                 showIcon={false}
                 value={groupButtonValue1}
                 onChange={onGroupButtonChange1}
                 multiple={false}
                 list={[
                   {
                     name: '<=',
                     value: 'A',
                   },
                   {
                     name: '<',
                     value: 'B',
                   },
                   {
                     name: '>',
                     value: 'C',
                   },
                   {
                     name: '>=',
                     value: 'D',
                   },
                   {
                     name: '=',
                     value: 'E',
                   },
                   {
                     name: '~',
                     value: 'F',
                   },
                 ]}></GroupedButtons></StyledInnerElement>);
               } else {
                 return  (<StyledInnerElement><InputField
                 errorMessage=""
                 label={item.header}
                 type="text"
                 width="255px"
               /></StyledInnerElement>);
               }
             })
            }
            </StyledSidebar> : <StyledSidebar style={{ width: '0px', border:'0' }}></StyledSidebar>}          
          </StyledMainWrapper>
        
  );
};
export { UiFramework };