import { UiFramework } from "./UiFramework";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom';

const Props = {
    id: "UIFramework",
    isActive:false,
    groupButtonValue:'4',
}

describe('Test', () => {
    test(" UI Framework id", () => {
        render(<UiFramework />);
    });

    test(" UI Framework id", () => {
        render(<UiFramework id={Props.id} />);
        const container = document.querySelector('#UIFramework');
        expect(container).toBeTruthy();
    });
    test(" UI Framework side panel is active", () => {
        render(<UiFramework id={Props.id} multiple={Props.isActive}/>);
        const container = document.querySelector('#UIFramework');
        expect(container).toBeTruthy();
    });
    test(" UI Framework", () => {
        render(<UiFramework id={Props.id} groupButtonValue={''}/>);
        const container = document.querySelector('#UIFramework');
        expect(container).toBeTruthy();
    });

});