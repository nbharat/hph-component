/* eslint-disable import/extensions */
/* eslint-disable @typescript-eslint/no-unused-expressions */
import React, { Component } from 'react';
import { Lozenges } from '..';
import { IconButton } from '../IconButtons/IconButtons';
import { 
  AutoCompleteGlobalStyles, StyledAutoComplete, StyledInputErrorMsg, StyledInputLabel,
  StyledInputWrapper, StyledMenuItemTemplate, StyledMenuItemTemplateIcon,
  StyledMenuItemTemplateValue, StyledRequired, StyledLozengesContainer,
} from '../styled/inputDropdown.styled';

interface VirtualScrollerOptions {
  showLoader?: boolean,
  delay?: number,
  lazy?: boolean,
  onLazyLoad?: (e: any) => void,
  [x:string]: any
}

export interface InputDropdownProps {
  options?: any[],
  freeTextInput?: boolean | false,
  mode?: string | 'single',
  virtualScrollerOptions?: VirtualScrollerOptions,
  onChange?: (e: any) => void,
  inputType?: string | 'freeText' | 'number' | 'text',
  field?: string | 'dropdownLabel' | 'tagLabel' | 'value',
  width? : string | '200px',
  [x:string]: any
}

class InputDropdown extends Component<InputDropdownProps, {}, any> {
  state = {
    selectedValue: null,
    options: this.props.options || [],
    filteredList: [],
    panelLeft: null,
    tempValue: null,
  };

  elemRef: any = React.createRef();

  searchList = (event: { query: string }) => {
    const { options } = this.state;
    let filteredList;
    let sortedOptions = options.sort();
    // if (this.props.sort) {
    //   sortedOptions = options.sort((_a, b) => b.icon ? 1 : -1);
    // }
    if (!event.query.trim().length) {
      filteredList = [...sortedOptions];
    } else {
      filteredList = sortedOptions.filter((item: any) => {
        const v = (item.dropdownLabel || item.value);
        return v.toLowerCase().startsWith(event.query.toLowerCase());
      });

      this.setState({ tempValue: event.query });
    }

    this.setState({ filteredList });
  };

  itemTemplate = (item: any) => {
    return (
      <StyledMenuItemTemplate>
        {item.icon && <StyledMenuItemTemplateIcon fileName={item.icon} width="24px" height="24px" />}
        <StyledMenuItemTemplateValue>{item.dropdownLabel || item.value}</StyledMenuItemTemplateValue>
        {item.isMaster && <StyledLozengesContainer id="LozengesContainer"><Lozenges label={item.lozengesLabel} variation={item.lozengesVariation} /></StyledLozengesContainer>}
      </StyledMenuItemTemplate>
    );
  };

  selectedItemTemplate = (item: any) => {
    return (
      <StyledMenuItemTemplate>
        {item.icon && <StyledMenuItemTemplateIcon fileName={item.icon} width="24px" height="24px" />}
        <StyledMenuItemTemplateValue>{item[this.props.field || 'value'] || item.value}</StyledMenuItemTemplateValue>
      </StyledMenuItemTemplate>
    );
  };

  isNumber = (evt: any) => {
    evt = (evt) ? evt : window.event;
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  };

  isAlfa = (evt: any) => {
    evt = (evt) ? evt : window.event;
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  };

  handleFreeText = (e) => {
    if (this.props.freeTextInput) {
      const { options, tempValue, selectedValue } = this.state;
      if (e.which === 13 && (tempValue && tempValue !== '')) {
        const itemExists = options.filter(
          (item: any) => item.value === tempValue || item.dropdownLabel === tempValue,
        )[0];
        if (!itemExists) {
          options.push({ value: tempValue });
          const newValue = this.props.mode === 'multiple' ? [
            ...(selectedValue ? selectedValue : []),
            { value: tempValue },
          ] : tempValue;
          this.setState({ options, selectedValue: newValue }, () => {
            this.setState({ tempValue: null });
            if (this.props.mode === 'multiple') {
              const input: any = document.querySelector(
                '.p-autocomplete-input-token input',
              );
              if (input) {
                input.value = '';
              }  
            }
          });
        }
      }
      
    }
  };

  handleKeyPress = (e) => {
    const shouldType = this.props.inputType === 'number' ? this.isNumber(e) : this.props.inputType === 'text' ? this.isAlfa(e) : true;
    (shouldType) ? this.handleFreeText(e) : e.preventDefault();
  };

  handleBlur = () => {
    const inp: any = document.querySelector('.p-autocomplete-input-token input');
    if (inp) {
      inp.value = '';
    }
  };

  onDropdownClick = (e) => {
    const pAuto = this.elemRef?.current?.container;
    const rect = pAuto?.getBoundingClientRect();

    this.setState({ panelLeft: rect?.left });
  };

  render() {
    const { 
      freeTextInput, mode, field, virtualScrollerOptions,
      label = 'Label', required = false,
      onChange, width = '200px', errorMessage = '',
      helpIcon = false, toolTipText = 'Helper Message',
      placeholder = 'Please select option(s)',
      ...rest
    } = this.props;
    const { selectedValue, filteredList } = this.state;
    let inputWidth: any, panelWidth: any, autoCompleteWidth: any = width;
    if (width && !isNaN(Number(width.replace('px', '').replace('%', '')))) {
      const w = Number(width.replace('px', '').replace('%', ''));
      panelWidth = `${w - 2}px`;
      // autoCompleteWidth = `${w - 45}px`;
      let sv: any = selectedValue;
      inputWidth = Array.isArray(sv) && sv.length ? (w * .8) : '100%';
    }
    return (
      <>
      <AutoCompleteGlobalStyles panelLeft={this.state.panelLeft} />
      <StyledInputWrapper>
      <StyledInputLabel>
            {label} {required ? <StyledRequired>*</StyledRequired> : ''}
            {helpIcon ? <IconButton fileName={'Icon-help'} size={'small'}
              toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
              toolTipText={toolTipText} {...rest} /> : ''}
          </StyledInputLabel>
      <StyledAutoComplete
        autoCompleteWidth={autoCompleteWidth}
        ref={this.elemRef}
        errorMessage={errorMessage ? true : false}
        {...freeTextInput ? { onKeyPress: this.handleKeyPress, dropdownIcon: 'Icon-auto-text' } : {}}
        onKeyPress={this.handleKeyPress}
        value={selectedValue}
        suggestions={filteredList}
        completeMethod={this.searchList}
        field={field || 'value'}
        onDropdownClick={this.onDropdownClick}
        dropdown
        {...mode === 'multiple' ? { multiple: true, selectedItemTemplate: this.selectedItemTemplate, removeIcon: 'pi pi-times' } : {}}
        virtualScrollerOptions={{ 
          ...virtualScrollerOptions,
          ...!virtualScrollerOptions?.itemSize ? {  itemSize: 30 } : {}, style: { width: panelWidth } }
        }
        scrollHeight="225px"
        style={{ width }}
        placeholder={placeholder}
        itemTemplate={this.itemTemplate}
        inputStyle={{ width: inputWidth }}
        onBlur={this.handleBlur}
        onChange={(e) => this.setState({ selectedValue: e.value }, () => onChange && onChange(e.value))}
        {...rest}
      />
      <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
      </StyledInputWrapper>
      </>
    );
  }
}

export { InputDropdown };