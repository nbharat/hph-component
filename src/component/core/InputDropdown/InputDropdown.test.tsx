import React from "react";
import '@testing-library/jest-dom';
import { render } from '@testing-library/react'
import { InputDropdown } from "./InputDropdown";

describe('InputDropdown', () => {
    test("InputDropdown 1", () => {
        render(<InputDropdown dropdownList={[]} freeTextInput mode="multiple" required={true} />);
    });

    test("InputDropdown 2", () => {
        render(<InputDropdown dropdownList={[]} freeTextInput mode="multiple" required={false} />);
    });

    test("InputDropdown 3", () => {
        render(<InputDropdown dropdownList={[]} freeTextInput />);
    });

    test("InputDropdown 4", () => {
        render(<InputDropdown dropdownList={[]} errorMessage="Error" />);
    });

    test("InputDropdown 5", () => {
        render(<InputDropdown dropdownList={[]} virtualScrollerOptions={{ itemSize: 30 }} />);
    });

    test("InputDropdown 6", () => {
        render(<InputDropdown dropdownList={[]} inputType="number" freeTextInput={true} />);
    });

    test("InputDropdown 7", () => {
        render(<InputDropdown dropdownList={[]} inputType="freeText" freeTextInput={true} />);
    });

    test("InputDropdown 8", () => {
        const fn = jest.fn();
        render(<InputDropdown dropdownList={[]} inputType="text" freeTextInput={true} onChange={fn} />);
    });
});
