import { SingleSlider } from "./SingleSlider";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
 
  label: "label",
  value: 8,
  min: 2,
  max:  8,
  orientation:"horizontal",
  step: 1, 
  className: "hph-Slider",
  disabled:false,
  disabledtxt:false,
  disabledInputBox: false,
  errorMessage: "errorMsg",

};
describe('SingleSlider', () => {
    test("hph-Slider", () => {
        render(<SingleSlider />);
    });

    test("hph-Slider", () => {
        render(<SingleSlider errorMessage="Error" />);
    });

    test("hph-Slider", () => {
        render(<SingleSlider  className={Props.className} step={1} min={1}  max={6} />);
        const container = document.querySelector('.hph-Slider');              
        expect(container).toBeNull();
    });

    test("hph-Slider horizontal", () => {
        render(<SingleSlider  className={Props.className} orientation="horizontal" step={1} min={1}  max={6} />);
        const container = document.querySelector('.hph-Slider');              
        expect(container).toBeNull();
    });

    test("hph-Slider vertical", () => {
        render(<SingleSlider  className={Props.className} orientation="vertical" step={1} min={1}  max={6} />);
        const container = document.querySelector('.hph-Slider');              
        expect(container).toBeNull();
    });

    test("hph-Slider empty errorMessage", () => {
        render(<SingleSlider  className={Props.className} orientation="vertical" errorMessage="" step={1} min={1}  max={6} />);
        const container = document.querySelector('.hph-Slider');              
        expect(container).toBeNull();
    });

    test("hph-Slider empty sstep=0 and min=0", () => {
        render(<SingleSlider  className={Props.className} step={1} min={1}  max={6} />);
        const container = document.querySelector('.hph-Slider');              
        expect(container).toBeNull();
    });
});
