import React from 'react';
import  { SliderWrapper, StyledSlider, SliderLabel, SliderControl, SliderInner, SliderInput } from '../styled/slider.styled';

type SliderOrientationType = 'horizontal' | 'vertical';
type Sliderdisabled = true | false;
type SliderValueType = number;
type SliderValueArrayType = [number, number];

interface SliderChangeParams {
  originalEvent: React.SyntheticEvent;
  value: SliderValueType;
}

interface SliderArrayChangeParams {
  originalEvent: React.SyntheticEvent;
  valueArray: SliderValueArrayType;
}

interface SliderSlideEndParams extends SliderArrayChangeParams { }
interface SliderSlideEndParams extends SliderChangeParams { }

export interface Propsslider {
  /**
  * Id Unique identifier of the element.
 */
  id?: string,
  /**
  * slider 0 to 100 or range [0,100]	Value of the component.
 */
  value?: SliderValueType,
  /**
  * 0 Mininum boundary value.
 */
  min?: number,
  /**
  * 100	Maximum boundary value.
 */
  max?: number,
  /**
  * horizontal	Orientation of the slider, valid values are horizontal and vertical.
 */
  orientation?: SliderOrientationType,
  /**
 * 1	Step factor to increment/decrement the value.
*/
  step?: number,
  /**
 * 	Inline style of the component.
*/
  style?: object,
  /**
 * 	Style class of the element.
*/
  className?: string,
  /**
  * 	false	When present, it spe`cifies that the component should be input text disabled.
 */
  disabledtxt?: Sliderdisabled,
  /**
  * 	false	When present, it spe`cifies that the component should be disabled.
 */
  disabled?: Sliderdisabled,
  /**
    * 	false	When present, it spe`cifies that the component should be disabled.
   */
  disabledInputBox?: Sliderdisabled,
  /**
  * slider 0 to 100 or range [0,100]	Value of the component.
 */
  valuetxt?: string,
  /**
 * 		Index of the element in tabbing order.
*/

  tabIndex?: number,
  /**
 * 		Index of the element in errorMessage.
*/

  errorMessage?: string,
  /**
 * 		label of the element in label display.
*/
  label?: string
  /**
  * Establishes relationships between the component and label(s) where its value should be one or more element IDs.
 */
  ariaLabelledBy?: string
  onChange?(e: SliderChangeParams): void;
  onSlideEnd?(e: SliderSlideEndParams): void;
  [x:string]: any;
}

class SingleSlider extends React.Component<Propsslider, {}, any>{

  setBubble = (value: any, minv: any, maxv: any) => {
       
    minv = value < minv ? value : minv;
    maxv = value > maxv ? value : maxv;
    const val = value;
    const min = minv ? minv : 0;
    const max = maxv ? maxv : 100;
    const newVal = Number(((val - min) * 100) / (max - min));
    return newVal;
  };

  state = {
    id: this.props.id,
    label: this.props.label,
    value: this.props.value || 4,
    valuetxt: this.props.valuetxt || 4,
    min: this.props.min || 0,
    max: this.props.max || 8,
    orientation: this.props.orientation,
    step: this.props.step || 1,
    style: {},
    className: '',
    disabled: this.props.disabled,
    disabledtxt: this.props.disabledtxt || true,
    disabledInputBox: this.props.disabledInputBox,
    tabIndex: 0,
    ariaLabelledBy: '',
    errorMessage: this.props.errorMessage,
  };

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        label: this.props.label,
        step: this.props.step,
        value:this.props.value, 
        valuetxt: this.props.value,
        min: Number(this.props.min),
        max:  Number(this.props.max), 
        orientation: this.props.orientation,
        disabled: this.props.disabled,
        disabledtxt: this.props.disabledtxt,
        disabledInputBox: this.props.disabledInputBox,
        errorMessage: this.props.errorMessage,
      });
    }
  }

  componentDidMount() {
    this.setState({
      label: this.props.label || 'label',
      step: this.props.step || 1,
      value: this.props.value || 8, 
      valuetxt: this.props.value || 8,
      min: this.props.min || 0,
      max:  Number(this.props.max) || 8, 
      orientation: this.props.orientation || 'horizontal',
      disabled: this.props.disabled,
      disabledtxt: this.props.disabledtxt,
      disabledInputBox: this.props.disabledInputBox,
      errorMessage: this.props.errorMessage || '',
    });
  }

  render() {
    return (
      <>
      <SliderWrapper vertical={this.state.orientation == 'vertical' ? true : false}>
        <SliderLabel>{this.state.label}</SliderLabel>
          <SliderControl>
            <SliderInner>
              <StyledSlider
                data-testid={this.state.id}
                min={this.state.min}
                max={this.state.max }
                orientation={this.state.orientation}
                step={this.state.step }
                style={this.state.style}
                className={this.state.className}
                disabled={this.state.disabled}
                tabIndex={this.state.tabIndex}
                ariaLabelledBy={this.state.ariaLabelledBy}
                value={Number(this.state.value)}
                onChange={(e: any) => 
                  this.setState({ value: e.value,
                    valuetxt:e.value,
                  
                  })}  />
              {
                this.state.orientation == 'horizontal' ? <output style={{ left: `${this.setBubble(this.state.value, this.state.min, this.state.max)}%` }}>{this.state.value || 0}</output> : <output style={{ bottom: `${this.setBubble(this.state.value, this.state.min, this.state.max)}%` }}>{this.state.value}</output>
              }
              <ul>
                <li>{this.state.min}</li>
                <li>{(this.state.min + this.state.max) / 2}</li>
                <li>{this.state.max}</li>
              </ul>
              <em>{this.state.errorMessage}</em>
            </SliderInner>
            <SliderInput errMsg={this.state.errorMessage ? true : false}>
              {!this.state.disabledtxt ? <input disabled={this.state.disabled || this.state.disabledInputBox} value={this.state.valuetxt?.toString()} 
              onChange={(e: any) => {                      
                this.setState({          
                  value:Number(e.target.value) 
                  , valuetxt:e.target.value,                           
                });
              }}/> : ''}
            </SliderInput>
        </SliderControl>
      </SliderWrapper>
      </>
    );

  }

}
export { SingleSlider };         