import React from 'react';
import{ StyledContextMenu} from './../styled/rightClickMenu.styled';

interface Props {
  element: any,
  items?: any[],
  disabled?: boolean,
  [x:string]: any;
}

const RightClickMenu: React.FC<Props> = ({ element, items, disabled, ...rest }): any => {
  return !disabled && <><StyledContextMenu ref={element} model={items} {...rest} /></>;
};

export { RightClickMenu };
