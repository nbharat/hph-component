import { HPHButton } from "./Button";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom';

const Props = {
    id: "button",
    label: "Button",
    size: 'Standard',
    theme: 'Primary',
    disabled: false,
    showIcon: false,
}

describe('Test', () => {
    test("Button id", () => {
        render(<HPHButton />);
    });

    test("Button id", () => {
        render(<HPHButton id={Props.id} />);
        const container = document.querySelector('#button');
        expect(container).toBeTruthy();
    });
    test("Button Label", () => {
        render(<HPHButton id={Props.id} label={Props.label}/>);
        const container = document.querySelector('#button');
        expect(container).toBeTruthy();
    });
    test("Button Size", () => {
        render(<HPHButton id={Props.id} size={Props.size}/>);
        const container = document.querySelector('#button');
        expect(container).toBeTruthy();
    });
    test("Button Theme", () => {
        render(<HPHButton id={Props.id} theme={Props.theme}/>);
        const container = document.querySelector('#button');
        expect(container).toBeTruthy();
    });
    test("Button is not disable", () => {
        render(<HPHButton id={Props.id}  disabled={Props.disabled}/>);
        const container = document.querySelector('#button');
        expect(container).toBeTruthy();
    });
    test("Button show Icon", () => {
        render(<HPHButton id={Props.id}  showIcon={Props.showIcon}/>);
        const container = document.querySelector('#button');
        expect(container).toBeTruthy();
    });

});