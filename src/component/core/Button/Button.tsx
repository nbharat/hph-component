import React from 'react';
import { HPHBadge } from '../Badge/Badge';
import { StyledDiv, StyledButton } from '../styled/button.styled';

export interface ButtonProps{
  id?: string | ' button',
  label?:string | 'Button',
  disabled?: boolean,
  showIcon?: boolean,
  icon?: string,
  size?:string | 'Standard' | 'Small',
  theme?:string | 'Primary' | 'Secondary' | 'Alert',
  disableBadge?: boolean,
  badgeSize?:string | 'Badge-small' | 'Badge-medium' | 'Badge-large',
  badgeValue?: string | '1',
  badgeColor?: string | 'Alert-Red' | 'Sky-Blue' | 'Sea-Blue' | 'Horizon-Blue' | 'Aqua-Green' | 'Sunray-Yellow' | 'Sunset-Orange',
  [x:string]: any;
}
   
const HPHButton: React.FC<ButtonProps> = ({
  id = 'button',
  label = 'Button',
  size = 'Standard',
  theme = 'Primary',
  badgeSize = 'Badge-small',
  badgeValue = '2',
  badgeColor = 'Alert-Red',
  icon,
  disabled = false,
  showIcon = false,
  disableBadge = true,
  ...rest
}): any => {
  return (
      < >
        <StyledDiv id={id}>
          <StyledButton 
          label={label} 
          size={size} 
          theme={theme} 
          disabled={disabled}
          icon={showIcon ? icon : ''}
          iconPos={'right'}
          {...rest}
          >
          {disableBadge ? '' :
          <HPHBadge value={badgeValue}
                    size={badgeSize}
                    color={badgeColor}
                    {...rest}
                />}
          </StyledButton>
        </StyledDiv>  
      </>
  );
};
export { HPHButton } ;