import React, { Component } from 'react';
import { StyledDiv, StyledPagination, StyledText, StyledInput, StyledButton, StyledSpan, StyledRecord, StyledBox, StyledSelected } from '../styled/Pagination.styled';

export interface PaginatorProps{
  template?: any;
  inputText?: string,
  totalRecords: number | 120,
  buttonText?: string,
  width?: number | string,
  height?: number | string,
  id?: string,
  Label?: string,
  selectedText?: number,
  value?: any,
  onPageChange?: any;
  row: number | 10,
  first?: number | '0',
  onChange?: any;
  onClick?: any;
  [x:string]: any;
}

class HPHPaginator extends Component<PaginatorProps, {}, any> {
  state = {
    First : 0,
    customRows : this.props.row | 10,
    currentPage : 1,
  };

  onCustomPageChange1 = (event:any) => {
    this.setState({ First: event.first });
    this.setState({ customRows: event.rows });
    this.setState({ currentPage: event.page + 1 });
  };

  onPageInputChange = (event:any) => {
    this.setState({ currentPage :  event.target.value });
  };

  onPageInputKeyDown = () => {
    const curPage = parseInt(this.state.currentPage.toString());
    if (curPage < 0 || curPage > this.props.totalRecords) {
    } else {
      const curFirst = this.state.currentPage ? this.state.customRows * (curPage - 1) : 0;
      this.setState({ First: curFirst });
    }
  };

  render(){
    const {
      id = 'Paginator',
      inputText = 'p.',
      buttonText = 'Go',
      width = '750px',
      height = '70px',
      totalRecords = 120,
      selectedText = '3',
      Label = 'Selected',
      ...rest
    } = this.props;

    const { First, customRows, currentPage } = this.state;

    const  template1 = {
      layout:
      'PrevPageLink PageLinks NextPageLink CurrentPageReport',
      CurrentPageReport: () => {
        return (
          <StyledSpan id={id}>
              <StyledText>{inputText}</StyledText>
              <StyledInput type='number'
              value={currentPage}
              onChange={this.onPageInputChange}/>
              <StyledButton onClick={this.onPageInputKeyDown}>
                  {buttonText}
              </StyledButton>
          </StyledSpan>
        );
      },
    };

    const  Pagestyle = {
      width: width,
      height: height,
    };


    return (
      <StyledDiv style={Pagestyle} id={id}>
        <StyledBox>
          <StyledRecord>{'Total: ' + totalRecords + ' items'}</StyledRecord>
          <StyledSelected>{Label}: {selectedText}</StyledSelected>
        </StyledBox>
          <StyledPagination 
            template={template1}
            first={First}
            rows={customRows}
            totalRecords={totalRecords}
            onPageChange={this.onCustomPageChange1}
            {...rest}/>
      </StyledDiv>
    );
  }
}
export { HPHPaginator };