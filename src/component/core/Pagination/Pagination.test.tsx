
import { HPHPaginator } from "./Pagination";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
    id : 'Pagination',
    inputText : 'page',
    buttonText : 'click',
    width : '140px',
};

describe('Test', () => {
    test("Pagination", () => {
        render(<HPHPaginator totalRecords={130} row ={5}/>);
    });
    test("Pagination id", () => {
        render(<HPHPaginator totalRecords={130} row ={5} id={Props.id} />);
        const container = document.querySelector('#Paginator');
        expect(container).toBeNull();
    });
    test("Pagination total records", () => {
        render(<HPHPaginator totalRecords={130} row ={5} inputText={Props.inputText}/>);
        const container = document.querySelector('#Paginator');
        expect(container).toBeTruthy();
    });
    test("Pagination button text", () => {
        render(<HPHPaginator totalRecords={130} row ={5} buttonText={Props.buttonText} />);
        const container = document.querySelector('#Paginator');
        expect(container).toBeTruthy();
    });
    test("Pagination width", () => {
        render(<HPHPaginator totalRecords={130} row ={5} width={Props.width} />);
        const container = document.querySelector('#Paginator');
        expect(container).toBeTruthy();
    });
});
