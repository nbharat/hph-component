
import * as React from 'react';
import { getImgUrl } from '../../../common/getImgUrl';


export interface Props {
  id?: string,
  width?: string | '24px',
  height?: string | '24px',
  fileName?: string | 'Icon-doc',
  className?: string | 'Icon-doc',
  onMouseOver?: (e: any) => void
  onMouseOut?: (e: any) => void;
  onClick?: (e: any) => void;
  [x:string]: any;
}

const SVGIcon: React.FC<Props> = ({
  id = 'idsvgIcon',
  width = '24px',
  height,
  fileName,
  onMouseOver,
  onMouseOut,
  onClick,
  ...rest
}) => {
  return <>
      <img
        onClick={onClick}
        onMouseOver={onMouseOver}
        onMouseOut={onMouseOut}
        src={getImgUrl(fileName)}
        id={id}
        width={width}
        height={height}
        {...rest}
      />
      </>;
};
export { SVGIcon };