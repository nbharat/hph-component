
//Display Variables
export const df = 'flex';
export const dif = 'inline-flex';
export const db = 'block';
export const dib = 'inline-block';
export const dn = 'none';


//Primary Colors
export const HPHSkyBlue = '#009BDE';
export const HPHSkyBlueDark = '#0072A3';
export const HPHSkyBlueLight = '#C2EDFF';
export const HPHSeaBlue = '#002E6D';
export const HPHSeaBlueDark = '#C5E0F2';
export const HPHHorizonBlue = '#9ACAEB';

//Secondry Colors
export const HPHOrange = '#EE7523';
export const HPHOrangeDark = '#AB4C0D';
export const HPHOrangeLight = '#FBDBC6';
export const HPHYellow = '#FFC627';
export const HPHYellowDark = '#A37800';
export const HPHYellowLight = '#FFEFC2';
export const HPHGreen = '#54BBAB';
export const HPHGreenDark = '#348377';
export const HPHGreenLight = '#D3EEEA';
export const HPHRed = '#DB4D59';
export const HPHRedLight = '#F5CCD0';

//Typography Colors
export const HPHBlack = '#000000';
export const HPHWhite = '#FFFFFF';
export const HPHGrey = '#777777';
export const HPHGreyDark = '#222222';
export const HPHGreyLight = '#CCCCCC';
export const HPHWhiteSmoke = '#F7F7F7';

//Backgrounds
export const Background1 = '#F7F7F7';
export const Background2 = '#FFFFFF';
export const Background3 = 'linear-gradient(0deg, rgba(247,247,247,1) 0%, rgba(255,255,255,1) 100%)';
export const Background4 = '#FAFDFF';
export const Background5 = '#F5FBFF';
export const Background6 = 'linear-gradient(180deg, #F9F9F9 0%, rgba(247, 247, 247, 0) 100%)'; 
export const Background7 = 'linear-gradient(180deg, #F9F9F9 0%, #FFFFFF 100%)';   
export const Background8 = '#F1F2F3';
export const Background9 = 'linear-gradient(180deg, #f7f7f7 0%, #ffffff 100%)';
// Font
export const IncludeFont = `
@import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,900;1,400;1,500;1,600;1,700;1,900&display=swap');
`;
export const MontserratFont = '"Montserrat", serif';

// Components
export const RightClickMenuItemLink = '#F1F2F3';
export const ContainerBoxShadow = '0px 2px 2px 0px rgba(109, 140, 171, 0.2)';