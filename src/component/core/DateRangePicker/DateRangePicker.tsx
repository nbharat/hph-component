/* eslint-disable import/extensions */
import React from 'react';
import { IconButton } from '../IconButtons/IconButtons';
import { 
  StyledInputWrapper, StyledInputLabel, StyledRequired,
  StyledCalendar,  StyledInputErrorMsg, DateRangePickerGlobalStyles,
} from '../styled/dateRangePicker.styled';
import {
  HPHRed,
} from '../Colors';

interface Props {
  [x:string]: any;
}

const DateRangePicker: React.FC<Props> = ({  
  id = 'DatePicker',
  label = 'Label',
  disabled = false,
  required = false,
  helpIcon = false,
  errorMessage = '',
  toolTipText = 'Date format dd/mm/yyyy',
  date,  
  placeholder = 'dd/mm/yyyy - dd/mm/yyyy',
  dateFormat = 'dd/mm/yy',
  width = '200px',
  onChange,
  ...rest
}): any => <StyledInputWrapper>
  <DateRangePickerGlobalStyles />
  <StyledInputLabel>
      {label} {required ? <StyledRequired>*</StyledRequired> : ''}
      {helpIcon && <IconButton fileName={'Icon-help'} size={'small'}  
        toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
        toolTipText={toolTipText} {...rest}
      />} 
  </StyledInputLabel>   
  <StyledCalendar         
    disabled={disabled}
    id={id}
    value={date}
    showIcon
    placeholder={placeholder}
    dateFormat={dateFormat}    
    numberOfMonths={2}
    selectionMode="range"
    onChange={(e) => onChange && onChange(e.value)}  
    style={{ width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }}   
    {...rest} 
  />
  <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
</StyledInputWrapper>;

export { DateRangePicker };