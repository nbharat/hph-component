import { DateRangePicker } from "./DateRangePicker"
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
    id:"mask",
    helpIcon: true,
    width: "200px", 
    disabled:false,
    required:true,
    errorMessage: "errorMsg",
  
  };
describe('Test', () => {
    test("HelpIcon is not disabled", () => {
        render(<DateRangePicker />);
    });
    
    test("HelpIcon is not disabled", () => {
        render(<DateRangePicker id={Props.id} disabled={false}/>);

        const container = document.querySelector('#mask');
        expect(container).toBeTruthy();
    });
    test("HelpIcon is disabled", () => {
        render(<DateRangePicker id={Props.id} disabled = {true}/>);

        const container = document.querySelector('#mask');
        expect(container).toBeTruthy();
    });
    test("Calendar Date width", () => {
        render(<DateRangePicker id={Props.id} width={Props.width} />);

        const container = document.querySelector('#mask');              
        expect(container).toBeTruthy();
    });
    test("Calendar Date helpIcon true", () => {
        render(<DateRangePicker id={Props.id} width={Props.width} helpIcon={Props.helpIcon} />);

        const container = document.querySelector('#mask');              
        expect(container).toBeTruthy();
    });
    test("Calendar Date required true", () => {
        render(<DateRangePicker id={Props.id} width={Props.width} helpIcon={Props.helpIcon} required={Props.required} />);

        const container = document.querySelector('#mask');              
        expect(container).toBeTruthy();
    });
    test("Calendar Date errorMessage", () => {
      render(<DateRangePicker id={Props.id} width={Props.width} errorMessage={Props.errorMessage} required={Props.required} />);
      const container = document.querySelector('#mask');              
      expect(container).toBeTruthy();
    });

    test("Calendar Date errorMessage Blank", () => {
      render(<DateRangePicker id={Props.id} width={Props.width} errorMessage={''} />);
      const container = document.querySelector('#mask');              
      expect(container).toBeTruthy();
    });

});