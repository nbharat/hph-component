/* eslint-disable import/extensions */
import React from 'react';
import { StyledMenuItemTemplate, StyledMenuItemTemplateIcon, StyledWrapper, StyledMenuItemTemplateIconButton, StyledMenuItemTemplateValue } from '../styled/tags.styled';

export interface TagsProps{
  theme?: string | 'Ports Sea Blue' | 'Ports Sky Blue' | 'Ports Aqua Green' | 'Ports Sunray Yellow' | 'Ports Sunset Orange' | 'Alert Red',
  width?: number | string,
  [x:string]: any;
}

const Tags: React.FC<TagsProps> = ({
  label = 'Default',
  theme = 'Ports Sea Blue',
  enableIcon = false,
  remove = false,
  icon = 'Icon-tick',
  rounded = false,
  draggable = false,
  onRemove,
  width = 'auto',
  onDragOver,
  ...rest
}): any => {

  return (
      <StyledMenuItemTemplate width={width} theme={theme} rounded={rounded} {...draggable ? { draggable } : {}} {...rest}>
        <StyledWrapper>
          {draggable && <StyledMenuItemTemplateIcon fileName="Icon-drag" theme={theme} />}
          {enableIcon && <StyledMenuItemTemplateIcon fileName={icon} theme={theme} />}
          <StyledMenuItemTemplateValue>{label}</StyledMenuItemTemplateValue>
        </StyledWrapper>
        {remove && <StyledMenuItemTemplateIconButton fileName="Icon-cross-small" theme={theme} remove onClick={onRemove} />}
      </StyledMenuItemTemplate>
  );
};
export { Tags } ;