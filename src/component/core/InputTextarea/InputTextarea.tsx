import React from 'react';
import{ StyledInputWrapper, StyledInputLabel, StyledRequired, StyledInputTextarea, StyledInputErrorMsg} from '../styled/inputTextarea.styled';
import { HPHRed } from '../Colors';
import { IconButton } from '../IconButtons/IconButtons';
export interface TextareaProps {
  element?: any,
  placeholder?:string | '',
  value?: string,
  label?: string,
  disabled?:boolean | false,
  helpIcon?:boolean | false,
  required?:boolean | false,
  errorMessage?:string,
  toolTipText?:string,
  helperMessage?:string,
  rows?:number,
  autoResize?:boolean | false,
  cols?:number,
  size?:string,
  width?:string | '200px',
  className?: string | 'p-badge',
  onChange?(e:any): void;
  [x:string]: any;
}


const HPHInputTextarea: React.FC<TextareaProps> = ({
  element,
  placeholder = 'Type something...',
  className,
  label = 'label',
  disabled = false,
  helpIcon = false,
  required = false,
  errorMessage = '',
  toolTipText = 'Helper Message',
  rows = 4,
  cols = 30,
  autoResize = false,
  width = '200px',
  onChange,
  ...rest
}) : any => (
  <>
    <StyledInputWrapper>
      <StyledInputLabel>
        {label} {required && <StyledRequired>*</StyledRequired>}
        {helpIcon && <IconButton fileName={'Icon-help'} size={'small'}
          toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
          toolTipText={toolTipText} {...rest} />}
      </StyledInputLabel>
      <StyledInputTextarea
        placeholder={placeholder}
        rows={rows}
        cols={cols}
        disabled={disabled}
        ref={element}
        onChange={onChange}
        style={{ width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }}
        required={required}
        {...autoResize ? { autoResize } : {}}
        {...rest}
        />
      <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
    </StyledInputWrapper>
  </>
);
export { HPHInputTextarea };