import { HPHInputTextarea } from "./InputTextarea";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'
const Props = {
    placeholder: "",
    value:"22",
    label:"Lable",
    disabled:false,
    errorMessage:"",
    rows:4,
    autoResize:true,
    cols:32,
    size:"Sunset-Orange",
    className:"p-badge"
  };
describe('Test', () => {
    test("iconEnabledOnHover", () => {
        render(<HPHInputTextarea />);
    });

    test("all props", () => {
        render(<HPHInputTextarea {...Props} />);
    });

    test("required", () => {
        render(<HPHInputTextarea {...Props} required={true} />);
    });

    test("helpIcon", () => {
        render(<HPHInputTextarea {...Props} helpIcon={true} />);
    });

    test("errorMessage", () => {
        render(<HPHInputTextarea {...Props} errorMessage="Error" />);
    });
});
