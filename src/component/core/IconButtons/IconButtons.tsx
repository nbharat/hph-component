import React from 'react';
import { SVGIcon } from '../SVGIcon/SVGIcon';
import { Tooltips } from '../Tooltips/Tooltips';
import { StyledButton, sizeObj } from './../styled/iconButtons.styled';

export interface IconButtonsProps {
  idIcom?: string | 'idIcon',
  size?: string | 'small' | 'medium' | 'large',
  classNameBadge?: string,
  value?: string | '1',
  color?: string | 'Alert-Red' | 'Sky-Blue' | 'Sea-Blue' | 'Horizon-Blue' | 'Aqua-Green' | 'Sunray-Yellow' | 'Sunset-Orange',
  idToolTip?: string | 'iconTooltipID',
  toolTipText?: string,
  fileName?:string | 'Icon-doc',
  classNameToolTip?: string,
  toolTipPlacement?: | 'top' | 'right' | 'left' | 'bottom',
  toolTipArrow?: boolean | true | false,
  disabled?: boolean | false,
  enabledActive?: boolean | false,
  onMouseOver?: (e: any) => void
  onMouseOut?: (e: any) => void,
  onClick?: (e: any) => void,
  [x:string]: any;
}
  

const fileNameEnableDisable = (disabled: Boolean, enabledActive: Boolean) => {
  return (disabled && enabledActive) || disabled  ? '-disable' : enabledActive  ? '-enable' : '';
};
const IconButton: React.FC<IconButtonsProps> = ({
  idIcom = 'idIcon',
  size = 'medium',    
  idToolTip,
  fileName = 'Icon-doc',
  toolTipPlacement = 'bottom',
  toolTipText = '',
  toolTipArrow = true,
  disabled = false,
  enabledActive = false,
  onMouseOver,
  onMouseOut,
  onClick,
  ...rest
}) => {
  return (
        <>
            <StyledButton 
            type='button'
            disabled={disabled}
            disabledIcon={disabled}
            >
                <SVGIcon
                    onClick={onClick}
                    {...enabledActive ? { onMouseOver } : { onMouseOut } }
                    id={idIcom}
                    fileName={fileName + fileNameEnableDisable(disabled, enabledActive)}
                    width={sizeObj[size]}
                    height={sizeObj[size]}
                    {...rest}
                />
                {!disabled && <Tooltips
                    toolTiptext={toolTipText != '' ? toolTipText : fileName}
                    id={idToolTip || 'iconTooltipID'}
                    size={size}
                    toolTipPlacement={toolTipPlacement}
                    toolTipArrow={toolTipArrow}
                    {...rest}
                />
                }
            </StyledButton>
        </>
  );
};
export { IconButton };