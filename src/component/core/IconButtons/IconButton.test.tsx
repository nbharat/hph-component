import { IconButton } from "./IconButtons";
import { render, fireEvent } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
    fileName:"TestIcon-doc",
    idIcom: "idIcom",
    disabled: false,
    enabledActive: false,
    value: "2",
    size: "medium",
    color: "Alert-Red"
};
describe('Test Alert-Red', () => {
    test('Alert-Red', async () => {
        const { container } = render(<IconButton fileName={Props.fileName} disabled={false} enabledActive={false}  ></IconButton>);
        const component = container.firstChild;
        if (component) {
            const SvgIcon = component.firstChild;

            if (SvgIcon) {
                //expect(SvgIcon).toHaveClass("");
                fireEvent.mouseOver(component);
            }
        }
    })

})
describe('Test size', () => {
    test('small', () => {
        const { container } = render(<IconButton fileName={Props.fileName} size={'small'}></IconButton>);
        const component = container.firstChild;
        if (component) {
            const SvgIcon = component.firstChild;
            expect(SvgIcon).toHaveAttribute('height', '16px');
            expect(SvgIcon).toHaveAttribute('width', '16px');
        }
    })
    test('medium', () => {
        const { container } = render(<IconButton fileName={Props.fileName} size={'medium'}></IconButton>);
        const component = container.firstChild;
        if (component) {
            const SvgIcon = component.firstChild;
            expect(SvgIcon).toHaveAttribute('height', '24px');
            expect(SvgIcon).toHaveAttribute('width', '24px');
        }
    })

    test('large', () => {
        const { container } = render(<IconButton fileName={Props.fileName} size={'large'}></IconButton>);

        const component = container.firstChild;
        if (component) {
            const SvgIcon = component.firstChild;
            expect(SvgIcon).toHaveAttribute('height', '32px');
            expect(SvgIcon).toHaveAttribute('width', '32px');
        }
    })
})

describe('Test', () => {
    test("iconEnabledOnHover", () => {
        render(<IconButton
            idIcom={Props.idIcom}
            fileName={Props.fileName}
            value={Props.value}
            size={Props.size}
            disabled={Props.disabled}
            enabledActive={Props.enabledActive}
        />);

        // const container = document.querySelector('.iconEnabledOnHover');
        // if (container) {
        //     expect(container).toHaveClass("iconEnabledOnHover");
        // }

    });
    // test("disabled", () => {
    //     render(<IconButton
    //         fileName={Props.fileName}
    //         idIcom={Props.idIcom}
    //         classNameSVG={Props.classNameSVG}
    //         disabled={Props.disabled}
    //         enabledActive={Props.enabledActive}
    //         value={Props.value} size={Props.size} />);

    //     const container = document.querySelector('#idIcom');
    //     if (container) {
    //         expect(container.className).toHaveClass("");
    //     }

    // });
    // test("idIcom", () => {
    //     render(<IconButton
    //         idIcom={Props.idIcom}
    //         fileName={Props.fileName}
    //         classNameSVG={Props.classNameSVG} value={Props.value} size={Props.size} />);

    //     const container = document.querySelector('#idIcom');
    //     if (container) {

    //         expect(container.className).toHaveClass("");
    //     }
    // });
    // test("iconEnabledOnHover", () => {
    //     render(<IconButton
    //         fileName={Props.fileName}  classNameSVG={Props.classNameSVG} value={Props.value} size={Props.size} />);

    //     const container = document.querySelector('.iconStyle');
    //     if (container) {
    //         expect(container).toHaveClass("iconStyle");
    //     }

    // });


    test("iconBadgeButton default size", () => {
        render(<IconButton />);
    });

    test("iconBadgeButton enabledActive", () => {
        render(<IconButton enabledActive={true} />);
    });

    test("iconBadgeButton disabled", () => {
        render(<IconButton disabled={true} />);
    });

    test("iconBadgeButton idTooltip", () => {
        render(<IconButton idToolTip="test" />);
    });

    // test("iconBadgeButton classNameSVG", () => {
    //     render(<IconButton classNameSVG="" />);

    //     const container = document.querySelector('#idIcon');
    //     if (container) {
    //         expect(container.className).toBeTruthy()
    //     }
    // });

    test("iconBadgeButton classNameBadge", () => {
        render(<IconButton />);
    });

    test("iconBadgeButton classNameSVG iconEnabledOnFocus", () => {
        render(<IconButton />);
    });
});
