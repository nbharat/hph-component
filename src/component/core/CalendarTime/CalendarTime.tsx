import React from 'react';
import{ StyledInputWrapper, StyledInputLabel, StyledRequired,StyledInputErrorMsg,StyledCalendar} from './../styled/calenderTime.styled';
import { IconButton } from '../IconButtons/IconButtons';
import { HPHRed } from '../Colors';

interface Props {
  id?: string;
  date?: string;
  element?: any;
  label?: string;
  placeholder?: string | '';
  dateFormat?: string | 'dd/mm/yy';
  disabled?:boolean | false,
  helpIcon?:boolean | false,
  required?:boolean | false,
  errorMessage?: string;
  toolTipText?: string;
  className?: string | 'p-badge';
  width?:string | '200px',
  size?: string | 'Sunset-Orange';
  [x:string]: any;
}

const CalendarTime: React.FC<Props> = ({
  element,
  id = 'time',
  className = '',
  label = 'Label',
  disabled = false,
  required = false,
  helpIcon = false,
  errorMessage = 'Error Message',
  toolTipText = 'Time hh:mm 24 hours format',
  date,  
  width = 'auto',
  ...rest
}): any => {
  return (
    <>
      <StyledInputWrapper>
        <StyledInputLabel>
            {label} {required ? <StyledRequired>*</StyledRequired> : ''}
            {helpIcon ? <IconButton fileName={'Icon-help'} size={'small'} 
              toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
              toolTipText={toolTipText}
              {...rest}
            /> : ''} 
        </StyledInputLabel>   
        <StyledCalendar
          type={ 'time' } 
          disabled={disabled}
          ref={element}
          id={id}
          value={date}
          onChange={(e) =>(date = e.target.value)}
          style={{ 'width':width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }}  
        />
        <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
      </StyledInputWrapper>
    </>
  );
};
export { CalendarTime };
