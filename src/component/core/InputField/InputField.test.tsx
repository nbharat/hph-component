import { InputField } from "./InputField";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
  disabled: false,
  placeholder:"",
  value: "2",
  size: "medium",
};
describe('Test', () => {
    test("iconEnabledOnHover", () => {
        render(<InputField {...Props} />);
    });

    test("iconBadgeButton default size", () => {
        render(<InputField {...Props}/>);
    });  

    test("iconEnabledOnHover medium", () => {
        render(<InputField {...Props} />);
        const container = document.querySelector('.iconStyle');
        if (container) {
            expect(container.className).toBe("medium");
        }
    });
    test("value", () => {
        render(<InputField  {...Props} />);
        const container = document.querySelector('.badgeStyle');
        if (container) {
            expect(container.className).toBe("p-badge p-component p-badge-no-gutter sc-bdvvtL ciazrG badgeStyle badgeStyle Alert-Red");
        }
    });
});
