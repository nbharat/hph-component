import React from 'react';
import { IconButton } from '../IconButtons/IconButtons';
import { StyledInputWrapper, StyledInputLabel, StyledRequired, StyledInputText, StyledInputErrorMsg } from './../styled/inputField.styled';
import { HPHRed } from '../Colors';
type TextType = 'number' | 'text';


export interface InputFieldProps {
  element?: any,
  placeholder?:string | '',
  type?:TextType,
  label?: string,
  helpIcon?:boolean | false,
  required?:boolean | false,
  disabled?:boolean | false,
  errorMessage?:string,
  toolTipText?:string,
  size?:string | 'Sunset-Orange',
  className?: string | 'p-badge',
  width?:string | '200px',
  onChange?:any;
  [x:string]: any;
}

const InputField: React.FC<InputFieldProps> = ({
  element,
  type = 'number',
  placeholder = type == 'number' ? 'Number' : 'Please type...',
  className,
  label = 'Label',
  disabled = false,
  helpIcon = false,
  required = false,
  errorMessage = '',
  toolTipText = 'Helper Message',
  width = '200px',
  onChange,
  ...rest
}) : any => {
  return (
   <>   
      <StyledInputWrapper>
          <StyledInputLabel>
            {label} {required ? <StyledRequired>*</StyledRequired> : ''}
            {helpIcon ? <IconButton fileName={'Icon-help'} size={'small'}
              toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
              toolTipText={toolTipText} {...rest} /> : ''}
          </StyledInputLabel>
          <StyledInputText
            placeholder={placeholder}
            type={type}
            disabled={disabled}           
            onChange={onChange} 
            style={{ width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }}   
          /> 
          <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
      </StyledInputWrapper>
   </>
  );
  
};
export { InputField };