/* eslint-disable import/extensions */
import React from 'react';
import { StyledSpan, StyledWrapper } from '../styled/lozenges.styled';
import { Tooltips } from '../Tooltips/Tooltips';

export interface LozengesProps{
  variation?: string | 'Ports Sea Blue' | 'Ports Sky Blue' | 'Ports Horizon Blue' | 'Ports Aqua Green' | 'Ports Sunray Yellow' | 'Ports Sunset Orange' | 'Alert Red',
  [x:string]: any;
}

const Lozenges: React.FC<LozengesProps> = ({
  label = 'Default',
  variation = 'Ports Horizon Blue',
  enableTooltip = false,
  toolTipPlacement = 'bottom',
  id = 'lozengesTooltip',
  tooltipSize = 'medium',    
  toolTipArrow = true,
  toolTiptext,
  ...rest
}): any => {
  return (
        <>
            <StyledWrapper>
              <StyledSpan variation={variation} {...rest}>{label.toUpperCase()}</StyledSpan>
              {enableTooltip && <Tooltips toolTiptext={toolTiptext ? toolTiptext.toUpperCase() : label.toUpperCase()} id={id} size={tooltipSize} toolTipPlacement={toolTipPlacement} toolTipArrow={toolTipArrow} />}                          
            </StyledWrapper>
        </>
  );
};
export { Lozenges } ;