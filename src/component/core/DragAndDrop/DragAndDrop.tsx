import React, { Component } from 'react';
import { StyledMain, StyledInnerDiv, StyledSecondInnerDiv, StyledDiv } from '../styled/dragAndDrop.styled';
import { Tags } from '../Tags/Tags';

export interface DragAndDropProps {
  selectedOption?: any[];
  unselectedOption?: any[];
  orientation?: 'horizontal' | 'vertical',
  width?: number | string,
  height?: number | string,
  [x:string]: any;
}

class HPHDragAndDrop extends Component<DragAndDropProps, {}, any> {
  state = {
    selectedOptions : this.props.selectedOption || [],
    unselectedOptions : this.props.unselectedOption || [],
  };

  removeHandler = (item: any) => {

    const updatedTags: any = this.state.selectedOptions?.map((tag:any) => {
      if (tag.id === item.id) {
        tag.visible = false;
      }
      return tag;
    });

    const updatedUnselectedTags: any = this.state.unselectedOptions?.map((tag:any) => {
      if (tag.id === item.id) {
        tag.visible = false;
      }
      return tag;
    });

    this.setState({ selectedOptions: updatedTags });
    this.setState({ unselectedOptions: updatedUnselectedTags });
  };

  onDragOver = (ev: any) => {
    ev.preventDefault();
  };

  onDrop = (ev: any) => {
    debugger;
    let selectid = Number(ev.dataTransfer.getData('id'));
    const dragTag: any = this.state.selectedOptions.find((tag:any) => tag.id === selectid);
    // const dropTag: any = this.state.unselectedOptions.find((tag:any)=> tag.id === Number(ev.currentTarget.id));

    this.state.selectedOptions = this.state.selectedOptions.filter((tag:any) => tag.id != selectid);
    
    const dragTagOrder = dragTag?.id;
    // const dropTagOrder = dropTag?.id;
    const newTagState = this.state.selectedOptions?.map((tag:any) => {
      // if (tag.id === selectid) {
      //   tag.order = dropTagOrder;
      // }
      
      if (tag.id === Number(ev.currentTarget.id)) {
        tag.id = dragTagOrder;
      }
      return tag;
    });
    
    this.state.unselectedOptions.push(dragTag);
    this.setState({ selectedOptions : newTagState });
    
  };

  onDragStart = (ev: any, id: any) => {
    ev.dataTransfer.setData('id', id);
  };

  render(){
    const {
      id = 'drag&drop',
      orientation = 'horizontal',
      width =  '500px',
      height = '100px',
      ...rest
    } = this.props;

    const { selectedOptions, unselectedOptions } = this.state;
    return (
      <StyledDiv id={id}>
      <StyledMain {...rest}
      onDragOver={(ev: any) => this.onDragOver(ev)}
      onDrop={(e: any) => this.onDrop(e)}
      width={width}
      height={height}
      orientation={orientation}>
        <StyledInnerDiv 
        orientation={orientation}>
        {selectedOptions.map(
          (item : any, i: number) =>
            item.visible && <Tags
            remove= {true} 
            onDragStart = {(e: any) => this.onDragStart(e, item.id)}
            onDrop={(e: any) => this.onDrop(e)}
            draggable = {true}
            key={i}
            {...item}
            onRemove={() => this.removeHandler(item)}
            style={{ margin : '10px', height: '28px' }}/>,
        )}
        </StyledInnerDiv>
        <StyledSecondInnerDiv
        orientation={orientation}>
        {unselectedOptions.map(
          (item : any, i: number) =>
            item.visible && <Tags
            remove= {true} 
            onDragStart = {(e: any) => this.onDragStart(e, item.id)}
            onDrop={(e: any) => this.onDrop(e)}
            draggable = {true}
            key={i}
            {...item}
            onRemove={() => this.removeHandler(item)}
            style={{ margin : '10px', height: '28px' }}/>,
        )}
        </StyledSecondInnerDiv>
      </StyledMain>
      </StyledDiv>
    );
  }
}
export  { HPHDragAndDrop } ;