import { HPHDragAndDrop } from "./DragAndDrop";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom';

const Props = {
    id : 'dragdrop',
    orientation : 'horizontal',
    width :  '500px',
    height : '100px',
}

describe('Test', () => {
    test("Drag And Drop id", () => {
        render(<HPHDragAndDrop />);
    });
    test(" Drag And Drop id", () => {
        render(<HPHDragAndDrop id={Props.id} />);
        const container = document.querySelector('#dragdrop');
        expect(container).toBeTruthy();
    });
    test(" Drag And Drop orientation", () => {
        render(<HPHDragAndDrop id={Props.id} orientation={'horizontal'}/>);
        const container = document.querySelector('#dragdrop');
        expect(container).toBeTruthy();
    });
    test(" Drag And Drop orientation", () => {
        render(<HPHDragAndDrop id={Props.id} orientation={'vertical'}/>);
        const container = document.querySelector('#dragdrop');
        expect(container).toBeTruthy();
    });
    test(" Drag And Drop width", () => {
        render(<HPHDragAndDrop id={Props.id} width={Props.width}/>);
        const container = document.querySelector('#dragdrop');
        expect(container).toBeTruthy();
    });
    test(" Drag And Drop height", () => {
        render(<HPHDragAndDrop id={Props.id} height={Props.height}/>);
        const container = document.querySelector('#dragdrop');
        expect(container).toBeTruthy();
    });
});