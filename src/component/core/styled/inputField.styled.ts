import styled from 'styled-components';
import { InputFieldProps } from './../InputField/InputField'
import { HPHSeaBlue, HPHRed, HPHWhite, HPHGreyDark, HPHSkyBlue, HPHGreyLight, MontserratFont } from '../Colors';
import { InputText } from 'primereact/inputtext';

export const StyledInputWrapper = styled.div`
  display:flex;
  flex-direction:column;
  position:relative;
  .p-inputtext:enabled:focus{
    outline:none;
    box-shadow:none;
    border-color:${HPHSkyBlue};
  }
  .p-inputtext{ 
    &:enabled:hover{
      border-color:${HPHSkyBlue};
    }
  }
  .p-inputtext:disabled{
    opacity:1;
  }
`;


export const StyledRequired = styled.em`
  color:${HPHRed};   
  font-style: initial;
  margin-left: 2px;
`;
export const StyledInputLabel = styled.label`
    color:${HPHSeaBlue};    
    display: block;
    font-family: ${MontserratFont}; 
    font-size:0.875rem;
    font-weight: 700;
    letter-spacing: 0px;
    line-height: 1.2;
    text-transform: capitalize;
    display:flex;
    align-items: center;
    button {
      margin-left:3px;
      img
      {
        padding:0;
        width:14px;
        height:14px;   
        opacity: 0.6;     
        &.iconEnabledOnHover:not(.disable):hover{
          border-radius:0;
          background:none;
        }
      }
    }
`;

export const StyledInputText = styled(InputText)<InputFieldProps>`    
    &.p-inputtext{
      border:0;      
      border-bottom:1px solid ${HPHGreyDark};
      border-radius: 0;     
      background-color:${HPHWhite};
      color:${HPHGreyDark};
      font-size:1rem;
      font-weight:500;
      font-family: ${MontserratFont};    
      padding: 0.375rem 0rem;
    }   
    &::-webkit-input-placeholder {
      color: ${HPHGreyLight};
      font-size:1rem;
      font-weight:500;
    }    
    &:-ms-input-placeholder { 
      color: ${HPHGreyLight};
      font-size:1rem;
      font-weight:500;
    }    
    &::placeholder {
      color: ${HPHGreyLight};
      font-size:1rem;
      font-weight:500;
    }    
    &:disabled{
      border-color: ${HPHGreyLight};
      color:${HPHGreyLight};
    }
    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin: 0; 
    }
`;

export const StyledInputErrorMsg = styled.span`
  color: ${HPHRed};
  display: block;
  font-family: ${MontserratFont};
  font-size: 0.875rem;
  font-style: italic;
  font-weight: 500;
  line-height: 1.2;    
  margin-top: .25rem;    
  position: absolute;    
  top: 100%;
  white-space: nowrap;
`;
export const StyledColorPickerWrapper = styled.div`
    width: 24px;
    height: 24px;
    border-radius:50%;
    overflow:hidden;
    margin: 0.5rem 0rem;
    padding: 0;
`;
export const StyledColorPicker = styled.input`    
  padding: 0;
  border: 0;
  width: 35px;
  height: 35px;
  cursor: pointer;
  position: relative;
  top: -5px;
  left: -5px;
  &:disabled{
    pointer-events: none;
  }
`;