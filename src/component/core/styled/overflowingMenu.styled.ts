import styled, { css } from 'styled-components';
import { Menu } from 'primereact/menu';
import { HPHSkyBlue, HPHWhiteSmoke, HPHWhite, Background9, HPHGreyDark, RightClickMenuItemLink, MontserratFont  } from '../Colors';
import { OverflowingMenuProps} from '../OverflowingMenu/OverflowingMenu'
export const StyledBox = styled.div`
    position: relative;
    width: 30px;
    margin: 0 auto;
    height: 30px;

`;
const menuPosition = {
  'right': css`
  position: absolute;
  top: -25px;
  left: 100%;
  margin-left: 16px;
  &::after{
    content: " ";
    width: 0; 
    height: 0; 
    border-top: 1.25rem solid transparent;
    border-bottom: 1.25rem solid transparent;
    border-right: 1.25rem solid ${HPHWhiteSmoke};
    filter: drop-shadow(0px 0px 0px #6D8CAB);
    position: absolute;
    top:1.25rem;
    right: 100%;
  }
`,
  'left': css`
  position: absolute;
  left: 0;
  top: 0;
  transform: translateX(-100%);
  margin-left: -16px;
  top: -25px;
  &::after{
    content: " ";
    width: 0; 
    height: 0; 
    border-top: 1.25rem solid transparent;
    border-bottom: 1.25rem solid transparent;
    border-left: 1.25rem solid ${HPHWhiteSmoke};
    filter: drop-shadow(0px 0px 0px #6D8CAB);
    position: absolute;
    top:1.25rem;
    left:100%;    
  }
`,
  'topLeft': css`
  position: absolute;
  right: -25px;
  top: 0;
  transform: translateY(-100%);
  margin-top: -18px;
  &::after{
    content: " ";
    width: 0; 
    height: 0; 
    border-left: 1.25rem solid transparent;
    border-right: 1.25rem solid transparent;
    border-top: 1.25rem solid ${HPHWhiteSmoke};
    filter: drop-shadow(0px 0px 0px #6D8CAB);
    position: absolute;
    top:100%;
    right: 1.25rem;
  }
`,
  'topRight': css`
  position: absolute;
  left: -25px;
  top: 0;
  transform: translateY(-100%);
  margin-top: -18px;
  &::after{
    content: " ";
    width: 0; 
    height: 0; 
    border-left: 1.25rem solid transparent;
    border-right: 1.25rem solid transparent;
    border-top: 1.25rem solid ${HPHWhiteSmoke};
    filter: drop-shadow(0px 0px 0px #6D8CAB);
    position: absolute;
    top:100%;
    Left: 1.25rem;
  }
`,
  'bottomLeft': css`
  position: absolute;
  top: 100%;
  right: -25px;
  margin-top: 20px;
  &::after{
    content: " ";
    width: 0; 
    height: 0; 
    border-left: 1.25rem solid transparent;
    border-right: 1.25rem solid transparent;
    border-bottom: 1.25rem solid ${HPHWhiteSmoke};
    filter: drop-shadow(0px 0px 0px #6D8CAB);
    position: absolute;
    bottom:100%;
    right:1.25rem;
}
`,
  'bottomRight': css`
  position: absolute;
  top: 100%;
  left: -25px
  margin-top: 20px;
  &::after{
  content: " ";
    width: 0; 
    height: 0; 
    border-left: 1.25rem solid transparent;
    border-right: 1.25rem solid transparent;
    border-bottom: 1.25rem solid ${HPHWhiteSmoke};
    filter: drop-shadow(0px 0px 0px #6D8CAB);
    position: absolute;
    bottom:100%;
    Left: 1.25rem;
  }
  `,
};

const getMenuPosition = (props) => menuPosition[props.position];
const getWidth = (props) => props.width && css`width: ${props.width}`;
const getheight = (props) => props.height && css`height: ${props.height}`;
const getpaddingLeft = (props) => props.paddingLeft && css`padding-left: ${props.paddingLeft}`;
const getpaddingRight = (props) => props.paddingRight && css`padding-right: ${props.paddingRight}`;
const getpaddingTop = (props) => props.paddingTop && css`padding-top: ${props.paddingTop}`;
const getpaddingBottom = (props) => props.paddingBottom && css`padding-bottom: ${props.paddingBottom}`;

export const StyledMenu = styled(Menu)<OverflowingMenuProps>`
&.p-menu{
    position : absolute;
    width: auto;
    height:auto;
    white-space: nowrap;
    display: flex;
    border: 1px solid ${HPHWhiteSmoke};
    box-shadow: 0px 0px 3px -1px rgb(109 140 171);
    background: ${Background9};
    overflow: visible;
    flex-direction: column
    border-radius: 8px;
   
    ${getMenuPosition}
    ${getWidth}
    ${getheight}
    ${getpaddingLeft}
    ${getpaddingRight}
    ${getpaddingTop}
    ${getpaddingBottom}
    
        ul{
            padding: 0px 0;
            .p-menuitem{
                padding: 0;               
            }
            .p-menuitem-link {
                padding: 10px 25px 10px 25px;
                background-color: transparent;
                color: ${HPHGreyDark};   
                position: relative;  
                font-size: 16px;
                width:auto;
                font-weight: 500;
                font-family: ${MontserratFont};             
                &:hover{
                    background-color: ${RightClickMenuItemLink};
                    > .p-menuitem-text{
                        color:  ${HPHSkyBlue};
                    }   
                }
            }
        }      
}  

`;