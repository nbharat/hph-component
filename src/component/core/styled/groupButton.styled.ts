import styled, { css } from 'styled-components';
import { HPHWhite, HPHRed, Background8, HPHGrey, HPHSkyBlue, MontserratFont, HPHSeaBlue, Background2, HPHGreyLight } from '../Colors';
import { SelectButton } from 'primereact/selectbutton';
import { GroupedButtonsProps } from '../GroupButton/GroupButton';
import { getImgUrl } from '../../../common/getImgUrl';

const ErrMsgStyle = (Props) => {
  if (Props.errorMessage) {
    return css`
      border: 1px solid ${HPHRed};
    `;
  }
};
const MidbuttonStyle = (Props) => {
  if (Props.multiple) {
    return css`
      border-radius: 4px;
      margin: 0px 2.5px;
      border-right:1px solid ${HPHGreyLight};
      ${ErrMsgStyle}
    `;
  }
};
const FirstbuttonStyle = (Props) => {
  if (Props.multiple) {
    return css`
        margin: 0px 2.5px 0px 0px;
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
        border-right:1px solid ${HPHGreyLight};
        ${ErrMsgStyle}
    `;
  }
};
const LastbuttonStyle = (Props) => {
  if (Props.multiple) {
    return css`
        margin: 0px 0px 0px 2.5px;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
        ${ErrMsgStyle}
    `;
  }
};
const FirstButton = (Props) => {
  if (!Props.multiple){
    return css`
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
    `;
  }
};
const LastButton = (Props) => {
  if (!Props.multiple){
    return css`
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
    `;
  }
};
const MiddleButton = (Props) => {
  if (!Props.multiple){
    return css`
      border-radius: 0;
    `;
  }
};
export const StyledDiv = styled.div<GroupedButtonsProps>`
    display : block;
    width: auto;
    height: auto;
    position: relative;
    padding: 0;
    border: 0;
    justify-content: center;
    align-items: center;
    align-content: center;
    cursor: pointer;
  }
`;

export const StyledInputErrorMsg = styled.span<GroupedButtonsProps>`
  color: ${HPHRed};
  display: block;
  font-family: ${MontserratFont};
  font-size: 0.875rem;
  font-weight: 500;
  line-height: 1.2;    
  margin-top: .25rem;    
  position: absolute;
  white-space: nowrap;
  top: 100%;
`;
export const StyledLabel = styled.div<GroupedButtonsProps>`
  font-weight: 700;
  color: ${HPHSeaBlue};
  display: block;
  font-family: ${MontserratFont};
  font-size: 14px;
  letter-spacing: 0px;
  margin-bottom:14px;
  line-height: 1.2;
  align-items: center;
`;
export const StyledWrapper = styled.div`
  display:flex;
  align-items:center;
  flex-direction:column; 
`;
export const StyledName = styled.div`
  font-weight: 700;
  color: ${HPHSeaBlue};
  display: block;
  font-family: ${MontserratFont};
  font-size: 16px;
  letter-spacing: 0px;
  padding: 0px;
  line-height: 1.2;
  width: 120px;
  word-break: break-word;
`;

const getIcon = (props) => props.icon && css `mask-image: url(${getImgUrl(props.icon)})`;
  
export const StyledIcon = styled.div<GroupedButtonsProps>`
  width: 45px;
  height: 45px;
  mask-size: 100%;
  margin-bottom: 11px;
  padding: 3px;
  -webkit-mask-size: 100%;
  background:${HPHSeaBlue};
  ${getIcon};
`;
 
export const StyledButton = styled(SelectButton)<GroupedButtonsProps>`
&.p-selectbutton{
  .p-button{
    width: 8.75rem;
    height: auto;
    border: 1px solid ${HPHGreyLight};
    border-radius: 4px;
    flex-direction:column;
    background: ${Background2};
    font-size: 14px;
    font-family: ${MontserratFont};    
    ${ErrMsgStyle}
    &.p-component{
        padding: 20px 20px 20px 20px;
    }
    &:focus{
        box-shadow:none;
    }
    &.p-disabled{
      opacity:inherit;
      background: ${Background8};
      border: 1px solid ${HPHGreyLight};
      ${StyledName}{
          color:${HPHGreyLight};
      }
      ${StyledIcon}{
          background:${HPHGreyLight};
      }
      ${ErrMsgStyle}        
    }
    &.p-highlight{
      background: ${HPHSkyBlue};
      border: 1px solid ${HPHSkyBlue};
      ${StyledName}{
          color:${HPHWhite};
      }
      ${StyledIcon}{
          background:${HPHWhite};
      }
      ${ErrMsgStyle}
        &.p-disabled{
          background: ${HPHGreyLight};
          border: 1px solid ${HPHGrey};
          ${StyledName}{
              color:${HPHGrey};
          }
          ${StyledIcon}{
              background:${HPHGrey};
          }
          ${ErrMsgStyle}
        }
        :hover{
          background: ${HPHSkyBlue};
          border: 1px solid ${HPHSkyBlue};
          ${StyledName}{
          color:${HPHWhite};
        }
        ${StyledIcon}{
          background:${HPHWhite};
        }
        ${ErrMsgStyle}
        }
      }
      &:not(.p-disabled):not(.p-highlight):hover{
        border-color:${HPHSkyBlue} !important;
        background: ${Background2} !important;
        ${StyledName}{
          color:${HPHSkyBlue};
        }
        ${StyledIcon}{
          background:${HPHSkyBlue};
        }
        ${ErrMsgStyle}
      }
  }
}
&.p-buttonset{
    display:flex;
    .p-button:not(:first-of-type):not(:last-of-type){
      ${MidbuttonStyle}
    }
    .p-button:not(:last-child){
      border-right:none;
    }
    .p-button:first-of-type{
      ${FirstbuttonStyle}
    }
    .p-button:last-of-type{
      ${ErrMsgStyle}
      ${LastbuttonStyle}
    }
    .p-button:first-of-type{
      ${FirstButton}
    }
    .p-button:not(:first-of-type):not(:last-of-type){
      ${MiddleButton}
    }
    .p-button:last-of-type{
      ${LastButton}
    }
};
`;