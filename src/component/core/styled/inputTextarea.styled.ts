import styled, { css } from 'styled-components';
import { HPHSeaBlue, HPHRed, HPHWhite, HPHGreyDark, HPHSkyBlue, HPHGreyLight, MontserratFont } from '../Colors';
import { InputTextarea } from 'primereact/inputtextarea';
import { TextareaProps } from '../InputTextarea/InputTextarea'

export const StyledInputWrapper = styled.div`
  display:flex;
  flex-direction:column;
  position:relative;
  .p-inputtextarea:enabled:focus{
    outline:none;
    box-shadow:none;
    border-color:${HPHSkyBlue};
  }
  .p-inputtextarea{ 
    &:enabled:hover{
      border-color:${HPHSkyBlue};
    }
  }
  .p-inputtextarea:disabled{
    opacity:1;
  }
`;

export const StyledRequired = styled.em`
  color:${HPHRed};   
  font-style: initial;
  margin-left: 2px;
`;
export const StyledInputLabel = styled.label`
    color:${HPHSeaBlue};    
    display: block;
    font-family: ${MontserratFont}; 
    font-size:0.875rem;
    font-weight: 700;
    letter-spacing: 0px;
    line-height: 1.2;
    margin-bottom: 0.5rem;
    text-transform: capitalize;
    display:flex;
    align-items: center;
    button {
      margin-left:3px;
      img
      {
        padding:0;
        width:14px;
        height:14px;   
        opacity: 0.6;     
        &.iconEnabledOnHover:not(.disable):hover{
          border-radius:0;
          background:none;
        }
      }
    }
`;
export const StyledInputTextarea = styled(InputTextarea)<TextareaProps>`
  &.p-inputtextarea{    
    border:1px solid ${HPHGreyDark};
    border-radius: 5px;     
    background-color:${HPHWhite};
    color:${HPHGreyDark};
    font-size:1rem;
    font-weight:500;
    font-family: ${MontserratFont};       
    height: 128px;
    padding: 0.375rem;
    overflow-y:auto;
  }   
  &::-webkit-input-placeholder {
    color: ${HPHGreyLight};
    font-size:1rem;
    font-weight:500;
  }    
  &:-ms-input-placeholder { 
    color: ${HPHGreyLight};
    font-size:1rem;
    font-weight:500;
  }    
  &::placeholder {
    color: ${HPHGreyLight};
    font-size:1rem;
    font-weight:500;
  }
  &:disabled{
    border-color: ${HPHGreyLight};
    color:${HPHGreyLight};
  }
`;

export const StyledInputErrorMsg = styled.span`
  color: ${HPHRed};
  display: block;
  font-family: ${MontserratFont};
  font-size: 0.875rem;
  font-style: italic;
  font-weight: 500;
  line-height: 1.2;    
  margin-top: .25rem;    
  position: absolute;    
  top: 100%;
  white-space: nowrap;
`;
