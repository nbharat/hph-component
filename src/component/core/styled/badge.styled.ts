import { Badge } from 'primereact/badge';
import styled, { css } from 'styled-components';
import { BadgeProps } from '../Badge/Badge';

const badgeSize = {
  'Badge-small': css`
      width: auto;
      min-width: 0.4375rem !important;
      height: 0.75rem !important;
      border-radius: 0.375rem;
      font-size: 0.625rem;
      line-height: 0.75rem !important;
      font-weight: 500;
      padding:0 0.25rem;
    `,
  'Badge-medium': css`
      width: auto;
      min-width: 0.4375rem !important;
      height: 0.875rem !important;
      border-radius: 0.4375rem;
      font-size: 0.75rem ;
      line-height: 0.875rem !important;
      font-weight: 500;
      padding:0 0.25rem;
    `,
  'Badge-large': css`
      width: auto;
      min-width: 0.65625rem !important;
      height: 1.3125rem !important;
      border-radius: 0.65625rem;
      font-size: 1rem;
      line-height: 1.3125rem !important;
      font-weight: 500;
      padding:0 0.375rem;
    `,
  'smallBadge': css`
      height: 0.75rem;
      border-radius: 0.375rem;
      font-size: 0.625rem;
      line-height: 0.75rem;
      font-weight: 500;
      padding:0 0.25rem;
      position:absolute;
      left: 50%;
      top: -15%;
      right: auto;
    `,
  'badgeStyle': css`
      min-width: 0.4375rem;
      height: 0.875rem;
      border-radius: 0.4375rem;
      font-size: 0.75rem;
      line-height: 0.875rem;
      font-weight: 500;
      padding:0 0.25rem;
      position:absolute;
      left: 60%;
      top: -10%;
    `,
  'largeBadge': css`
      width: auto;
      min-width: 0.65625rem;
      height: 1.3125rem;
      border-radius: 0.65625rem;
      font-size: 1rem;
      line-height: 1.3125rem;
      font-weight: 500;
      padding:0 0.375rem;  
    `,
};
  
const getBadgeSize = (props) => badgeSize[props.size];
  
const badgeColor = {
  'Alert-Red': css`background: #DB4D59 !important;`,
  'Sky-Blue': css`background: #009BDE !important;`,
  'Sea-Blue': css`background: #002E6D !important;`,
  'Horizon-Blue': css`background: #9ACAEB !important;`,
  'Aqua-Green': css`background: #54BBAB !important;`,
  'Sunray-Yellow': css`background: #FFC627 !important;`,
  'Sunset-Orange': css`background: #EE7523 !important;`,
};
  
const getBadgeColor = (props) => badgeColor[props.color];
  
  
export const StyledBadge = styled(Badge)<BadgeProps>`
    &.p-badge{
      display: inline-flex;
      justify-content: center;
      align-items: center;
      align-content: center;
      flex-wrap: nowrap;
      color: white;
      width: auto;
      position: relative;
      letter-spacing: 0rem;
      border-radius: 50%;
      min-width: initial;
      font-family: 'Montserrat', sans-serif;
      ${getBadgeSize}
      ${getBadgeColor}  
    }
  `;