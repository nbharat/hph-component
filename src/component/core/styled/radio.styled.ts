import { RadioButton } from 'primereact/radiobutton';
import styled, { css } from 'styled-components';
import { HPHGreyDark, MontserratFont, Background2, HPHSkyBlue, HPHGreyLight } from '../Colors';
import { RadioProps } from '../RadioButton/RadioButton';


const orientation = (Props) =>{
  if (Props.orientation === 'vertical'){
    return css`
    flex-direction: column;
    `;
  } else {
    return css`
    flex-direction: row;
    `;
  }
};
const labelDisable = (Props) =>{
  if (Props.disabled){
    return css`
      color: ${HPHGreyLight};
      `;
  }
};
  
const RadioDisable = (Props) =>{
  if (Props.disabled){
    return css`
      background: ${HPHGreyLight};
      `;
  }
};
  
export const StyledRadio = styled(RadioButton)<RadioProps>`
  &.p-radiobutton{
    width:16px;
    height:16px;
    .p-radiobutton-box{
      width:16px;
      height:16px;
      border: 1px solid ${HPHGreyDark}
       &.p-disabled{
        border-color: ${HPHGreyLight};
        background: ${Background2};
        }
      &:not(.p-disabled){
        &:hover{
          border-color: ${HPHSkyBlue};
          background: ${Background2};
          color: ${Background2};
        }
        &.p-focus{
          box-shadow: none;
          border: 1px solid ${HPHGreyDark}
        }
        &:not(.p-highlight):hover{
          border-color: ${HPHSkyBlue};
        }
      }
      &.p-highlight {
        border-color: ${HPHGreyDark};
        background: ${Background2};
        &:not(.p-disabled){
        &:hover{
            border-color: ${HPHSkyBlue};
            background: ${Background2};
        }
      }
        &.p-disabled{
          border-color: ${HPHGreyLight};
          background: ${Background2};
        }
      }
      .p-radiobutton-icon {
        width:10px;
        height: 10px;
        background-color: ${HPHSkyBlue};
        ${RadioDisable}
      }
    }
  }
  `;
  
export const StyledLabel = styled.label<RadioProps>`
  padding-left: 6px;
  font-weight: 500;
  font-size: 1rem;
  letter-spacing: 0px;
  font-family: ${MontserratFont};
  color: ${HPHGreyDark};
  text-transform: capitalize;
  position:relative;
  top:2px;
  ${labelDisable}
  `;
  
export const StyledDiv = styled.div<RadioProps>`
  // display : flex;
  width: auto;
  height: auto;
  position: relative;
  padding: 0.25rem 0.5rem 0.25rem 0;
  border: 0;
  justify-content: center;
  align-items: center;
  align-content: center;
  `;
export const StyledBox = styled.div<RadioProps>`
  ${orientation};
  width: auto;
  height: auto;
  position: relative;
  display: flex;
  border: 0;
  flex-wrap:wrap;
`;