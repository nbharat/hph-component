import styled, { css } from 'styled-components';
import { Badge } from 'primereact/badge';
import { AvatarStatusProps } from '../Avatar/AvatarStatus'

const avatarStatusSize = {
  'small': css`
      width: auto;
      min-width: 0.43rem;
      height: 0.43rem;
      border: 1px solid white;
      position: absolute;
      bottom: 0px;
      right: 0px;
      box-shadow: 0px 0px 3px 0px black;
  `,
  'xSmall': css`
      width: auto;
        min-width: 0.3rem;
        height: 0.3rem;
        border: 1px solid white;
        position: absolute;
        bottom: 0px;
        right: 0px;
        box-shadow: 0px 0px 2px 0px black;
  `,
  'medium': css`
      width: auto;
        min-width: 0.54rem;
        height: 0.55rem;
        border: 1px solid white;
        position: absolute;
        bottom: 1px;
        right: 0px;
        box-shadow: 0px 0px 4px 0px black;
    
  `,
  'large': css`
      width: auto;
      min-width: 0.6rem;
      height: 0.6rem;
      border: 1px solid white;
      position: absolute;
      bottom: 3px;
      right: 0px;
      box-shadow: 0px 0px 4px 0px black;
   `,
  'xLarge': css`
      width: auto;
      min-width: 1rem;
      height: 1rem;
      border: 2px solid white;
      position: absolute;
      bottom: 12px;
      right: 4px;
      box-shadow: 0px 0px 6px 0px black;
  `,
  'xxLarge': css`
      width:auto;
      min-width:1.2rem;
      height:1.2rem;
      border: 2px solid white;
      position: absolute;
      bottom: 14px;
      right: 7px;
      box-shadow: 0px 1px 6px 0px black;
  `,
};
  
const getAvatarStatusSize = (props)=> avatarStatusSize[props.size];
  
const avatarStatus = {
  'online': css`
    background: #54BBAB;
  `,
  'away': css`
    background: #DB4D59;
  `,
  'busy': css`
    background: #FFC627;
  `,
};
  
const getAvatarStatus = (props) => avatarStatus[props.status];
  
export const StyledAvatarStatus = styled(Badge)<AvatarStatusProps>`
  &.p-badge{
  display: inline-flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: auto;
  ${getAvatarStatusSize}
  ${getAvatarStatus}
  &.p-badge-lg{
    ${getAvatarStatusSize}
  }
  }
`;