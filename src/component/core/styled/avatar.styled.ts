import { Avatar } from 'primereact/avatar';
import styled, { css } from 'styled-components';
import { AvatarProps } from '../Avatar/Avatar'

const avatarSize = {
  'xSmall': css`
      width:1rem;
      height:1rem;
      font-size:0.375rem;
      line-height: 1rem;
      text-align: center;
      border-radius: 50%;
      font-weight: 700;
      position: relative;
    `,
  'small': css`
      width:1.5rem;
      height:1.5rem;
      font-size:0.68rem;
      line-height: 1.5rem;
      text-align: center;
      border-radius: 50%;
      font-weight: 700;
      position: relative;
    `,
  'medium': css`
      width:2rem;
      height:2rem;
      font-size:0.875rem;
      line-height: 2rem;
      text-align: center;
      font-weight: 700;
      border-radius: 50%;
    `,
  'large': css`
      width:2.5rem;
      height:2.5rem;
      font-size:1.125rem;
      line-height: 2.5rem;
      text-align: center;
      border-radius: 50%;
      font-weight: 700;
    `,
  'xLarge': css`
      width:6rem;
      height:6rem;
      font-size:3rem;
      line-height: 6rem;
      text-align: center;
      font-weight: 700;
      border-radius: 50%;
    `,
  'xxLarge': css`
      width:8rem;
      height:8rem;
      font-size:4rem;
      line-height: 8rem;
      text-align: center;
      border-radius: 50%;
      font-weight: 700;
    `,
};
  
const getAvatarSize = (props) => avatarSize[props.size];
  
const avatarImgSize = {
  'xSmall': css`
    width:1rem;
    height:1rem;
    border-radius:50%;
    position: relative;
  `,
  'small': css`
    width:1.5rem;
    height:1.5rem;
    border-radius:50%;
    position: relative;
  `,
  'medium': css`
    width:2rem;
    height:2rem;
    border-radius:50%;
  `,
  'large': css`
    width:2.5rem;
    height:2.5rem;
    border-radius:50%;
  `,
  'xLarge': css`
     width:6rem;
     height:6rem;
     border-radius:50%;
  `,
  'xxLarge': css`
    width:8rem;
    height:8rem;
    border-radius:50%;
  `,
};
const getAvatarImgSize = (props) => avatarImgSize[props.size];
  
export const StyledButton = styled.button`
    background:none;
    position:relative;
    margin: 0;
    padding: 0;
    border: 0;
`;
  
export const StyledDiv = styled.div`
  width:100%;
  position:relative;
  margin: 0;
  padding: 0;
  border: 0;
`;
  
export const StyledAvatar = styled(Avatar)<AvatarProps>`
  &.p-avatar{
  position: relative;
  margin: 0;
  padding: 0;
  border: 0;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  flex-wrap: nowrap;
  color: white;
  overflow: "hidden";
  letter-spacing: 0rem;
  min-width: initial;
  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  ${getAvatarSize}
  &.p-avatar-lg{
    ${getAvatarSize}
  }
  img{
    ${getAvatarImgSize}
  }
  }
`;