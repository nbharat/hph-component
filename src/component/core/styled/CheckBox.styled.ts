import { Checkbox } from 'primereact/checkbox';
import styled, { css } from 'styled-components';
import { HPHSkyBlue, HPHGreyDark, Background8, HPHGreyLight, HPHWhite, HPHRed, MontserratFont } from '../Colors';
import { Propscheck } from '../CheckBox/CheckBox';


const CheckBoxErrMsgStyle = (Props) => {
  if (Props.errorMessage) {
    return css`
            border: 1px solid ${HPHRed};
        `;
  }
};
  
const CheckBoxDisable = (Props) => {
  if (Props.disabled) {
    return css`
      color: ${HPHGreyLight};
      `;
  }
};
  
export  const StyledInputErrorMsg = styled.span<Propscheck>`
    color: ${HPHRed};
    display: block;
    font-family: ${MontserratFont};
    font-size: 0.875rem;
    font-weight: 500;
    line-height: 1.2;    
    margin-top: .25rem;    
    position: absolute;
    white-space: nowrap;
    top: 100%;
    margin-left: -4rem;
  `;
  
export const StyledDiv = styled.div<Propscheck>`
  display : flex;
  width: auto;
  height: auto;
  position: relative;
  margin-bottom: 1rem;
  margin-right: 0.5rem;
  padding: 0;
  border: 0;
  cursor: pointer;
  }
  `;
  
export const StyledCheckBox = styled(Checkbox)<Propscheck>`
  &.p-checkbox{
      width: 20px;
      height: 20px;
      .p-checkbox-box{
          border: 1px solid ${HPHGreyDark};
          background: ${HPHWhite};
          width: 20px;
          height: 20px;
          color: ${HPHWhite};
          border-radius: 4px;
          ${CheckBoxErrMsgStyle}
          &:not(.p-checkbox-disabled){
            &.p-focus{
              box-shadow: none;
              border: 1px solid ${HPHGreyDark};
              ${CheckBoxErrMsgStyle}
              }
              &:hover{
                      border-color: ${HPHSkyBlue};
                      ${CheckBoxErrMsgStyle}
              }
              &.p-disabled{
                border-color: ${HPHGreyLight};
                background: ${Background8};
              }
          &.p-highlight{
              border-color: ${HPHGreyDark};
              background: ${HPHSkyBlue};
              &:not(.p-checkbox-disabled):hover{
                border-color: ${HPHSkyBlue};
                background: ${HPHSkyBlue};
              }
                  &.p-disabled{
                      border-color: ${HPHGreyLight};
                      background: ${Background8};
                  .p-checkbox-icon{
                    font-size: 12px;
                    font-weight: 900;
                          color: ${HPHGreyLight}
                      }
                  }
              }
                  .p-checkbox-icon{
                    font-size: 12px;
                    font-weight: 900;
                  }
          }
      }
      }   
  `;
  
export const StyledLabel = styled.div<Propscheck>`
  font-weight: 500;
  font-size: 16px;
  letter-spacing: 0px;
  line-height: 1.2;
  text-align: left;
  font-family: ${MontserratFont};
  color: #222222;
  padding: 2px 5px;
  ${CheckBoxDisable}
  `;
  