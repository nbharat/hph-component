import styled, { css } from 'styled-components';
import { DragAndDropProps } from '../DragAndDrop/DragAndDrop';

const getWidth = (props) => props.width && css`width: ${props.width}`;
const getHeight = (props) => props.height && css`height: ${props.height}`;

const orientation = (Props) =>{
  if (Props.orientation === 'vertical'){
    return css`
    flex-direction: row;
    max-height: 100%;
    ${getHeight};
    `;
  } else {
    return css`
    flex-direction: column;
    max-width: 100%;
    ${getWidth};
    `;
  }
};
const itemOrientation = (Props) =>{
  if (Props.orientation === 'vertical'){
    return css`
    flex-direction: column;
    padding-bottom: 200px;
    overflow-x: hidden !important;
    `;
  } else {
    return css`
    flex-direction: row;
    padding-right: 200px;
    `;
  }
};
export const StyledDiv = styled.div`
  width: 100%;
  height: 100% ;
`;

export const StyledMain = styled.div<DragAndDropProps>`
  display: flex;
  ${orientation};
`;

export const StyledInnerDiv = styled.div<DragAndDropProps>`
  display: flex;
  ${itemOrientation};
  border: 2px solid gray;
  overflow: auto;
  white-space: nowrap;
  `;
  
export const StyledSecondInnerDiv = styled.div<DragAndDropProps>`
  display: flex;
  ${itemOrientation};
  border: 2px solid gray;
  overflow: auto;
  white-space: nowrap;
  > div {
    background: #fff;
    border: 1px solid gray;
    > div{
      color: #222;
    }
  }
`;
