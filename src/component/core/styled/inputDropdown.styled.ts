/* eslint-disable import/extensions */
import { AutoComplete } from 'primereact/autocomplete';
import styled, { createGlobalStyle, css } from 'styled-components';
import { InputDropdownProps } from '../InputDropdown/InputDropdown';
import { SVGIcon, Props } from '../SVGIcon/SVGIcon';
import { HPHRed, HPHSeaBlue, HPHSkyBlue, HPHGreyDark, HPHGreyLight, MontserratFont } from '../Colors';
import { getImgUrl } from '../../../common/getImgUrl';
import { scrollbarStyles } from './scrollbar.styled';


interface StyledAutoCompleteGlobal {
  panelWidth?: string;
  [x:string]: any
}
const getPanelLeft = (props) => props.panelLeft && css`left: ${props.panelLeft}px !important`;
export const AutoCompleteGlobalStyles = createGlobalStyle<StyledAutoCompleteGlobal>`
  ${scrollbarStyles}
  .p-autocomplete-panel {
    border-radius: 0px !important;
    box-shadow: none !important;
    overflow: hidden !important;
    border:1px solid ${HPHGreyDark} !important;
    ${getPanelLeft}
    .p-virtualscroller {
    }  
    .p-autocomplete-items {
      padding: 0 !important;
      .p-autocomplete-item {
        display: flex;
        padding: 0.35rem 0.55rem !important;
        min-height: 30px;
        white-space: break-spaces;
        &:hover {
          background: #C5E0F2 !important;
        }
      }
    }
  }
`;
  
export const StyledInputWrapper = styled.div`
    display:flex;
    flex-direction:column;
    position:relative;
    .p-inputtext:enabled:focus{
      outline:none;
      box-shadow:none;
      border-color:${HPHSkyBlue};
    }
    .p-inputtext{ 
      &:enabled:hover{
        border-color:${HPHSkyBlue};
      }
    }
    .p-inputtext:disabled{
      opacity:1;
    }
  `;

export const StyledMenuItemTemplate = styled.div`
  display: flex;
  align-items: center;
  #LozengesContainer {
    margin-bottom: 5px;
  }
`;
  
export const StyledMenuItemTemplateValue = styled.div`
  font-size: 16px;
  font-family: ${MontserratFont};
`;
  
export const StyledRequired = styled.em`
    color:${HPHRed};   
    font-style: initial;
    margin-left: 2px;
  `;

export const StyledLozengesContainer = styled.span`
    margin-left: 10px;
`;

export const StyledInputLabel = styled.label`
      color:${HPHSeaBlue};    
      display: block;
      font-family: ${MontserratFont}; 
      font-size:0.875rem;
      font-weight: 700;
      letter-spacing: 0px;
      line-height: 1.2;
      text-transform: capitalize;
      display:flex;
      align-items: center;
  `;
  
interface StyledSVGIcon extends Props {
  [x:string]: any
}
  
export const StyledMenuItemTemplateIcon = styled(SVGIcon)<StyledSVGIcon>`
  margin-right: 5px;
`;
  
const getAutoCompleteWidth = (props) => props.autoCompleteWidth && css`width: ${props.autoCompleteWidth}`;

const InputErrMsgStyle = (props) => {
  if (props.errorMessage) {
    return css`
      .p-component{
        border-color: ${HPHRed} !important;
        .p-inputtext,.p-button{
          border-color: ${HPHRed} !important;
        }
        &:hover{
          border-color: ${HPHRed} !important;
          .p-inputtext,.p-button{
            border-color: ${HPHRed} !important;
          }
        }
      }
    `;
  } else {
    return css`
      .p-component{
        border-color: ${HPHGreyDark};
        .p-inputtext,.p-button{
          border-color: ${HPHGreyDark} !important;
        }
        &:hover,&:focus{
          border-color: ${HPHSkyBlue} !important;
          ~ *{
            border-color: ${HPHSkyBlue} !important;
          }
        }
      }
    `;
  }
};

export const StyledAutoComplete = styled(AutoComplete)<InputDropdownProps>`   
  &.p-inputwrapper-focus{
    .p-component{
      border-color: ${HPHSkyBlue} !important;
    }
  }
  .p-component{    
    border:none;
    border-bottom: 1px solid ${HPHGreyDark};
    padding: 0.375rem 0rem !important;
  }
  .p-autocomplete-multiple-container {
    overflow: auto;
    flex-wrap: unset;
    ${getAutoCompleteWidth}
    padding: 0 !important;
    .p-autocomplete-input-token {
      input {
        font-family: ${MontserratFont} !important;
      }
    }
    &:not(.p-disabled).p-focus {
      box-shadow: none !important;
      border-color: ${HPHSkyBlue} !important;
    }
  }
  &:hover{
    .p-component{
      border-color: ${HPHSkyBlue}; 
    }
  }
  ${InputErrMsgStyle};
  .p-inputtext {
    font-family: ${MontserratFont};
    border-radius: 0px !important;
  }
  .p-autocomplete-dd-input {
    padding: 0;
  }
  .p-button {
      color: ${HPHGreyDark};
      background: transparent;
      border-radius: 0;
    &:focus{
      box-shadow: none !important;
    }
    &.p-button-icon-only {
      padding : 0;
    }
  }
  .p-button:enabled:hover {
    color: ${HPHGreyDark};
    background: transparent;
    border-radius: 0px;
    border-color: ${HPHSkyBlue};
  }
  .p-button-icon.Icon-auto-text {
    mask-image: url(${getImgUrl('Icon-auto-text')});
    width: 24px;
    height: 20px;
    background: ${HPHGreyDark};
  }
  .p-autocomplete-multiple-container .p-autocomplete-token {
    background: #C5E0F2 !important;
    color: ${HPHGreyDark} !important;
    padding: 0.15rem 0.25rem !important;
    font-family: ${MontserratFont};
    input::placeholder {
    font-family: ${MontserratFont};
    color: ${HPHGreyLight} !important;
    font-size: 16px;
  }
}
  .p-autocomplete-multiple-container .p-autocomplete-input-token {
    padding: 0 !important;
  }
  .p-disabled, .p-component:disabled {
    color: ${HPHGreyLight};
    border-color: ${HPHGreyLight} !important;
    opacity:1;   
    .Icon-auto-text {
      mask-image: url(${getImgUrl('Icon-auto-text')});
      width: 24px;
      height: 20px;
      background: ${HPHGreyLight};
  }
}

`;
  
export const StyledInputErrorMsg = styled.span`
  color: ${HPHRed};
  display: block;
  font-family: ${MontserratFont};
  font-size: 0.875rem;
  font-style: italic;
  font-weight: 500;
  line-height: 1.2;    
  margin-top: .25rem;    
  position: absolute;    
  top: 100%;
  white-space: nowrap;
`;