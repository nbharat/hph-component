import styled, { css } from 'styled-components';
import { HPHWhite, HPHSkyBlue, MontserratFont, HPHSeaBlue, Background1, Background2, HPHRed, HPHGreyLight, HPHWhiteSmoke } from '../Colors';
import { ButtonProps } from '../Button/Button';
import { Button } from 'primereact/button';
import { getImgUrl } from '../../../common/getImgUrl';

export const StyledDiv = styled.div`
    display : flex;
    width: auto;
    height: auto;
    position: relative;
    padding: 0;
    border: 0;
    justify-content: center;
    align-items: center;
    align-content: center;
    cursor: pointer;
`;

const buttonSize = {
  'Standard': css`
    height: 2.5rem;
    padding: 0rem 1.25rem;
    font-size: 1rem;
`,
  'Small': css`
    height: 1.875rem;
    padding: 0rem 0.75rem;
    font-size: 0.875rem;
`,
};

const getButtonSize = (props) => buttonSize[props.size];

const iconSize = {
  'Small': css`
    width: 12px;
    height: 12px;
    -webkit-mask-size: 12px;
  `,
  'Standard': css`
    width: 20px;
    height: 20px;
    -webkit-mask-size: 20px;
  `,
};

const getIconSize = (props) => iconSize[props.size];

const iconColor = {
  'Primary':css`
    background: ${Background2}; 
`,
  'Secondary':css`
    background: ${HPHSeaBlue}; 
`,
  'Alert':css`
  background: ${Background2};     
`,
};
const getIconColor = (props) => iconColor[props.theme];
const buttonTheme = {
  'Primary':css`
    color: ${HPHWhite};
    background: ${HPHSkyBlue};
    border: 1px solid ${HPHSkyBlue};
    font-weight: bold;
    border-radius: 4px;
    font-family: ${MontserratFont};
    &:enabled:hover{
        background: ${HPHSeaBlue};
        transform: scale(1.1);
    }
`,

  'Secondary':css`
    color: ${HPHSeaBlue};
    background: ${Background2};
    border: 1px solid #C2C2C2;
    font-family: ${MontserratFont};
    font-weight: bold;
    border-radius: 4px;
    &:enabled:hover{
        border: 1px solid ${HPHSkyBlue};
        color: ${HPHSkyBlue};
        background: ${Background2};
        transform: scale(1.1);
    .p-button-icon-right{
        background: ${HPHSkyBlue};
    }
} `,

  'Alert':css`
color: ${HPHWhite};
background: ${HPHRed};
border: 1px solid ${HPHRed};
font-family: ${MontserratFont};
font-weight: bold;
border-radius: 4px;
&:enabled:hover{
    border: 1px solid #AF212D;
    color: ${HPHWhite};
    background: #AF212D;
    transform: scale(1.1);
    }
`,
};

const getButtonTheme = (props) => buttonTheme[props.theme];
const getIcon = (props) => props.icon && css `mask-image: url(${getImgUrl(props.icon)})`;

export const StyledButton = styled(Button)<ButtonProps>`
&.p-button.p-component{    
    ${getButtonTheme}
    ${getButtonSize}
    &:focus{
        box-shadow:none;
    }
    &:disabled{
        background: ${HPHGreyLight};
        color: ${HPHWhiteSmoke};
        border: 1px solid ${HPHGreyLight};
        .p-button-icon-right{
            background:${Background1};
        }
    }
    .p-button-icon-right{
        ${getIconSize};
        margin: 0rem 0rem 0rem 0.625rem;
        order:2;
        ${getIconColor}
        ${getIcon}
    }
    .p-badge{
        order:2;
        min-width: 0.4375rem;
        height: 0.875rem;
        line-height: 0.875rem;
        background: #DB4D59;
        color:white;
    }
}
.p-button-label{
    white-space: nowrap;
    overflow: hidden;
    max-width: 25ch;
    order:0;
}
`;