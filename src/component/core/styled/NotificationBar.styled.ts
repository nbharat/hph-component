import styled, { css } from 'styled-components';
import { HPHRed, HPHOrange, HPHGreen, HPHWhite, MontserratFont, HPHSeaBlue } from '../Colors';
import { BarProps } from '../NotificationBar/NotificationBar';
import { getImgUrl } from '../../../common/getImgUrl';

const icon = {
  'Alert Red': css`
  mask-image: url(${getImgUrl('Icon-alert')});
`,
  'Warning Sunset Orange': css`
  mask-image: url(${getImgUrl('Icon-alert-triangle')});
`,
  'Success Aqua Green': css`
  mask-image: url(${getImgUrl('Icon-tick')});
`,
};

const getIcon = (props) =>icon[props.background];

export const StyledIcon = styled.div`
  -webkit-mask-size: 100%;
  background: ${HPHWhite};
  width: 1.5rem;
  min-width: 1.5rem;
  height: 1.5rem;
`;

const BarBackground = {
  'Alert Red': css`
    background: ${HPHRed}
  `,
  'Warning Sunset Orange': css`
    background: ${HPHOrange}
  `,
  'Success Aqua Green': css`
    background: ${HPHGreen}
  `,
};

const getBarBackground = (props) => BarBackground[props.background];
const getWidth = (Props) => Props.width && css`width: ${Props.width}`;
const getHeight = (Props) => Props.height && css`height: ${Props.height}`;
  
export const StyledDiv = styled.div<BarProps>` 
  position: relative;
  border: 1px solid black;
  display: flex;
  padding: 0.813rem 0.625rem;
  border-color: ${HPHWhite};
  background: ${HPHRed}
  ${getBarBackground}
  ${getWidth}
  ${getHeight}
  ${StyledIcon}{
   ${getIcon}
  }
`;
  
export const StyledChildren = styled.div`
  margin-left: 0.825rem;
  margin-right: 3.825rem;
  display: flex;
  letter-spacing: 0px;
  font-size: 1rem;
  font-family: ${MontserratFont};
  line-height: 1.4;
  word-wrap: break-word;
  word-break: break-word;
  overflow: hidden;
  font-weight: 500;
  color: ${HPHWhite};
  `;
  
export const StyledTime = styled.div<BarProps>`
  height: 1.25rem;
  font-size: 1rem;
  width: 1.25rem;
  letter-spacing: 0px;
  color: ${HPHWhite};
  overflow: visible;
  line-height: 1.2;
  text-align: center;
  font-family: ${MontserratFont};
  font-weight: 500;
  margin-right: 0.825rem;
  `;

export const StyledButton = styled.button<BarProps>`
  padding: 0;
  border: none;
  background: none;
  cursor: pointer;
  width: 1rem;
  height: 1rem;
  &:hover{
    background: ${HPHWhite};
    border-radius: 50%;
    width: 1rem;
    height: 1rem;
  }
  `;
export const StyledCross = styled.div`
  mask-image: url(${getImgUrl('Icon-cross')});
  -webkit-mask-size: 1rem;
  background: ${HPHWhite};
  width: 1rem;
  height: 1rem;
  &:hover{
    background: ${HPHSeaBlue};
  }
`; 
  
export const StyledBox = styled.div`
  display: flex;
  position: absolute;
  right: 10px;
`;