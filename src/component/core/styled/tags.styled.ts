/* eslint-disable import/extensions */
import styled, { css } from 'styled-components';
import { MontserratFont } from '../Colors';
import { TagsProps } from '../Tags/Tags';
import { SVGIcon, Props } from '../SVGIcon/SVGIcon';
import { getImgUrl } from '../../../common/getImgUrl';

const Theme = {
  'Ports Sea Blue': {
    bg: css`
     background: #C5E0F2;
     div {
      color: #002E6D;
     }
    `,
    icon: css`background: #002E6D;`,
  },
  'Ports Sky Blue': {
    bg: css`
     background: #C2EDFF;
     div {
      color: #0072A3;
     }
    `,
    icon: css`background: #0072A3;`,
  },
  'Ports Aqua Green': {
    bg: css`
     background: #D3EEEA;
     div {
      color: #348377;
     }
    `,
    icon: css`background: #348377;`,
  },
  'Ports Sunray Yellow': {
    bg: css`
     background: #FFEFC2;
     div {
      color: #A37800;
     }
    `,
    icon: css`background: #A37800;`,
  },
  'Ports Sunset Orange': {
    bg: css`
    background: #FBDBC6;
    div {
      color: #AB4C0D;
    }
    `,
    icon: css`background: #AB4C0D;`,
  },
  'Alert Red': {
    bg: css`
     background: #F5CCD0;
     div {
      color: #DB4D59;
     }
    `,
    icon: css`background: #DB4D59;`,
  },
};

const getTheme = (props) => Theme[props.theme].bg;

const getRounded = (props) => props.rounded ? css`border-radius: 100px;` : css`border-radius: 4px;`;

const display = (props) =>{
  if (props.width === 'auto'){
    return css`
      display: flex;
    `;
  } else {
    return css`
    display: inline-flex;
    `;
  }
};
const getWidth = (props) => props.width && css`width: ${props.width}`;

export const StyledMenuItemTemplate = styled.div<TagsProps>`
  align-items: center;
  ${display}
  ${getWidth}
  ${getTheme}
  ${getRounded}
  position:relative;
  padding: 5px 10px;
  `;
  
export const StyledMenuItemTemplateValue = styled.div`
  font-size: 14px;
  font-family: ${MontserratFont};
  font-weight: 500;
`;

interface StyledSVGIcon extends Props {
  [x:string]: any
}

const getIcon = (props) => props.fileName && css`mask-image: url(${getImgUrl(props.fileName)});`;
const getIconColor = (props) => Theme[props.theme].icon;

export const StyledMenuItemTemplateIcon = styled.div<StyledSVGIcon>`
  ${getIconColor}
  ${getIcon}
  margin-right: 5px;
  width: 22px;
  height: 22px;
`;

interface StyledIconButton extends Props {
  [x:string]: any
}

const getRemoveIcon = (props) => props.remove && css`mask-image: url(${getImgUrl('Icon-cross-small')});`;

export const StyledMenuItemTemplateIconButton = styled.div<StyledIconButton>`
  ${getIconColor}
  ${getRemoveIcon}
  width: 16px;
  height: 16px;
  margin-left: 10px;
  cursor: pointer;
`;
export const StyledWrapper = styled.div`
    display:flex;
    flex:1;
    align-items:center;
`;