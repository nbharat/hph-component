import styled, { css } from 'styled-components';
import { HPHRed, HPHOrange, HPHGreen, HPHWhite, MontserratFont, HPHHorizonBlue, HPHYellow, HPHSkyBlue, HPHSeaBlue } from '../Colors';
import { LozengesProps } from '../Lozenges/Lozenges';

const Variation = {
  'Ports Sea Blue': css`
     background: ${HPHSeaBlue}
     color: ${HPHWhite}
    `,
  'Ports Sky Blue': css`
     background: ${HPHSkyBlue}
     color: ${HPHWhite}
    `,
  'Ports Horizon Blue': css`
     background: ${HPHHorizonBlue}
     color: ${HPHSeaBlue}
    `,
  'Ports Aqua Green': css`
     background: ${HPHGreen}
     color: ${HPHWhite}
    `,
  'Ports Sunray Yellow': css`
     background: ${HPHYellow}
     color: ${HPHSeaBlue}
    `,
  'Ports Sunset Orange': css`
    background: ${HPHOrange}
    color: ${HPHWhite}
    `,
  'Alert Red': css`
     background: ${HPHRed}
     color: ${HPHWhite}
    `,
};

const getVariation = (props) => Variation[props.variation];

export const StyledSpan = styled.span<LozengesProps>` 
    padding: 2px 10px;
    font-size: 11px;
    font-weight: 700;
    border-radius: 4px;
    font-family: ${MontserratFont};
    ${getVariation}
    &:hover ~ div {
        span#lozengesTooltip {
            display: block;
        }
    }
`;

export const StyledWrapper = styled.div` 
    position:relative;
`;