import styled from 'styled-components';
import { MontserratFont } from '../Colors';

export const IconWrapper = styled.div`
   width:12.5%;
   padding:0.5rem;
   display: inline-flex;
   @media(max-width:1300px){
    width:14.28%;
   }
`;

export const IconInner = styled.div`
  display:flex;
  flex-direction:column;
  position:relative;
  width:100%;
  border: 1px solid rgba(180,180,180,1);
`;

export const IconBox = styled.div`
  display:flex;
  height:3.5rem;
  align-items:center;
  justify-content:center;
  padding-top: 0.5rem;
  img{
    width:35px
  }
`;
export const IconName = styled.span`
  padding:0 0.5rem 0.5rem;
  font-size:0.875rem;
  text-align:center;
  min-height:3.5rem;
  display:flex;
  align-items:center;
  justify-content:center;
  text-transform:capitalize;
  font-family: ${MontserratFont};
`;
