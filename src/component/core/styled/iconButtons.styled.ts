/* eslint-disable import/extensions */
import styled, { css } from 'styled-components';
import { MontserratFont } from '../Colors';
import { IconButtonsProps } from '../IconButtons/IconButtons';

export const sizeObj = {
  small: '16px',
  medium: '24px',
  large: '32px',
};
  
const getDisabledProp = (props) => {
  if (!props.disabledIcon) {
    return css`
          img:not(.disable){
              &:hover{
                  background: #C5E0F2;
                  border-radius: 25px;
                  cursor:pointer;
                  ~ div {
                      span {
                          display: block;
                          z-index: 1;
                      }
                  }
              }
          }
          `;    
  }
};
  
export const StyledButton = styled.button<IconButtonsProps>`
      position: relative;
      margin: 0;
      padding: 0;
      border: 0;
      background: none;
      cursor: pointer;
      font-family: ${MontserratFont}; 
      z-index:11;
      &:disabled{
        cursor: default;
      }
      img {
          padding: 6px;
          display: block;
          box-sizing: content-box;
      }
  
      ${getDisabledProp}
      
      .iconEnabledOnFocus{
          background: #C5E0F2;
          border-radius: 25px;
          padding: 6px;
      }
      .iconDisabled{
          opacity: 0.3;
          pointer-events: none;
      }
  `;
  