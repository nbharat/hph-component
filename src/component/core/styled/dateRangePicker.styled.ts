import styled, { createGlobalStyle } from 'styled-components';
import { Calendar } from 'primereact/calendar';
import {
  HPHSeaBlue,
  HPHRed,
  HPHWhite,
  HPHGreyDark,
  HPHSkyBlue,
  HPHGreyLight,
  MontserratFont,
} from '../Colors';
import { getImgUrl } from '../../../common/getImgUrl';

  
export const StyledInputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  .p-inputtext:enabled:focus {
    outline: none;
    box-shadow: none;
    border-color: ${HPHSkyBlue};
  }
  .p-inputtext {
    &:enabled:hover {
      border-color: ${HPHSkyBlue};
    }
  }
  .p-inputtext:disabled {
    opacity: 1;
  }
`;

export const StyledRequired = styled.em`
  color:${HPHRed};   
  font-style: initial;
  margin-left: 2px;
`;

export const StyledInputLabel = styled.label`
    color:${HPHSeaBlue};  
    font-family: ${MontserratFont}; 
    font-size:0.875rem;
    font-weight: 700;
    letter-spacing: 0px;
    line-height: 1.2;
    text-transform: capitalize;
    display:flex;
    align-items: center;
    button {
      margin-left:3px;
      img
      {
        padding:0;
        width:14px;
        height:14px;  
        opacity: 0.6;      
        &.iconEnabledOnHover:not(.disable):hover{
          border-radius:0;
          background:none;
        }
      }
    }
    .iconTooltip.right{
      top: 0;
      padding: 0.5rem;
      transform: translate(100%,0);
      z-index:1;
    }
`;

interface StyledCalendarGlobal {
  [x:string]: any
}

export const DateRangePickerGlobalStyles = createGlobalStyle<StyledCalendarGlobal>`
.p-datepicker.p-component {
  padding: 20px;

  thead tr th span {
    color: #777;
    font-size: 0.75rem;
    font-weight: 500;
    font-family: ${MontserratFont}; 
  }
  .p-datepicker-other-month {
    color: #ccc ;
  }

  .p-datepicker-header {
    font-size: 1rem;
    color: #009BDE;
    font-weight: bold;
    border-bottom: 0px;
    font-family: ${MontserratFont}; 
    padding:0;

    .p-datepicker-prev:hover,
    .p-datepicker-next:hover {
      background: rgb(197, 224, 242) !important;
    }

    .p-datepicker-prev:focus,
    .p-datepicker-next:focus {
      box-shadow: none !important;
    }

    .p-datepicker-prev {
      .p-datepicker-prev-icon {
        mask-image: url(${getImgUrl('Icon-arrow-left')});
        width: 24px;
        height: 24px;
        background: rgb(119,119,119);  
      }
    }

    .p-datepicker-next {
      .p-datepicker-next-icon {
        mask-image: url(${getImgUrl('Icon-arrow-right')});
        width: 24px;
        height: 24px;
        background: rgb(119,119,119);  
      }        
    }
  }

  table td {
    padding: 0px !important;
  }

  table td > span {
    border-radius: 0px !important;
  }

  table td > span:focus {
    box-shadow: none !important;
  }

  table td > span.p-highlight {
    background: #009BDE;
    color: #fff;   
    border-radius: 0px;   
    display: flex;
    width: 100%;      
  }

  table td > span {
    width: 2rem;
    height: 2rem;
    font-family: ${MontserratFont};
  }

  table td.p-datepicker-today > span {
    background: transparent;
    font-weight: bold;
  }

  table td.p-datepicker-today > span.p-highlight{
    background: #009BDE;
    color: #002E6D;
  }

  &:not(.p-disabled) table td span:not(.p-highlight):not(.p-disabled):hover {
    background: rgb(197, 224, 242) !important;
    color: #002E6D !important;
  }

  &.p-datepicker-multiple-month {
    .p-datepicker-group:first-child {
      padding-left: 0px;
      padding-right: 10px;
    }

    .p-datepicker-group:last-child {
      padding-left: 10px;
      padding-right: 0px;
    }
  }
}
`;

interface StyledCalendarProps {
  [x:string]: any
}

export const StyledCalendar =  styled(Calendar)<StyledCalendarProps>`  
    border: 0;
    border-bottom:1px solid ${HPHGreyDark};
    border-radius: 0;     
    background-color:${HPHWhite};
    color: ${HPHGreyDark};
    font-size: 1rem;
    font-weight: 500;
    font-family: ${MontserratFont};
    padding: 0.175rem 0rem;
    outline: none;
    letter-spacing: 2px;
    &::-webkit-input-placeholder {
      color: ${HPHGreyLight};
      font-size: 1rem;
      font-weight: 500;
    }
    &:-ms-input-placeholder {
      color: ${HPHGreyLight};
      font-size: 1rem;
      font-weight: 500;
    }
    &::placeholder {
      color: ${HPHGreyLight};
      font-size: 1rem;
      font-weight: 500;
    }
    &:focus,&::focus-visible{
      border:0;
      border-bottom:1px solid ${HPHSkyBlue};
      outline:none;
    }
    &:not(.p-calendar-disabled):hover{
      border-color: ${HPHSkyBlue};
    }
    &.has-error {
      border-color:${HPHRed};
    }
    &:disabled{
      border-color: ${HPHGreyLight};
      color:${HPHGreyLight};
    }  
    &::-webkit-inner-spin-button,
    &::-webkit-calendar-picker-indicator {
        display: none;
        -webkit-appearance: none;
    }

    &.p-calendar-disabled {
      border-bottom: 1px solid ${HPHGreyLight};
    }

    .p-inputtext {
      border: none;
      padding: 0.18rem 0rem;
      letter-spacing: 2px;
      font-weight: 500;
      font-family: ${MontserratFont};
      &.p-disabled {
        color:${HPHGreyLight};
      }
    }

    .p-button:disabled {
      color: #ccc !important;
    }

    .p-disabled {
      opacity: 1 !important;
    }

    .p-button {
      background: none;
      color: #002E6D;
      border: none;
      border-radius: 20px !important;
      &.p-button-icon-only {
        width: 2rem;
        padding: 0.18rem 0;        
      }
      &:focus {
        box-shadow: none !important;
      }
      &:not(.p-disabled):hover {
        color: #002E6D;
        background: rgb(197, 224, 242);
      }
      .pi-calendar {
        mask-image: url(${getImgUrl('Icon-calendar')});
        width: 24px;
        height: 24px;
        background: #002E6D;    
      }
      &.p-disabled {
        .pi-calendar {
          mask-image: url(${getImgUrl('Icon-calendar')});
          width: 24px;
          height: 24px;
          background: ${HPHGreyLight};    
        }          
      }
    }
`;

export const StyledInputErrorMsg = styled.span`
  color: ${HPHRed};
  display: block;
  font-family: ${MontserratFont};
  font-size: 0.875rem;
  font-style: italic;
  font-weight: 500;
  line-height: 1.2;    
  margin-top: .25rem;    
  position: absolute;    
  top: 100%;
  white-space: nowrap;
`;