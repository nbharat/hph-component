import { DataTable } from 'primereact/datatable';
import styled, { css, createGlobalStyle } from 'styled-components';
import { getImgUrl } from '../../../common/getImgUrl';
import { MultiSelect } from 'primereact/multiselect';
import { TableProps } from '../Table/Table';
import { HPHSkyBlue, HPHGreyDark, Background5, HPHWhiteSmoke, Background2, HPHGreyLight, MontserratFont, HPHSeaBlue, HPHSeaBlueDark, HPHWhite } from '../Colors';

const showPagination = (props) =>{
  if (props.paginator){
    return css`
      border: none;
    `;
  }
};

const getWidth = (Props) => Props.width && css`width: ${Props.width}`;

export const StyledDiv = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

export const StyledTag = styled.div`
display: flex;
padding-left: 5px;
`; 
export const Table = styled.div<TableProps>`
 position: relative;
 ${getWidth}
 height: 100%;
`;
export const StyledIcon = styled.div`
  display:flex;
  align-items:center;
  flex-direction:row; 
  position: absolute;
  right: 10px;
  `;
export const StyledTemplate = styled.div`
  display:flex;
  flex-direction:row; 
  `;
export const StyledDrag = styled.div`
    padding: 0px;
    left: 4px;
    top: 10px;
    position: absolute;
  `;
export const StyledItem = styled.div`
  position: absolute;
  top: 14px;
  left: 57px;
  `;
export const StyledWrapper = styled.div`
  display:flex;
  flex-direction:row; 
  `;
const paginatorHeight = (props) =>{
  if (props.paginator){
    return css`
     height: calc(100% - 55px);
    `;
  } else {
    return css`
     height: 100%;
    `;
  }
};

export const StyledTable = styled(DataTable)<TableProps>`
  &.p-datatable {
    position: relative;
    table-layout: fixed;
    width: 100%;
    height: 100%;
    font-family: ${MontserratFont};
    .pi-sort-alt:before {
      display: none;
    }
    .pi-sort-amount-up-alt:before {
      display: none;
    }
    .pi-sort-amount-down:before {
      display: none;
    }
    &.p-datatable-selectable .p-datatable-tbody > tr.p-selectable-row:focus{
      outline: none !important;
      outline-offset: 0 !important;
    }
    .p-datatable-wrapper{
      max-height: initial !important;
      ${paginatorHeight}
    }
    .p-datatable-thead > tr > th:last-child{
      max-width: 60px;
      min-width: 60px !important;
    }
    .p-datatable-tbody > tr:last-child > td{
      ${showPagination}
    }
    .p-datatable-tbody > tr > td:last-child{
      max-width: 80px;
    min-width: 80px !important;
    }
    .p-datatable-thead > tr > th:first-child{
      max-width: 70px;
      min-width: 70px !important;
    }
    .p-datatable-tbody > tr > td:first-child{
      max-width: 70px;
      min-width: 70px !important;
    }
    .p-datatable-thead > tr > th:nth-last-child(2){
     
      #iconTooltipID{
        display: none;
      }
    }
    .p-datatable-tbody > tr > td:nth-last-child(2){

      #iconTooltipID{
        display: none;
      }
    }
    .p-column-header-content{
      .p-column-filter-menu-button{
      mask-image: url(${getImgUrl('Icon-angle-down')});
      -webkit-mask-size: 100%;
      -webkit-mask-repeat: no-repeat;
      background:  ${HPHSeaBlue};
      width: 24px;
      height: 24px;
      &:hover{
        background: ${HPHSkyBlue};
      }
      &:focus{
        background: ${HPHSkyBlue};
      }
      }
    }
    th.p-sortable-column:last-child .p-column-filter-menu{
      right:30px;
    }
    .p-sortable-column{
     &:focus {
      box-shadow: none;
      outline: none;
     }
     .p-column-filter-menu {
      margin-left: 8px;
     }
    .p-sortable-column-icon {
      color: ${HPHSeaBlue};
      padding: 2px;
      width: 24px;
      height: 24px;
      &.pi-sort-amount-up-alt{
        background:  ${HPHSeaBlueDark} url(${getImgUrl('Icon-sort-alpha')});
        -webkit-mask-size: 100%;
        -webkit-mask-repeat: no-repeat;
        border-radius: 50%;
        background-size: cover;
        background-size: 20px;
        background-position: center;
        background-repeat: no-repeat;
      }
      &.pi-sort-amount-down{
        background:  ${HPHSeaBlueDark} url(${getImgUrl('Icon-sort-alpha-alt')});
        -webkit-mask-size: 100%;
        -webkit-mask-repeat: no-repeat;
        border-radius: 50%;
        background-size: cover;
        background-size: 20px;
        background-position: center;
        background-repeat: no-repeat;
      }
      &.pi-sort-alt{
        background: url(${getImgUrl('Icon-connect')});
        -webkit-mask-size: 100%;
        -webkit-mask-repeat: no-repeat;
        border-radius: 50%;
        background-size: cover;
        background-size: 24px;
        background-position: center;
        background-repeat: no-repeat;
        &:hover{
          background:  ${HPHSeaBlueDark} url(${getImgUrl('Icon-connect')});
          border-radius: 50%;
        }
      }
    }
      &.p-highlight:not(.p-sortable-disabled):hover {
        color: ${HPHSeaBlue};
        background:  ${HPHWhiteSmoke};
      }
      :not(.p-highlight):not(.p-sortable-disabled):hover {
        background:  ${HPHWhiteSmoke};
        color: ${HPHSeaBlue};
      }
    }
    .p-datatable-tbody > tr:hover {
      color:  ${HPHSeaBlue} !important;
      background:  ${Background5} !important;
      outline:none !important;
    }
    .p-datatable-tbody > tr:hover > td{
      color:  ${HPHSeaBlue} !important;
      background:  ${Background5} !important;
      outline:none !important;
    }
    .p-datatable-tbody > tr.p-highlight {
      color: ${HPHSkyBlue}  !important;
      background: #F1F2F3  !important;
      outline:none !important;
    }
    .p-datatable-tbody > tr.p-highlight > td {
      color: ${HPHSkyBlue}  !important;
      background: #F1F2F3  !important;
    }
    .p-datatable-thead > tr{
      position:relative;
      &:after{
        content:'';
        left:0;
        right:0;
        bottom:0;
        border-bottom: 2px solid  ${HPHSkyBlue} !important;
        position:absolute;
        z-index:1;
      }
    }
    .p-datatable-thead > tr > th{
      position: relative;
      text-align: left;
      padding: 0.3rem 1rem;
      border: none;
      min-height: 36px;
      font-size: 16px;
      min-width: 12rem;
      row-height: 70px;
      font-weight: 700;
      color:  ${HPHSeaBlue};
      background:  ${HPHWhiteSmoke};
      border-bottom: 2px solid  ${HPHSkyBlue} !important;
      position:relative;
      z-index:2;
      .p-checkbox .p-checkbox-box{
        width:20px;
        height:20px;
        border: 1px solid  ${HPHGreyDark};
        .p-checkbox-icon{
          font-size: 12px;
        }
        &.p-highlight {
          border-color:  ${HPHGreyDark};
          background:  ${HPHSkyBlue};
        }
        &:hover{
          border-color:  ${HPHSkyBlue};
        }
        &.p-focus{
          box-shadow: none;
        }
      }
    }
    
    .p-datatable-tbody .p-datatable-emptymessage > td:first-child {
      min-width: 100% !important;
    }
    
    .p-datatable-tbody > tr{
      background: transparent;
      color:  ${HPHSeaBlue};
      height: 36px;
      min-width: 12rem;
      outline-color: transparent !important;
      position:relative;
      &:after{
        content:'';
        border-bottom: 1px solid  ${HPHGreyLight};
        left:0;
        right:0;
        bottom: 0;
        position:absolute;
      }
      &:last-child:after{
        display:none
      }
    }
    .p-datatable-tbody > tr > td{
      min-width: 11rem !important;
      width: 11rem;
      text-align: left;
      font-weight: 500;
      padding: 13px 15px;
      outline-color: #C7D2FE;
      border: none;
      border-bottom: 1px solid  ${HPHGreyLight};
      position:relative;
      z-index:2;
      .p-checkbox .p-checkbox-box{
        width:20px;
        height:20px;
        border: 1px solid  ${HPHGreyDark};
        .p-checkbox-icon{
          font-size: 12px;
        }
        &.p-highlight {
          border-color:  ${HPHGreyDark};
          background:  ${HPHSkyBlue};
        }
        &:hover{
          border-color:  ${HPHSkyBlue};
        }
        &.p-focus{
          box-shadow: none;
        }
      }
      .p-radiobutton .p-radiobutton-box{
        width:16px;
        height:16px;
        border: 1px solid ${HPHGreyDark}
        .p-radiobutton-icon{
          width: 10px;
          height: 10px;
          background-color:  ${HPHSkyBlue};
        }
        &:not(.p-disabled){
          &:hover{
            border-color: ${HPHSkyBlue};
            background: ${Background2};
            color: ${Background2};
          }
          &.p-focus{
            box-shadow: none;
          }
          &:not(.p-highlight):hover{
            border-color: ${HPHSkyBlue};
          }
        }
        &.p-highlight {
          border-color: ${HPHGreyDark};
          background: ${Background2};
          &:not(.p-disabled){
            &:hover{
              border-color: ${HPHSkyBlue};
              background: ${Background2};
            }
          }
        }
      }
      .p-row-editor-init{
        position: relative;
        right: 0.2rem;
        color:  ${HPHSeaBlue};
        &:enabled:hover {
          color:  ${HPHSeaBlue};
          border-color: transparent;
          background:  ${HPHSeaBlueDark};
        }
        &:focus{
          box-shadow: none;
        }
      }
      .p-row-editor-save{
        position: absolute;
        right: 2.5rem;
        color:  ${HPHSeaBlue};
        &:enabled:hover {
          color:  ${HPHSeaBlue};
          border-color: transparent;
          background:  ${HPHSeaBlueDark};
        }
        &:focus{
          box-shadow: none;
        }
      }
      .p-row-editor-cancel{
        position: absolute;
        right: 0.2rem;
        color:  ${HPHSeaBlue};
        &:enabled:hover {
          color:  ${HPHSeaBlue};
          border-color: transparent;
          background:  ${HPHSeaBlueDark};
        }
        &:focus{
          box-shadow: none;
        }
      }
        .p-inputtext {
          height: 2rem;
          font-family: ${MontserratFont};
          width: 11rem;
          border: 1px solid  ${HPHSkyBlue};
          &:enabled:hover{
            border-color:  ${HPHSkyBlue};
          }
        }
        .p-dropdown{
          width: 11rem !important;
          .p-dropdown-label {
           border: 0 none !important;
           padding: 0.4rem 0.75rem !important;
         }
         &:not(.p-disabled).p-focus{
           box-shadow: none;
           border-color: #009BDE !important;
         }
         &:not(.p-disabled):hover{
          border-color: #009BDE !important;
         }
        }
    }
    .p-paginator{
      border-top: 2px solid  ${HPHSkyBlue};
      background:  ${HPHWhiteSmoke};
      padding: 10px 20px;
      border-radius: 0px;
      .p-paginator-pages{
          .p-paginator-page {
              border-radius: 50%;
              border: none;
              background: none;
              font-size: 1rem;
              font-family: ${MontserratFont};
              color: ${HPHSeaBlue};
              font-weight: 500;
              margin: 0px 5px;
              height: 1.5rem;
              min-width: 1.5rem;
              cursor: pointer;
              &:not(.p-highlight):hover{
                  background: ${HPHSeaBlueDark};
                  height: 1.5rem;
                  width: 1.5rem;
                  color: ${HPHSeaBlue};
              }
              &.p-highlight{
                  background: ${HPHSkyBlue};
                  height: 1.5rem;
                  width: 1.5rem;
                  color: ${HPHWhite};
                  padding: 3px 5px;
              }
          }
      }
      .p-paginator-prev{
         min-width: 2rem;
         height:2rem; 
         color: ${HPHSeaBlue};
         margin: 0 5px;
         &:not(.p-disabled):not(.p-highlight){
              &:hover{
                background: ${HPHSeaBlueDark};
                border-color: transparent;
                color: ${HPHSeaBlue};
              }
          }
      }
      .p-paginator-next{
          min-width: 2rem;
          height: 2rem; 
          color: ${HPHSeaBlue};
          margin: 0 5px;
          &:not(.p-disabled):not(.p-highlight){
              &:hover{
                background: ${HPHSeaBlueDark};
                border-color: transparent;
                color: ${HPHSeaBlue};
              }
          }
      }
  }
  .p-link{
      &:focus{
          box-shadow: none;
      }
  }
  .p-datatable-thead{
    background:  ${HPHWhiteSmoke};  
    z-index: 3;  
  }

  }
`;

export const StyledSpan = styled.span`
    display: flex;
    align-items:center;
    font-family: ${MontserratFont};
`;

export const StyledText = styled.div`
  width: auto;
  margin: 0 5px;
  font-size: 1rem;
  letter-spacing: 0px;
  line-height: 1.2;
  color: ${HPHGreyDark};
  font-weight: 500;
`;

export const StyledPaginationButton = styled.button`
  border: none;
  width: auto;
  height: 2rem;
  background: ${HPHSkyBlue};
  color: ${HPHWhite};
  border-radius: 5px;
  margin: 0 0px 0 5px;
  cursor: pointer;
  font-size: 1rem;
  font-weight: 500;
  text-transform: capitalize;
`;

export const StyledInput = styled.input`
    outline:none;
    background: ${HPHWhite};
    border: none;
    font-family: ${MontserratFont};
    font-weight: 500;
    text-align: center;
    color: ${HPHGreyDark};
    line-height: 1.4;
    font-size: 1rem;
    width: 2rem;
    height: 2rem;
    margin: 0 5px;
    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin: 0; 
    }
`;
interface StyledTableGlobal {
  [x:string]: any
}
export const TableGlobalStyles = createGlobalStyle<StyledTableGlobal>`
  .p-dropdown-panel{
    .p-dropdown-items{
      .p-dropdown-item{
        color: #002E6D !important;
      }
      .p-dropdown-item.p-highlight{
        color: #009BDE !important;
        background: #F1F2F3 !important;
      }
      .p-dropdown-item:not(.p-highlight):not(.p-disabled):hover {
        color: #002E6D !important;
        background: #F5FBFF !important;
      }
    }
  }
  .p-column-filter-constraints{
    border-radius: 4px;
    background:linear-gradient(180deg,  ${HPHWhiteSmoke} 0%,  ${HPHWhite} 100%);
    border: 1px solid  ${HPHWhiteSmoke};
    box-shadow: 0px 0px 3px -1px rgb(109 140 171);
    position: absolute;
    top: 12px;
    left: -14px;
    &::after{
      content: " ";
      width: 0; 
      height: 0; 
      border-left: 1rem solid transparent;
      border-right: 1rem solid transparent;
      border-bottom: 1rem solid  ${HPHWhiteSmoke};
      -webkit-filter: drop-shadow(0px 0px 0px #6D8CAB);
      filter: drop-shadow(0px 0px 0px #6D8CAB);
      position: absolute;
      bottom: 102%;
      Left: 0.5rem;
    }
  }
  .p-inputtext:enabled:focus {
    box-shadow: none !important;
    border-color:  ${HPHSkyBlue} !important;
  }
  .p-column-filter-overlay-menu .p-column-filter-constraint .p-inputtext{
    width: auto;
    border: 0;
    border-bottom: 1px solid  ${HPHGreyDark};
    border-radius: 0;
    font-family: ${MontserratFont};
    color:  ${HPHGreyDark};
    font-size: 1rem;
    font-weight: 500;
    padding: 0.375rem 0rem;
    &:hover{
      border-color:  ${HPHSkyBlue};
    }
  }
  p-connected-overlay-enter-done{
    left:21.1rem;
  }
  .p-column-filter-overlay-menu .p-column-filter-buttonbar {
    position: absolute;
    top: 2.3rem;
    padding: 0 !important;
    right: 0rem;
  }
  .p-button:enabled:hover, .p-button:not(button):not(a):not(.p-disabled):hover {
    background:  ${HPHSkyBlue} !important;
    color:  ${HPHWhite} !important;
    border-color:  ${HPHSkyBlue} !important;
  }
  .p-column-filter-overlay-menu .p-column-filter-buttonbar .p-button{
    background: ${HPHWhite};
    mask-image: url(${getImgUrl('Icon-search')});
    -webkit-mask-size: 100%;
    -webkit-mask-repeat: no-repeat;
      background:  ${HPHSeaBlue};
      width: 24px !important;
      height: 24px;
      padding:0;
      .p-button-label{
        display:none;
      }
  }
  &.p-multiselect-panel{
    .p-multiselect-header {
      display:none;
    }
    .p-multiselect-items-wrapper > ul{
      font-family: ${MontserratFont} !important;
      font-weight: 500;
    }
    .p-multiselect-items {
    padding: 0 !important;
    background:  ${HPHWhiteSmoke};
    .p-disabled.p-highlight{
      opacity: 10;
      span{
      color:  ${HPHGreyLight} !important;
      }
      .p-checkbox .p-checkbox-box.p-highlight {
        border-color:  ${HPHGreyLight} !important;
        background: #ececec !important;
      }
    }
    .p-multiselect-item{
      &:focus{
        box-shadow:none !important;
      }
      .p-checkbox{
        right: -9px;
      }

      .p-checkbox .p-checkbox-box{
        border: 1px solid  ${HPHGreyDark};  
        background:  ${HPHWhite};  
        &:hover{
          border-color:  ${HPHSkyBlue};
        }    
      }
    }
    .p-multiselect-item.p-highlight {
      background: none !important;
      color: black !important;
      .p-checkbox .p-checkbox-box{
        border: 1px solid  ${HPHGreyDark};
      &.p-highlight {
        border-color:  ${HPHGreyDark};
        background:  ${HPHSkyBlue};
      }
      &:hover{
        border-color:  ${HPHSkyBlue};
      }
    }
    }
  }
}
  `;

export const StyledMultiselectBox = styled.div`
  width: 100%;
`;

export const StyledMultiselect = styled(MultiSelect)<TableProps>`
  &.p-multiselect{
    top: 0.2rem;
    width: 27px;
    height: 27px;
    background:none;
    border:none;
    position: absolute;
    right: 1.2rem;
    z-index: 10;
    border-radius : 50%;
    background:  ${HPHWhiteSmoke};
    &:not(.p-disabled).p-focus{
      box-shadow: none;
      background:  ${HPHSeaBlueDark};
    }
    &:not(.p-disabled):hover {
      background:  ${HPHSeaBlueDark};
    }
    .p-multiselect-trigger {
      width: 1.5rem;
      height: 1.5rem;
    }
    .p-multiselect-label-container{
      display:none;
    }
    .pi{
      mask-image: url(${getImgUrl('Icon-column-setting')});
      -webkit-mask-size: 100%;
      -webkit-mask-repeat: no-repeat;
      background:  ${HPHSeaBlue};
      width: 22px;
      height: 22px;
      margin-top: 5px;
      margin-left: 3px;
    }
  }
  `;
export const StyledFooter = styled.div`
  display:flex;
  flex-direction:row; 
  align-items: center;
  padding: 15px 20px;
  min-width: 300px;
  justify-content: space-between;
`;
export const StyledRefresh = styled.div`
  padding:0;
  display: inline-flex;
  img{
    padding: 2px !important;
  }
  span{
    padding: 0.225rem !important;
  }
`;
export const StyledButton = styled.div`
  padding:0;
`;
interface StyledCalendarGlobal {
  [x:string]: any
}
export const DateTimePickerGlobalStyles = createGlobalStyle<StyledCalendarGlobal>`
.p-datepicker.p-component {
  box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.25) !important;
  border-radius: 8px;
  padding: 20px;
  color: #222;
  thead tr th span {
    color: #777;
    font-size: 12px;
    font-weight: 500;
    font-family: ${MontserratFont}; 
  }
  .p-datepicker-other-month {
    color: #ccc;
  }
  .p-datepicker-header {
    font-size: 16px;
    color: #009BDE;
    font-weight: bold;
    padding: 0;
    border-bottom: 0px ;
    font-family: ${MontserratFont}; 

    .p-datepicker-prev:hover,
    .p-datepicker-next:hover {
      background: rgb(197, 224, 242) !important;
    }
    .p-datepicker-prev:focus,
    .p-datepicker-next:focus {
      box-shadow: none !important;
    }
    .p-datepicker-prev {
      .p-datepicker-prev-icon {
        mask-image: url(${getImgUrl('Icon-arrow-left')});
        width: 24px;
        height: 24px;
        background: rgb(119,119,119);  
      }
    }
    .p-datepicker-next {
      .p-datepicker-next-icon {
        mask-image: url(${getImgUrl('Icon-arrow-right')});
        width: 24px;
        height: 24px;
        background: rgb(119,119,119);  
      }        
    }
  }
  table td {
    padding: 0px;
  }

  table td > span {
    border-radius: 0px !important;
  }

  table td > span:focus {
    box-shadow: none !important;
  }
  table td > span.p-highlight {
    background: #009BDE;
    color: #fff;            
  }
  table td > span {
    width: 2rem;
    height: 2rem;
    font-family: ${MontserratFont}; 
  }
  table td.p-datepicker-today > span {
    background: transparent;
    font-weight: bold;
  }
  table td.p-datepicker-today > span.p-highlight{
    background: #009BDE;
    color: #002E6D;
  }
  &:not(.p-disabled) table td span:not(.p-highlight):not(.p-disabled):hover {
    background: rgb(197, 224, 242) !important;
    color: #002E6D !important;
  }
  &.p-datepicker-multiple-month {
    .p-datepicker-group {
      padding-left: 10px;
      padding-right: 10px;
      &:first-child {
        padding-left: 0;
      }
      &:last-child {
        padding-right: 0;
      }
    }
  }
  .p-datepicker-group-container {
    border-right: 1px solid #ccc;
    padding-right: 20px;
  }
  &:not(.p-datepicker-inline) {
    display: flex;
  }
  .p-timepicker {
    border-top: none;
    padding-left: 20px;
    padding-right: 0;

    .p-separator {
      padding: 0 0.25rem !important;
    }
    button {
      width: 1rem;
      height: 1rem;
      .pi-chevron-down, .pi-chevron-up {
        font-size: .938rem;
        color: rgb(119, 119, 119);
      }
    }
    button:enabled:hover {
      background: transparent !important;
    }
    button ~ span {
      font-size: 1rem;
      border: 1px solid #222;
      padding: .15rem .4rem;
      font-family: ${MontserratFont};
    }
  }
}
`;
