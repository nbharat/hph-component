import { css } from 'styled-components';

export const scrollbarStyles = css`
::-webkit-scrollbar { height: 6px; width: 6px; }
::-webkit-scrollbar:hover { height: 8px; width: 8px; }
::-webkit-scrollbar-thumb { background-color: rgba(34, 34, 34, .5); border-radius: 100px; }
`;