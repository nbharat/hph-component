import styled from 'styled-components';
import { HPHGrey, HPHSeaBlue, HPHSkyBlue, MontserratFont } from '../Colors';
import { getImgUrl } from '../../../common/getImgUrl';

export const StyledMainWrapper = styled.div`
    width:100%;
    display:flex;
    min-height: 100vh;
`;
export const StyledMainContent = styled.div`
    flex:1;
    display:flex;
    flex-wrap:wrap;
    padding:1rem;
    position:relative;
`;

export const StyledColoum = styled.div`
    min-width: 33.33%;
    flex:1;
    padding-right: 50px
    &:last-child{
        padding-right: 0px
    }
    @media(max-width:1280px){
        min-width: 50%; 
        padding-right: 30px
        &:last-child{
            padding-right: 0px
        }
    }
    @media(max-width:980px){
        min-width: 100%; 
        padding-right:0px
    }
    .p-datatable .p-datatable-thead > tr > th{
        min-width:initial;
    }
    .p-datatable .p-datatable-tbody > tr > td{
        min-width: 6rem !important;
        width: auto;
    }
    .p-datatable .p-datatable-thead > tr > th:nth-last-child(2),
    .p-datatable .p-datatable-tbody > tr > td:nth-last-child(2){
        display:none
    }
    .p-selectbutton.p-buttonset.p-component {
        display: block;
        white-space: nowrap;
        overflow: scroll;
        .p-button{
            padding:10px;
            width:auto;
            &.p-highlight{
                position:relative;
                padding:10px 10px 10px 25px;
                &:before{
                    content:'';
                    width:20px;
                    height:20px;
                    background:${HPHSeaBlue};
                    border-radius:50%;
                    position:absolute;
                    left:5px;
                }
                &:after{
                    content:'';
                    width:16px;
                    height:16px;
                    position:absolute;
                    left:7px;
                    top:12px;
                    mask-image: url(${getImgUrl('Icon-tick')});
                    -webkit-mask-size: 100%;
                    -webkit-mask-repeat: no-repeat;
                    background:  ${HPHSkyBlue};
                }
            }
        }
    }

`;
export const StyledSidebar = styled.div`
    width:350px;
    min-height: 100%;
    border-left:5px solid ${HPHSkyBlue};
    transition: width 0.3s;
    -webkit-transition: width 0.3s;
    padding:20px;
`;
export const SidebarActionBar = styled.div`{
    display:flex;
    justify-content:flex-end;
`;

export const StyledElement = styled.div`
    margin-bottom:55px;
    padding-left:10px;
    position:relative;
`;

export const StyledTableActionButton = styled.div`
    position:absolute;
    top: 45px;
    right: -30px;
`;

export const Sidebarheader = styled.div`
    display:flex;
    justify-content:space-between;
    align-items:flex-start;
`;
export const SidebarTitle = styled.div`
    font-size: 1.25rem;
    color:${HPHSeaBlue};
    font-weight: 700;
    font-family: ${MontserratFont};
`;
export const SidebarCaption = styled.span`
   display:block;
   font-size:0.75rem;
   color:${HPHGrey};
   font-style:italic;
   font-family: ${MontserratFont};
   margin-top:5px;
   font-weight: 500;
`;


export const StyledInnerElement = styled.div`
    margin-top: 30px;
`;
export const StyledTitle = styled.div`
    margin-bottom:20px;
    font-size: 16px;
    text-transform: uppercase;
    font-weight: 700;
    color:#222;
    font-family: ${MontserratFont};
    margin-left:-10px;
    display: flex;
    align-items: center;
    justify-content:space-between;
`;
export const StyledParentLabel = styled.span`
    color: #002E6D;
    display: block;
    font-family: ${MontserratFont};
    font-size: 0.875rem;
    font-weight: 700;
    line-height: 1.2;
    text-transform:capitalize;
    margin-bottom: 8px;
    width: 100%;
`;
export const StyledHorizontal = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
    margin-top:30px;
    flex-wrap:wrap;
    max-width:420px;
`;
export const StyledVertical = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
`;


