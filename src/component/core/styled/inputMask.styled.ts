import styled from 'styled-components';
import { CommonProps } from '../InputMask/InputMask';
import { HPHSkyBlue, HPHGreyDark, HPHRed, HPHGreyLight, MontserratFont, HPHSeaBlue, HPHWhite } from '../Colors';
export const OuterDiv = styled.div`
    display: flex;
    flex-direction: column;
    color: ${HPHGreyDark};
    position:relative;
`;
export const InputDiv = styled.div<CommonProps>`
    display: flex;
    align-items: center;
    min-height: 34px;
    background: ${HPHWhite};
    ${props => {
    if (props.backgroundMode) {
      if (!props.isValid) {
        return `
                    border: 1px solid ${props.disabled ? HPHGreyDark  : props.isValid ? 'none' : HPHRed + '!important'};
                `;
      }
    } else {
      return `
                border-bottom: 1px solid ${props.disabled ? HPHGreyLight + '!important' : props.isValid ? HPHGreyDark : HPHRed + '!important'};
            `;
    }
  }}
    &:focus {
        color: ${HPHSkyBlue};
    }    
    & > input {  
        outline: none;
        border: none;
        font-size: 1rem;
        padding: 0.375rem 0rem;
        color: ${HPHGreyDark};
        text-transform: uppercase;
        box-sizing: border-box;
        font-family: ${MontserratFont}; 
        &:disabled {
            background: none;
            pointer-events:none;
            color: ${HPHGreyLight};
        }
    }    
    & > span {
        padding:0 5px;
        font-weight: 600;
        font-family: ${MontserratFont}; 
        color: ${props => props.disabled ? HPHGreyLight : props.isValid ?  HPHGreyDark : HPHRed};
    }
`;

export const ComInput = styled.input`
  min-width: 20%;
  width:20%;
`;
export const VslInput = styled.input`
  min-width: 25%;
  width:25%;
`;
export const VoyInput = styled.input`
  min-width: calc(55% - 40px);
  width: calc(55% - 40px);
`;
export const StyledRequired = styled.em`
  color:${HPHRed};   
  font-style: initial;
  margin-left: 2px;
`;

export const StyledInputLabel = styled.label`
    color:${HPHSeaBlue};    
    display: block;
    font-family: ${MontserratFont}; 
    font-size:0.875rem;
    font-weight: 700;
    letter-spacing: 0px;
    line-height: 1.2;
    text-transform: capitalize;
    display:flex;
    align-items: center;
    margin-bottom:0.375rem;
    button {
      margin-left:3px;
      img
      {
        padding:0;
        width:14px;
        height:14px;   
        opacity: 0.6;     
        &.iconEnabledOnHover:not(.disable):hover{
          border-radius:0;
          background:none;
        }
      }
    }
`;
export const StyledInputErrorMsg = styled.span`
  color: ${HPHRed};
  display: block;
  font-family: ${MontserratFont};
  font-size: 0.875rem;
  font-style: italic;
  font-weight: 500;
  line-height: 1.2;    
  margin-top: .25rem;    
  position: absolute;    
  top: 100%;
  white-space: nowrap;
`;