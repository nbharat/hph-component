import styled from 'styled-components';

import {
    HPHSeaBlue,
    HPHRed,
    HPHWhite,
    HPHGreyDark,
    HPHSkyBlue,
    HPHGreyLight,
    MontserratFont,
  } from '../Colors';

  export const StyledInputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  .p-inputtext:enabled:focus {
    outline: none;
    box-shadow: none;
    border-color: ${HPHSkyBlue};
  }
  .p-inputtext {
    &:enabled:hover {
      border-color: ${HPHSkyBlue};
    }
  }
  .p-inputtext:disabled {
    opacity: 1;
  }
`;

export const StyledRequired = styled.em`
  color:${HPHRed};   
  font-style: initial;
  margin-left: 2px;
`;

export const StyledInputLabel = styled.label`
    color:${HPHSeaBlue};  
    font-family: ${MontserratFont}; 
    font-size:0.875rem;
    font-weight: 700;
    letter-spacing: 0px;
    line-height: 1.2;
    text-transform: capitalize;
    display:flex;
    align-items: center;
    button {
      margin-left:3px;
      img
      {
        padding:0;
        width:14px;
        height:14px;  
        opacity: 0.6;      
        &.iconEnabledOnHover:not(.disable):hover{
          border-radius:0;
          background:none;
        }
      }
    }
    .iconTooltip.right{
      top: 0;
      padding: 0.5rem;
      transform: translate(100%,0);
      z-index:1;
    }
`;

export const StyledCalendar =  styled.input`  
    border: 0;
    border-bottom:1px solid ${HPHGreyDark};
    border-radius: 0;     
    background-color:${HPHWhite};
    color: ${HPHGreyDark};
    font-size: 1rem;
    font-weight: 500;
    font-family: ${MontserratFont};
    padding: 0.375rem 0rem;
    outline: none;
    letter-spacing: 2px;
    &::-webkit-input-placeholder {
      color: ${HPHGreyLight};
      font-size: 1rem;
      font-weight: 500;
    }
    &:-ms-input-placeholder {
      color: ${HPHGreyLight};
      font-size: 1rem;
      font-weight: 500;
    }
    &::placeholder {
      color: ${HPHGreyLight};
      font-size: 1rem;
      font-weight: 500;
    }
    &:focus,&::focus-visible{
      border:0;
      border-bottom:1px solid ${HPHSkyBlue};
      outline:none;
    }
    &:hover{
      border-color: ${HPHSkyBlue};
    }
    &.has-error {
      border-color:${HPHRed};
    }
    &:disabled{
      border-color: ${HPHGreyLight};
      color:${HPHGreyLight};
    }  
    &::-webkit-inner-spin-button,
    &::-webkit-calendar-picker-indicator {
        display: none;
        -webkit-appearance: none;
    }
`;

export const StyledInputErrorMsg = styled.span`
  color: ${HPHRed};
  display: block;
  font-family: ${MontserratFont};
  font-size: 0.875rem;
  font-style: italic;
  font-weight: 500;
  line-height: 1.2;    
  margin-top: .25rem;    
  position: absolute;    
  top: 100%;
  white-space: nowrap;
`;
