import styled, { css } from 'styled-components';
import { Background7, HPHWhiteSmoke, Background2, HPHSkyBlue, HPHGreyLight, ContainerBoxShadow } from '../Colors';
import { ContainerProps } from '../Container/Container';

const themes = {
    'theme1': css`
      background: ${Background7};  
      border: 1px solid ${HPHWhiteSmoke};
    `,
    'theme2': css`
      background: ${Background2};
      box-shadow: ${ContainerBoxShadow};
      border: 3px solid ${HPHSkyBlue};
    `,
    'theme3': css`
      box-shadow: ${ContainerBoxShadow};
      background: ${Background7};
      border: 1px solid ${HPHGreyLight};
    `,
    'theme4': css`
      box-shadow: ${ContainerBoxShadow};
      background: ${Background2};
    `,
  };
  
  const radius = {
    'roundAll': css`border-radius: 8px;`,
    'roundNone': css`border-radius: 0;`,
    'roundLeft': css`border-radius: 8px 0 0 8px;`,
    'roundRight': css`border-radius: 0 8px 8px 0;`,
  };
  
  const getTheme = (props) => themes[props.theme];
  
  const getRadius = (props) => radius[props.borderRadius];
  
  export const StyledDiv = styled.div<ContainerProps>`
  width: 100%;
  height: 100%;
  overflow: hidden;
  margin:0 auto;
  ${getTheme}
  ${getRadius}
  `;
  