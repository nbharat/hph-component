import styled, { css } from 'styled-components';
import { HPHGreyLight, MontserratFont, HPHSeaBlue, Background2, HPHSkyBlue, Background8 } from '../Colors';
import { InputSwitch } from 'primereact/inputswitch';
import { ToggleProps } from '../Toggle/Toggle';

const getDisabledProp = (ToggleProps) => {
  if (!ToggleProps.disabled) {
    return css`
      &:hover{
          ~ div{
            span {
              display: block;
          }
        }
    }`;    
  }
};

const ToggleDisable = (Props) => {
  if (Props.disabled) {
    return css`
    color: ${HPHGreyLight};
    `;
  }
};

export const StyledDiv = styled.div`
  display : flex;
  width: auto;
  height: auto;
  position: relative;
  padding: 0;
  border: 0;
  justify-content: center;
  align-items: center;
  align-content: center;
  cursor: pointer;
`;
export const StyledBox = styled.div`
  display : flex;
  width: auto;
  height: auto;
  position: relative;
  padding: 0;
  border: 0;
  justify-content: center;
  align-items: center;
  align-content: center;
  cursor: pointer;
`;
export const StyledLabel = styled.div<ToggleProps>`
  font-weight: 500;
  font-size: 0.875rem;
  letter-spacing: 0px;
  text-align: left;
  font-family: ${MontserratFont};
  color: ${HPHSeaBlue};
  padding: 0px 5px;
  ${ToggleDisable}
`;

export const StyledInputSwitch = styled(InputSwitch)<ToggleProps>`
&.p-inputswitch 
{
  position:relative;
  width: 2.5rem;
  height: 1.25rem;
  &.p-disabled{
    opacity: inherit;
  }
  ${getDisabledProp}
.p-inputswitch-slider       
  {
      background:${HPHGreyLight};
      border-radius: 30px;  
      &:before{
        background:${Background2};
        width: 16px;
        height: 16px;
        left: 0.2rem;
        margin-top: -0.5rem;
      }
    }
    &:not(.p-disabled)&:hover>.p-inputswitch-slider{
      background:${HPHGreyLight};
    }
    &.p-disabled>.p-inputswitch-slider{
      background:${Background8};
      &:before{
          background:${HPHGreyLight};
      }
  }
  &.p-inputswitch-checked
  {
    >.p-inputswitch-slider
    {
    background:${HPHSkyBlue};
    &:before{
      background:${Background2};
      transform: translateX(1.1rem);
    }
  }
  &.p-disabled>.p-inputswitch-slider{
        background:${Background8};
        &:before{
            background:${HPHGreyLight};
        }
    }
  &:not(.p-disabled)&:hover>.p-inputswitch-slider
    {
      background:${HPHSkyBlue};
    }      
  }
} 
`;
