import styled, { css } from 'styled-components';
import { HPHSeaBlueDark, HPHGreyDark, HPHSkyBlue, MontserratFont,Background7,Background8 } from '../Colors';
import { TieredMenu } from 'primereact/tieredmenu';
import { NavigationProps } from '../NavigationMenu/NavigationMenu';
const AngleRightIcon = require('../../../assets/Icon/Icon-angle-right.svg').default;

const BreakLine = (props) =>{
  if (props.breakLine){
    return css` 
    white-space: pre-line;
    `;
  }
  if (!props.breakLine){
    return css`
      white-space: nowrap;
      `;
  }
};

export const StyledDiv = styled.div`
  width: auto;
  height: auto;
  position: relative;
  margin: 0;
  padding: 0;
  border: 0;
`;

export const StyledTieredMenu = styled(TieredMenu)<NavigationProps>`
    &.p-component{
        display:none;
    }
    &.p-tieredmenu{
        background:${Background7};
        font-family: ${MontserratFont}; 
        border-radius: 8px;
        width: 15rem;
        padding:0.75rem 0;
        ul{
            .p-menuitem-link{
                padding:0.75rem 1.5rem;
                justify-content: space-between;
                .p-menuitem-text{
                    color:${HPHGreyDark};
                    font-size:1rem;
                    font-weight:500;
                    ${BreakLine}
                }
                .p-submenu-icon {
                    &.pi-angle-right {
                        width: 1rem;
                        height: 1rem;
                        mask-image: url(${AngleRightIcon});
                        background-color: ${HPHGreyDark};
                        mask-size: 100%;
                        -webkit-mask-size: 100%;
                        margin-left:20px
                    }
                }
                &:not(.p-disabled):hover{
                    background:${Background8};                                      
                    outline:none;
                    .p-menuitem-text{
                        color:${HPHSkyBlue};      
                    }
                    .p-submenu-icon {
                        &.pi-angle-right {
                            background-color: ${HPHSkyBlue};
                        } 
                    }
                }
                &:focus{
                    box-shadow:none;
                }
            }
            .p-menuitem.p-menuitem-active > .p-menuitem-link {
                background:${Background8};                                      
                outline:none;
                .p-menuitem-text{
                    color:${HPHSkyBlue};      
                }
                .p-submenu-icon {
                    &.pi-angle-right {
                        background-color: ${HPHSkyBlue};
                    }
                }
            }
        }
        .p-submenu-list{
            background:${Background7};
            font-family: ${MontserratFont}; 
            border-radius: 8px;
            width: auto;
            padding:0.75rem 0;
            min-width: initial;
            .p-menuitem-link .p-menuitem-text{
                ${BreakLine}
            }
        }
    }    
`;

export const StyledIcon = styled.div`
    position: relative;
    width: 50px;
    height: 50px;
    overflow: visible;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor:pointer;
    &:hover{ 
        cursor:pointer;
        background: ${HPHSeaBlueDark};
        border-radius: 5px;
    }  
    &:hover{
        ${StyledTieredMenu}{ 
            display:block;
            position: absolute;
            left: 100%;
            top:0;
        }
    }
`;