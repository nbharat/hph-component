import styled, { css } from 'styled-components';
import { MontserratFont } from '../Colors';
import { TooltipsProps } from '../Tooltips/Tooltips';

const getTooltipArrow = (props) => {
    if (!props.toolTipArrow) {
      return css`
        &::after {
          border:none;
        }
      `;
    }
  };
  
  const getTooltipPlacement = (props) => {
    let style;
    switch (props.toolTipPlacement) {
      case 'top':
        style = css`
          transform: translateX(-50%);
          left: 50%;
          top:0;
          margin-top: -44px;
          &::after{
              top: 100%;
              border-top-color: rgba(34,34,34,0.8);
          }
        `;
        break;
      case 'bottom':
        style = css`
          transform: translateX(-50%);
          left: 50%;
          margin-top: 7px;
          top:100%;
          &::after{
              bottom: 100%;
              border-bottom-color: rgba(34,34,34,0.8);
          }
        `;
        break;
      case 'left':
        style = css`
          transform: translate(-100%,-50%);
          margin-left: -6px;
          top: 50%;
          left:0;
          &::after{
              left: auto;
              right: -11px;
              top: 50%;
              transform: translateY(-50%);
              border-left-color: rgba(34,34,34,0.8);
          }
        `;
        break;
      case 'right':
        style = css`
          transform: translate(100%,-50%);
          top: 50%;
          right: -6px;
          left: auto;
          &:after{
              left: -5px;
              top: 50%;
              transform: translateY(-50%);
              border-right-color: rgba(34,34,34,0.8);
          }
        `;
        break;
    
      default:
        break;
    }
    return style;
  };
  
  const topRightSmall = (props) => props.size === 'small' && css`
  &::after{
    margin-left: 8px;
  }
  `;
  const topRightMedium = (props) => props.size === 'medium' && css`
  &::after{
    margin-left: 12px;
  }
  `;
  
  const topLeftSmall = (props) => props.size === 'small' && css`
  &::after{
    right: 8px;
  }
  `;
  const topLeftMedium = (props) => props.size === 'medium' && css`
  &::after{
    right: 12px;
  }
  `;
  
  const bottomRightSmall = (props) => props.size === 'small' && css`
  &::after{
    margin-left: 7px;
  }
  `;
  const bottomRightMedium = (props) => props.size === 'medium' && css`
  &::after{
    margin-left: 11px;
  }
  `;
  
  const bottomLeftSmall = (props) => props.size === 'small' && css`
  right: 7px;
  `;
  const bottomLeftMedium = (props) => props.size === 'medium' && css`
  right: 12px;
  `;
  
  const getSizeTooltipPlacement = (props) => {
    let style;
    switch (props.sizetooltipplacement) {
      case 'topRight':
        style = css`
        transform: translateX(0);
        left: 0%;
        top:0;
        margin-top: -44px;
        &::after{
          top: 100%;
          border-top-color: rgba(34,34,34,0.8);
          left: 0;
          margin-left: 16px;
          ${topRightSmall}
          ${topRightMedium}
        }
        `;
        break;
      case 'topLeft':
        style = css`
        transform: none;
        top:0;
        margin-top: -44px;
        right: 0;
        &::after{
          top: 100%;
          border-top-color: rgba(34,34,34,0.8);
          right: 16px;
          left: auto;
          margin-left: 0;
          ${topLeftSmall}
          ${topLeftMedium}
        }    
        `;
        break;
      case 'bottomRight':
        style = css`
        transform: translateX(0);
        margin-top: 7px;
        left: 0;
        top: 100%;
        &::after{
          bottom: 100%;
          border-bottom-color: rgba(34,34,34,0.8);
          left: 0;
          margin-left: 16px;
          ${bottomRightSmall}
          ${bottomRightMedium}
        }  
        `;
        break;
      case 'bottomLeft':
        style = css`
        transform: none;
        margin-top: 7px;
        right: 0;
        top: 100%;
        &::after{
          bottom: 100%;
          border-bottom-color: rgba(34,34,34,0.8);
          right: 16px;
          left: auto;
          margin-left: 0;
          ${bottomLeftSmall}
          ${bottomLeftMedium}
        }
        `;
        break;
    
      default:
        break;
    }
    return style;
  };
  
  export const StyledTooltip = styled.div<TooltipsProps>`
  span{
    background: rgba(34,34,34,0.8);
    border-radius: 4px;
    color: #fff;
    display: none;
    font-size: 0.875rem;    
    font-family: ${MontserratFont}; 
    font-weight: 500;
    letter-spacing: 0px;
    line-height: 1.2;
    padding:0.625rem;    
    position: absolute;
    text-align: center;
    white-space: pre;
    &::after{
        content: " ";
        border: 6px solid transparent;
        left: 50%;        
        height: 0px;
        width: 0px;
        position: absolute;
        pointer-events: none;
        margin-left: -6px;
    }
    ${getTooltipPlacement}
    ${getSizeTooltipPlacement}
    ${getTooltipArrow} 
  } 
  `;
  
  export const sizetoolTipPlacementObj = {
    largebottom: 'largebottom',
    mediumbottom: 'mediumbottom',
    smallbottom: 'smallbottom',
    largetop: 'largetop',
    mediumtop: 'mediumtop',
    smalltop: 'smalltop',
    largeright: 'largeright',
    mediumright: 'mediumright',
    smallright: 'smallright',
    largeleft: 'largeleft',
    mediumleft: 'mediumleft',
    smallleft: 'smallleft',
  
    smalltopLeft:'topLeft',
    smalltopRight:'topRight',
    smallbottomLeft:'bottomLeft',
    smallbottomRight:'bottomRight',
  
    mediumtopLeft:'topLeft',
    mediumtopRight:'topRight',
    mediumbottomLeft:'bottomLeft',
    mediumbottomRight:'bottomRight',
  
    largetopLeft:'topLeft',
    largetopRight:'topRight',
    largebottomLeft:'bottomLeft',
    largebottomRight:'bottomRight',
  
  };
  