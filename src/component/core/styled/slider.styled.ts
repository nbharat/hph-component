import { Slider } from 'primereact/slider';
import styled, { css } from 'styled-components';
import { HPHSeaBlue, HPHRed, HPHWhite, HPHGreyDark, HPHSkyBlue, HPHGreyLight, MontserratFont } from '../Colors';

const StyledSlider = styled(Slider)` 
    .p-slider-handle{
        box-shadow: 0px 1px 2px 0px rgba(0,0,0,10%)!important;
        border-color:  ${HPHSeaBlue} !important;
        background-color:  ${HPHSeaBlue} !important;
    }
    &:hover{
        cursor: pointer;
        ~ output{
            opacity: 1;
        }
    }  
`;
const SliderControl = styled.div`
    display: flex;
    align-items: flex-start;
`;
const SliderLabel = styled.label`
    font-weight: 700;
    font-family: ${MontserratFont};
    color: ${HPHSeaBlue};
    font-size: 0.875rem;
    letter-spacing: 0px;
    line-height: 1.2;
    display: block;
    margin-bottom: 1rem;
    text-transform: capitalize;
`;

interface SliderInputProps {
  errMsg: boolean;
}

const SliderInputErrMsgStyle = (props) => {
  if (props.errMsg) {
    return css`
        border-bottom: 1px solid  ${HPHRed};
    `;
  }
};

const SliderInput = styled.div<SliderInputProps>`
    margin-left: 20px;
    input{
        font-family: ${MontserratFont};
        color: ${HPHGreyDark};   
        font-size: 1rem;
        font-weight: 500;
        padding:0 10px 5px 10px;
        border:0;
        border-bottom: 1px solid  ${HPHGreyDark};
        text-align: center;
        max-width: 5rem;
        ${SliderInputErrMsgStyle}
        &:hover{
            border-bottom: 1px solid ${HPHSkyBlue};
        }
        &:focus{
            outline: none;
            border: 0;
            border-bottom: 1px solid ${HPHSkyBlue};

        }
        &:disabled,&.disabled{
            color: ${HPHGreyLight};
            pointer-events: none;
            border-bottom: 1px solid ${HPHGreyLight};
            background-color: ${HPHWhite};
        }
    } 
`;
const SliderRangeInput = styled.div`
    display: flex;
`;

const SliderWrapperVerticalStyle = (props) => {
  if (props.vertical) {
    return ( 
      css` 
          position: relative;
          ${SliderLabel}{
              text-align: center;
              margin-bottom: 1rem;
          }
          ${SliderControl}{
              flex-direction: column-reverse; 
              align-items: center;           
              ${SliderInput}{
                  margin-left: 0;
                  margin-bottom: 20px;
              }
          }
          ul {
              flex-direction: column-reverse;
              position: absolute;
              top:0;
              bottom: 0;
              transform: translateX(-100%);
              left: -10px;
              text-align: right;
              margin-top: 0;
          }
          em{
              text-align: center;
              position: absolute;
              margin-top: .5rem;
              transform: translateX(-50%);
              white-space: nowrap;
          }
          output{
              right: -0.75rem;
              transform: translate(100%, 10px);
              left: auto;
              margin-top: 0;
              top: auto;
          &::after{
              border-top: 6px solid transparent;
              border-bottom: 7px solid transparent;
              border-right: 10px solid ${HPHSeaBlue};
              border-left: 0;
              top: 50%;
              left: 0;
              margin-top: -6px;
              margin-left: -6px;
          }
          `
    );
  }
};

interface StyleWrapperProps{
  vertical: boolean;
}

const SliderWrapper = styled.div<StyleWrapperProps>`
    ul {
        display: flex;
        justify-content: space-between;
        margin: 0.5rem 0 0;
        list-style: none;
        padding:0;
        li{
            font-size: 0.75rem;
            color: ${HPHGreyLight};
            font-weight: 500;
            font-family: ${MontserratFont};
            list-style: none;
        }
    } 
    ${SliderInput}{    
        ${SliderRangeInput}{             
            input{
                max-width: 3rem;
                padding: 0 2px 5px 2px;
            }
            span{
                border-bottom: 1px solid  ${HPHGreyDark};
                padding-bottom: 5px;
            }
            input.errorMsg ~ span{
                border-bottom: 1px solid ${HPHRed}; 
            }
            input:focus ~ span{
                border-bottom: 1px solid ${HPHSkyBlue};
            }
            input:disabled ~ span,input.disabled ~ span{
                border-bottom: 1px solid ${HPHGreyLight};
            }
            &:hover{
                input:not([disabled]),
                input:not([disabled])~span
                {
                    border-bottom: 1px solid ${HPHSkyBlue}; 
                }
            } 
        }
    }
    output{
        position: absolute;
        background: ${HPHSeaBlue};
        display: inline-flex;
        justify-content: center;
        align-items: center;
        color: ${HPHWhite};
        width: auto;       
        font-family: ${MontserratFont};
        min-width: 0.65625rem;
        height: 1.3125rem;
        border-radius: 0.65625rem;
        font-size: .875rem;
        line-height: 1;
        font-weight: 500;
        padding: 0 0.375rem;
        top: 0;
        margin-top: -2rem;
        transform: translatex(-50%);
        opacity: 0;
        &::after{
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            border-top: 0.375rem solid ${HPHSeaBlue};
            border-left: 0.25rem solid transparent;
            border-right: 0.25rem solid transparent;
            top: 100%;
            left: 50%;
            margin-left: -0.25rem;
            margin-top: -1px;
        }
    }

    em {
        font-style: italic;
        margin-top: .5rem;
        display: block;
        color: ${HPHRed};
        font-family: ${MontserratFont};
        font-size: 0.875rem;
        line-height: 1;
        position: absolute;
    }
    ${SliderWrapperVerticalStyle}
}     
`;

const SliderInner = styled.div`
    position: relative;
    .p-slider{
        font-family: ${MontserratFont};         
        background: #F1F2F3;          
        &.p-slider-horizontal{
            width: 250px;            
            height: 8px;
        }
        &.p-slider-vertical{
            height: 250px;
            width: 8px;
        }
        .p-slider-range{
            background-color: ${HPHSkyBlue};
            border-radius: 6px;
        }
        &.p-disabled{
            .p-slider-range{
                background-color:${HPHGreyLight};                
            }
            .p-slider-handle{
                border-color:  ${HPHWhite} !important;
                background-color:  ${HPHWhite} !important;
                box-shadow: 0px 1px 5px 0px rgba(0,0,0,10%) !important;
            }
        }
    }
`;


export { StyledSlider, SliderWrapper, SliderLabel, SliderControl, SliderInner, SliderInput, SliderRangeInput };