import styled from 'styled-components';
import { Background7, HPHSkyBlue, HPHGreyLight, ContainerBoxShadow, HPHGreyDark, RightClickMenuItemLink, MontserratFont } from '../Colors';
import { ContextMenu } from 'primereact/contextmenu';
import { getImgUrl } from '../../../common/getImgUrl';
const AngleRightIcon = require('../../../assets/Icon/Icon-angle-right.svg').default;

export const StyledContextMenu = styled(ContextMenu)`
border: 1px solid ${HPHGreyLight};
border-radius: 8px;
font-size: 0.875rem;
padding: 0;
width: auto;
height: auto;
box-shadow: ${ContainerBoxShadow};
background: ${Background7};
ul{
    padding: 3px 0;
    .p-menuitem{
        padding: 0;            
        font-family: ${MontserratFont};   
    }
    .p-menuitem-link {
        padding: 10px 30px 10px 15px;
        display: flex;
        background-color: transparent;
        color: ${HPHGreyDark};   
        position: relative;  
        font-size: 0.875rem;
        font-weight: 500;
        font-family: ${MontserratFont};      
        .hph-icon{
            background-color: ${HPHGreyDark};
            width: 1rem;
            height: 1rem;
            mask-size: 100%;
            -webkit-mask-size: 100%;
        }             
        &:hover{
            background-color: ${RightClickMenuItemLink};
            color:  ${HPHSkyBlue};
            > .p-menuitem-text{
                color:  ${HPHSkyBlue};
            }
            .hph-icon{
                background-color: ${HPHSkyBlue};
            } 
        }
        &.p-menuitem-active > .p-menuitem-link {
            background: ${RightClickMenuItemLink};
            color:  ${HPHSkyBlue};
            > .p-menuitem-text{
                color:  ${HPHSkyBlue};
            }
            .hph-icon{
                background-color: ${HPHSkyBlue};
            } 
        }
    }
    .p-menuitem.p-menuitem-active > .p-menuitem-link {
        background: ${RightClickMenuItemLink};
        > .p-menuitem-text{
            color:  ${HPHSkyBlue};
        }
        .hph-icon{
            background-color: ${HPHSkyBlue};
        } 
    }
}      
.p-submenu-list {
    border: 1px solid ${HPHGreyLight};
    border-radius: 8px;
    font-size: 0.875rem;     
    padding: 3px 0;
    width: auto;
    height: auto;
    box-shadow: ${ContainerBoxShadow};
    background: ${Background7};
} 
.p-submenu-icon {
    &.pi {
        &.pi-angle-right {
            width: 1.5rem;
            height: 1.5rem;
            mask-image: url(${AngleRightIcon});
            background-color: ${HPHSkyBlue};
            position: absolute;
            right: 2px;
            mask-size: 100%;
            -webkit-mask-size: 100%;
        }
    }
}
.hph-icon {
    width: 1.5rem;
    height: 1.5rem;
    background-color: #009bde;

    &.Icon-add-column {
        mask-image: url(${getImgUrl('Icon-add-column')});
    }
    
    &.Icon-add-row {
        mask-image: url(${getImgUrl('Icon-add-row')});
    }
    
    &.Icon-add {
        mask-image: url(${getImgUrl('Icon-add')});
    }
    
    &.Icon-adjust {
        mask-image: url(${getImgUrl('Icon-adjust')});
    }
    
    &.Icon-alarm-fill {
        mask-image: url(${getImgUrl('Icon-alarm-fill')});
    }
    
    &.Icon-alarm {
        mask-image: url(${getImgUrl('Icon-alarm')});
    }
    
    &.Icon-alert-triangle {
        mask-image: url(${getImgUrl('Icon-alert-triangle')});
    }

    &.Icon-alert {
        mask-image: url(${getImgUrl('Icon-alert')});
    }

    &.Icon-anchor {
        mask-image: url(${getImgUrl('Icon-anchor')});
    }

    &.Icon-angle-down {
        mask-image: url(${getImgUrl('Icon-angle-down')});
    }

    &.Icon-angle-left {
        mask-image: url(${getImgUrl('Icon-angle-left')});
    }

    &.Icon-angle-right {
        mask-image: url(${getImgUrl('Icon-angle-right')});
    }

    &.Icon-angle-up {
        mask-image: url(${getImgUrl('Icon-angle-up')});
    }

    &.Icon-arrow-left {
        mask-image: url(${getImgUrl('Icon-arrow-left')});
    }

    &.Icon-arrow-right {
        mask-image: url(${getImgUrl('Icon-arrow-right')});
    }

    &.Icon-attach {
        mask-image: url(${getImgUrl('Icon-attach')});
    }

    &.Icon-auto-text {
        mask-image: url(${getImgUrl('Icon-auto-text')});
    }   

    &.Icon-bitt {
        mask-image: url(${getImgUrl('Icon-bitt')});
    }   

    &.Icon-calendar {
        mask-image: url(${getImgUrl('Icon-calendar')});
    }

    &.Icon-capture {
        mask-image: url(${getImgUrl('Icon-capture')});
    }

    &.Icon-chart-view {
        mask-image: url(${getImgUrl('Icon-chart-view')});
    }

    &.Icon-clock-fill {
        mask-image: url(${getImgUrl('Icon-clock-fill')});
    }

    &.Icon-clock {
        mask-image: url(${getImgUrl('Icon-clock')});
    }

    &.Icon-column-setting {
        mask-image: url(${getImgUrl('Icon-column-setting')});
    }

    &.Icon-column {
        mask-image: url(${getImgUrl('Icon-column')});
    }

    &.Icon-connect {
        mask-image: url(${getImgUrl('Icon-connect')});
    }

    &.Icon-container {
        mask-image: url(${getImgUrl('Icon-container')});
    }

    &.Icon-copy {
        mask-image: url(${getImgUrl('Icon-copy')});
    }

    &.Icon-cross-small {
        mask-image: url(${getImgUrl('Icon-cross-small')});
    }

    &.Icon-cross {
        mask-image: url(${getImgUrl('Icon-cross')});
    }

    &.Icon-cursor-drag {
        mask-image: url(${getImgUrl('Icon-cursor-drag')});
    }

    &.Icon-cursor-leftAdjust {
        mask-image: url(${getImgUrl('Icon-cursor-leftAdjust')});
    }

    &.Icon-cursor-leftright {
        mask-image: url(${getImgUrl('Icon-cursor-leftright')});
    }

    &.Icon-cursor-move {
        mask-image: url(${getImgUrl('Icon-cursor-move')});
    }

    &.Icon-cursor-pointer {
        mask-image: url(${getImgUrl('Icon-cursor-pointer')});
    }

    &.Icon-cursor-resize {
        mask-image: url(${getImgUrl('Icon-cursor-resize')});
    }

    &.Icon-cursor-rightAdjust {
        mask-image: url(${getImgUrl('Icon-cursor-rightAdjust')});
    }

    &.Icon-cursor-stop {
        mask-image: url(${getImgUrl('Icon-cursor-stop')});
    }

    &.Icon-cursor-updown {
        mask-image: url(${getImgUrl('Icon-cursor-updown')});
    }

    &.Icon-cut {
        mask-image: url(${getImgUrl('Icon-cut')});
    }

    &.Icon-cwp-bay {
        mask-image: url(${getImgUrl('Icon-cwp-bay')});
    }

    &.Icon-cwp-DS-RR_QW {
        mask-image: url(${getImgUrl('Icon-cwp-DS-RR_QW')});
    }

    &.Icon-cwp-DS-RR_WQ {
        mask-image: url(${getImgUrl('Icon-cwp-DS-RR_WQ')});
    }

    &.Icon-cwp-DS-TT_QW {
        mask-image: url(${getImgUrl('Icon-cwp-DS-TT_QW')});
    }

    &.Icon-cwp-DS-TT_WQ {
        mask-image: url(${getImgUrl('Icon-cwp-DS-TT_WQ')});
    }

    &.Icon-cwp-sameComplete {
        mask-image: url(${getImgUrl('Icon-cwp-sameComplete')});
    }

    &.Icon-derrick {
        mask-image: url(${getImgUrl('Icon-derrick')});
    }

    &.Icon-detach {
        mask-image: url(${getImgUrl('Icon-detach')});
    }

    &.Icon-disease {
        mask-image: url(${getImgUrl('Icon-disease')});
    }

    &.Icon-divide {
        mask-image: url(${getImgUrl('Icon-divide')});
    }

    &.Icon-doc {
        mask-image: url(${getImgUrl('Icon-doc')});
    }
}
`;