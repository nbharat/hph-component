import styled, { css } from 'styled-components';
import { HPHWhiteSmoke, MontserratFont } from '../Colors';
import { DialogProps } from '../OverflowingDialog/OverflowingDialog';


const dialogPosition = {
  'right': css`
    position: absolute;
    top: -25px;
    left: 100%;
    margin-left: 30px;
    &::after{
      content: " ";
      width: 0; 
      height: 0; 
      border-top: 1.25rem solid transparent;
      border-bottom: 1.25rem solid transparent;
      border-right: 1.25rem solid ${HPHWhiteSmoke};
      filter: drop-shadow(0px 0px 0px #6D8CAB);
      position: absolute;
      top:1.25rem;
      right: 100%;
    }
  `,
  'left': css`
    position: absolute;
    left: 0;
    top: 0;
    transform: translateX(-100%);
    margin-left: -13px;
    top: -25px;
    &::after{
      content: " ";
      width: 0; 
      height: 0; 
      border-top: 1.25rem solid transparent;
      border-bottom: 1.25rem solid transparent;
      border-left: 1.25rem solid ${HPHWhiteSmoke};
      filter: drop-shadow(0px 0px 0px #6D8CAB);
      position: absolute;
      top:1.25rem;
      left:100%;    
    }
  `,
  'topLeft': css`
    position: absolute;
    right: -40px;
    top: 0;
    transform: translateY(-100%);
    margin-top: -20px;
    &::after{
      content: " ";
      width: 0; 
      height: 0; 
      border-left: 1.25rem solid transparent;
      border-right: 1.25rem solid transparent;
      border-top: 1.25rem solid ${HPHWhiteSmoke};
      filter: drop-shadow(0px 0px 0px #6D8CAB);
      position: absolute;
      top:100%;
      right: 1.25rem;
    }
  `,
  'topRight': css`
    position: absolute;
    left: -25px;
    top: 0;
    transform: translateY(-100%);
    margin-top: -20px;
    &::after{
      content: " ";
      width: 0; 
      height: 0; 
      border-left: 1.25rem solid transparent;
      border-right: 1.25rem solid transparent;
      border-top: 1.25rem solid ${HPHWhiteSmoke};
      filter: drop-shadow(0px 0px 0px #6D8CAB);
      position: absolute;
      top:100%;
      Left: 1.25rem;
    }
  `,
  'bottomLeft': css`
    position: absolute;
    top: 100%;
    right: -40px;
    margin-top: 18px;
    &::after{
      content: " ";
      width: 0; 
      height: 0; 
      border-left: 1.25rem solid transparent;
      border-right: 1.25rem solid transparent;
      border-bottom: 1.25rem solid ${HPHWhiteSmoke};
      filter: drop-shadow(0px 0px 0px #6D8CAB);
      position: absolute;
      bottom:100%;
      right:1.25rem;
  }
  `,
  'bottomRight': css`
    position: absolute;
    top: 100%;
    left: -25px
    margin-top: 18px;
    &::after{
      content: " ";
      width: 0; 
      height: 0; 
      border-left: 1.25rem solid transparent;
      border-right: 1.25rem solid transparent;
      border-bottom: 1.25rem solid ${HPHWhiteSmoke};
      filter: drop-shadow(0px 0px 0px #6D8CAB);
      position: absolute;
      bottom:100%;
      Left: 1.25rem;
    }
  `, 
};
const getDialogPosition = (Props) => dialogPosition[Props.boxPosition];
const getWidth = (Props) => Props.width && css`width: ${Props.width}`;
const getHeight = (Props) => Props.height && css`height: ${Props.height}`;
const getPaddingTop = (Props) => Props.paddingTop && css`padding-top: ${Props.paddingTop}`;
const getPaddingBottom = (Props) => Props.paddingBottom && css`padding-bottom: ${Props.paddingBottom}`;
const getPaddingLeft = (Props) => Props.paddingLeft && css`padding-left: ${Props.paddingLeft}`;
const getPaddingRight = (Props) => Props.paddingRight && css`padding-right: ${Props.paddingRight}`;

export const StyledDiv = styled.div<DialogProps>`
  background: rgba(247, 247, 247);
  border-radius: .5rem;
  border: 1px solid ${HPHWhiteSmoke};
  box-shadow: 0px 0px 3px -1px rgb(109 140 171);
  cursor: pointer;
  ${getDialogPosition}
  ${getWidth}
  ${getHeight}
  ${getPaddingTop}
  ${getPaddingBottom}
  ${getPaddingLeft}
  ${getPaddingRight}
`; 
export  const StyledChildren = styled.div<DialogProps>`
  font-family: ${MontserratFont}; 
  word-break:break-all;
  font-weight: 500;
  font-size: 1rem;
`;
export const StyledBox = styled.div`
  position: relative;
  width: 30px;
  margin: 0 auto;
  height: 30px;  
`;