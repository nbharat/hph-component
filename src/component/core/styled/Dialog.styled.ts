import styled from 'styled-components';
import { Dialog } from 'primereact/dialog';
import { HPHGrey, HPHGreyDark, MontserratFont, HPHWhite } from '../Colors';
import { DialogModalProps } from '../Dialog/ModalDialog';
import { getImgUrl } from '../../../common/getImgUrl';

export const StyledDiv = styled.div`
  position: relative;
  margin: 0;
  padding: 0;
  border: 0;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

export const StyleHeader = styled.div`
  display: flex;
  flex-direction: column;
  width: auto;
`;

export const StyledFooter = styled.div`
  width: auto;
  display: flex;
  float: right;
`;

export const StyledDialog = styled(Dialog)<DialogModalProps>`
  &.p-dialog {
      width: 50vw; 
      padding:0;
      font-family:${MontserratFont};
      background:${HPHWhite};
      border-radius: 0.5rem;
      box-shadow: 0px 1px 5px 5px rgba(0, 0, 0, 0.1);
      min-width: 200px;
      min-height: 175px;
      oveflow:hidden;
      .p-dialog-header{
        padding:1.875rem 1.875rem 0;
        .p-dialog-title{
          font-size:1rem;
          font-family:${MontserratFont};
          color:${HPHGreyDark};
        }
        .p-dialog-header-icon{
          display: none;
          border:0;
          box-shadow: none;
        }
      }
      .p-dialog-content{
        padding:0.5rem 1.875rem 1.5rem;
        font-family:${MontserratFont};
        font-size: 0.875rem;
        font-weight: 500;
         p {
            margin: 0;
        }
      }
      .p-dialog-footer{
        font-family:${MontserratFont};
        padding: 0 1.875rem 1.875rem ;
        button{
              margin: 0 0.25rem;
        }
      }
  }
  .p-component-overlay{
      background-color: rgba(0,0,0,0.5) !important;
  }
`;

export const StyledSubTitle = styled.span` 
    display: block;
    font-family: ${MontserratFont}; 
    font-size:0.875rem;
    font-weight: 700;
    color:${HPHGrey};
    text-transform:uppercase;
`;

export const StyledModalHeading = styled.span` 
  display: block;
  font-size:1.5rem;
  font-family:${MontserratFont};
  color:${HPHGreyDark};
  margin-top:5px;
  text-transform:uppercase;
`;  

export const StyledCross = styled.button<DialogModalProps>`
  position: absolute;
  right: 2rem;
  mask-image: url(${getImgUrl('Icon-cross')});
  -webkit-mask-size: 1.5rem;
  -webkit-mask-repeat: no-repeat;
  width: 1.5rem;
  height: 1.5rem;
  padding: 0px;
  background: #6c757d;
  cursor: pointer;
`;