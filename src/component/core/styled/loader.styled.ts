import styled, { css } from 'styled-components';
import { keyframes } from 'styled-components'
import { HPHSkyBlue, HPHWhite } from '../Colors';
import { LoaderProps } from '../Loader/Loader'

const breatheAnimation = keyframes`
  0% {
    margin-top: 0;
  }
  45% {
    margin-top: -55px;
  }
  90% {
    margin-top: 0px;
  }
`;

const SmallbreatheAnimation = keyframes`
  0% {
    margin-top: 0;
  }
  45% {
    margin-top: -30px;
  }
  90% {
    margin-top: 0px;
  }
`;

const spinnerAnimation = keyframes`
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
`;


const LoaderSize = {
  'Small': css`
    width: 24px;
    height: 24px;
  `,
  'Medium': css`
    width: 36px;
    height: 36px;
  `,
  'Large': css`
    width: 54px;
    height: 54px;
  `,
  };

  const StripeSize = {
    'Small': css`
      width: 6px;
      height: 6px;
      margin:0 2.5px;
    `,
    'Medium': css`
      width: 8px;
      height: 8px;
      margin:0 3px;
    `,
    'Large': css`
      width: 10px;
      height: 10px;
      margin:0 5px;
    `,
  };

  const Animationsize = {
    'Small': css`
      animation: ${SmallbreatheAnimation} 1.5s linear infinite
    `,
    'Medium': css`
        animation: ${breatheAnimation} 2s linear infinite;
    `,
    'Large': css`
      animation: ${breatheAnimation} 2s linear infinite;
    `,
  };

  const GetLoaderSize = (props) => LoaderSize[props.size];

  const GetStripeSize = (props) => StripeSize[props.size];

  const GetAnimation = (props) => Animationsize[props.size];

export const StyleLoader = styled.div`
  width: auto;
  height: 5rem;
  margin: auto;
  display: flex;  
  align-items: center;
  justify-content:center;
`;
export const LoaderCircle = styled.div`
  background: ${HPHSkyBlue};
  border-radius: 50%;
  margin: 0 5px; 
`;
export const LoaderCircle2 = styled.div`
  background: ${HPHSkyBlue};
  border-radius: 50%;
  margin: 0 5px;
`;
export const LoaderCircle3 = styled.div`
  background: ${HPHSkyBlue};
  border-radius: 50%;
  margin: 0 5px;    
`;


export const StyleSpinner = styled.div`
  border-radius: 50%;
  background: ${HPHSkyBlue};
  background: -moz-linear-gradient(left, ${HPHSkyBlue} 10%, rgba(0,155,222, 0) 42%);
  background: -webkit-linear-gradient(left, ${HPHSkyBlue} 10%, rgba(0,155,222, 0) 42%);
  background: -o-linear-gradient(left, ${HPHSkyBlue} 10%, rgba(0,155,222, 0) 42%);
  background: -ms-linear-gradient(left, ${HPHSkyBlue} 10%, rgba(0,155,222, 0) 42%);
  background: linear-gradient(to right, ${HPHSkyBlue} 10%, rgba(0,155,222, 0) 42%);
  position: relative;
  animation: ${spinnerAnimation} 1.4s infinite linear;
  transform: translateZ(0);
  &:before{
    width: 50%;
    height: 50%;
    background: ${HPHSkyBlue};
    border-radius: 100% 0 0 0;
    position: absolute;
    top: 0;
    left: 0;
    content: '';
  }
  &:after{
    background: ${HPHWhite};
    width: 75%;
    height: 75%;
    border-radius: 50%;
    content: '';
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
`;

export const StyledDiv = styled.div<LoaderProps>`
  width:auto;
  height:100vh;
  display:flex;
  align-items:center;
  justify-content:center;
  z-index:1;
  margin:0 auto;
  ${StyleSpinner}{
    ${GetLoaderSize}
  }
  ${LoaderCircle},${LoaderCircle2},${LoaderCircle3}{
    ${GetStripeSize};
    ${GetAnimation};
  }
  ${LoaderCircle2}{    
    animation-delay: 0.3s;
  }
  ${LoaderCircle3}{    
    animation-delay: 0.6s;
  }
`;