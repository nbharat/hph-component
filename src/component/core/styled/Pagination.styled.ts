import { Paginator } from 'primereact/paginator';
import styled from 'styled-components';
import { PaginatorProps } from '../Pagination/Pagination';
import { HPHWhiteSmoke, HPHSkyBlue, MontserratFont, HPHSeaBlue, HPHSeaBlueDark, HPHGreyDark, HPHWhite, HPHGrey } from '../Colors';

export const StyledDiv = styled.div<PaginatorProps>`
    position: relative;
    background: ${HPHWhiteSmoke};
    border-top: 2px solid ${HPHSkyBlue};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    overflow: visible;
    padding:23px 20px;
`;

export const StyledPagination = styled(Paginator)<PaginatorProps>`
&.p-paginator{
    background: none;
    border: none;
    border-width: 0;
    padding: 0rem;
    border-radius: 0px;
    .p-paginator-pages{
        .p-paginator-page {
            border-radius: 50%;
            border: none;
            background: none;
            font-size: 1rem;
            font-family: ${MontserratFont};
            color: ${HPHSeaBlue};
            font-weight: 500;
            margin: 0px 5px;
            height: 1.5rem;
            min-width: 1.5rem;
            cursor: pointer;
            &:not(.p-highlight):hover{
                background: ${HPHSeaBlueDark};
                height: 1.5rem;
                width: 1.5rem;
                color: ${HPHSeaBlue};
            }
            &.p-highlight{
                background: ${HPHSkyBlue};
                height: 1.5rem;
                width: 1.5rem;
                color: ${HPHWhite};
                padding: 3px 5px;
            }
        }
    }
    .p-paginator-prev{
       min-width: 2rem;
       height:2rem; 
       color: ${HPHSeaBlue};
       margin: 0 5px;
       &:not(.p-disabled):not(.p-highlight){
            &:hover{
              background: ${HPHSeaBlueDark};
              border-color: transparent;
              color: ${HPHSeaBlue};
            }
        }
    }
    .p-paginator-next{
        min-width: 2rem;
        height: 2rem; 
        color: ${HPHSeaBlue};
        margin: 0 5px;
        &:not(.p-disabled):not(.p-highlight){
            &:hover{
              background: ${HPHSeaBlueDark};
              border-color: transparent;
              color: ${HPHSeaBlue};
            }
        }
    }
}
.p-link{
    &:focus{
        box-shadow: none;
    }
}
`;
export const StyledText = styled.div<PaginatorProps>`
  width: auto;
  margin: 0 5px;
  font-size: 1rem;
  letter-spacing: 0px;
  line-height: 1.2;
  color: ${HPHGreyDark};
  font-weight: 500;
  font-style: normal;
  font-family: ${MontserratFont};
`;

export const StyledInput = styled.input`
    outline:none;
    background: ${HPHWhite};
    border: none;
    font-family: ${MontserratFont};
    font-weight: 500;
    text-align: center;
    color: ${HPHGreyDark};
    line-height: 1.4;
    font-size: 1rem;
    width: 2rem;
    height: 2rem;
    margin: 0 5px;
    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin: 0; 
    }
`;

export const StyledButton = styled.button`
    border: none;
    width: auto;
    height: 2rem;
    background: ${HPHSkyBlue};
    color: ${HPHWhite};
    border-radius: 5px;
    margin: 0 0px 0 5px;
    cursor: pointer;
    font-size: 1rem;
    font-weight: 500;
    text-transform: capitalize;
`;

export const StyledSpan = styled.span`
    display: flex;
    align-items:center;
`;

export const StyledBox = styled.div`
    display: flex;
`;

export const StyledRecord = styled.div`
    font-size: 1rem;
    letter-spacing: 0px;
    line-height: 1.2;
    font-family: ${MontserratFont};
    font-weight: 500;
    color: ${HPHGrey};
    margin-right:1rem;
`;
export const StyledSelected = styled.div<PaginatorProps>`    
    font-size: 1rem;
    -webkit-letter-spacing: 0px;
    -moz-letter-spacing: 0px;
    -ms-letter-spacing: 0px;
    letter-spacing: 0px;
    line-height: 1.2;
    font-family: ${MontserratFont};
    font-weight: 500;
    color: ${HPHSkyBlue};
`;