import * as React from 'react';
import { StyledDiv } from './../styled/container.styled';


export interface ContainerProps {
  width?: number | string,
  height?: number | string,
  theme?: string | 'theme1' | 'theme2' | 'theme3' | 'theme4',
  borderRadius?: string | 'roundLeft' | 'roundRight' | 'roundAll' | 'roundNone',
  onClick?: (e: any) => void,
  [x:string]: any;
}

const Container: React.FC<ContainerProps> = ({ width = '100%', height = '100%', theme = 'theme3', borderRadius = 'roundAll', children, onClick, style = {}, ...rest }) => {
  return <>
    <StyledDiv 
      theme={theme}
      borderRadius={borderRadius}
      style={ style ? { ...style, width, height } : { width, height }}
      {...{ onClick }}
      {...rest}
    >
      {children}
    </StyledDiv>
  </>;
};

export { Container };
