import React from 'react';
import { HPHButton } from '../Button/Button';
import { SVGIcon } from '../SVGIcon/SVGIcon';
import { StyledDiv, StyledAlert, StyledIcon, StyledLabel, StyledCard, StyledText, StyledButton, StyledButton1, StyledCross } from '../styled/NotificationCard.styled';

export interface CardProps{
  id?: string | 'card',
  width?: number | string,
  height?: number | string,
  heading?: string,
  text?: string,
  disabledIcon?:boolean | false,
  disabledLabel?:boolean | false,
  disabledButton?:boolean | false,
  onClick?: any,
}

const  NotificationCard: React.FC<CardProps> = ({
  id = 'card',
  width = '400px',
  height = '150px',
  heading = 'Alert Issue 1',
  text = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nihil rem!',
  disabledIcon = false,
  disabledLabel = false,
  disabledButton = false,
  onClick,
}):any=>{
  return (
<>
<StyledDiv id={id} width={width} height={height} >
    {disabledIcon ? '' : <StyledIcon><StyledAlert/></StyledIcon>}
   <StyledCard >    
        {disabledLabel ? '' : <StyledLabel>{heading}</StyledLabel>}
        <StyledText> {text} </StyledText>
        {disabledButton ? '' :
        <StyledButton >
            <StyledButton1><HPHButton label={'Cancel'} size={'Small'} theme={'Secondary'} onClick={onClick}/></StyledButton1>
            <HPHButton label={'Confirm'} size={'Small'} theme={'Primary'}/>
        </StyledButton>}
    </StyledCard>
    {disabledButton ? <StyledCross onClick={onClick}><SVGIcon fileName="Icon-cross"/></StyledCross> : ''}
   
</StyledDiv>
</>
  );
};

export { NotificationCard };