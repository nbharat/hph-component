import { NotificationCard } from "./NotificationCard";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom';


const Props = {
    id:"card",
    width:'400px',
    height:'150px',
    heading: "Alert Issue 1",
    disabledIcon: false,
    disabledLabel: false,
    disabledButton: false,
   
  };

describe('Test', () => {
    test("NotificationCard id", () => {
        render(<NotificationCard />);
    });

    test("Notification Card id", () => {
        render(<NotificationCard id={Props.id} />);
        const container = document.querySelector('#card');
        expect(container).toBeTruthy();
    });
    test("Notification Card Width", () => {
        render(<NotificationCard id={Props.id} width={Props.width}/>);
        const container = document.querySelector('#card');
        expect(container).toBeTruthy();
    });
    test("Notification Card Height", () => {
        render(<NotificationCard id={Props.id} height={Props.height}/>);
        const container = document.querySelector('#card');
        expect(container).toBeTruthy();
    });
    test("Notification Card Heading", () => {
        render(<NotificationCard id={Props.id} heading={Props.heading}/>);
        const container = document.querySelector('#card');
        expect(container).toBeTruthy();
    });
    test("Notification Card Icon is not disable", () => {
        render(<NotificationCard id={Props.id}  disabledIcon={Props.disabledIcon}/>);
        const container = document.querySelector('#card');
        expect(container).toBeTruthy();
    });
    test("Notification Card Label is not disable", () => {
        render(<NotificationCard id={Props.id}  disabledButton={Props.disabledButton}/>);
        const container = document.querySelector('#card');
        expect(container).toBeTruthy();
    });
    test("Notification Card Button is not disable", () => {
        render(<NotificationCard id={Props.id}  disabledLabel={Props.disabledLabel}/>);
        const container = document.querySelector('#card');
        expect(container).toBeTruthy();
    });

});