import React from 'react';
import { StyledDiv, StyledCheckBox, StyledLabel, StyledInputErrorMsg } from '../styled/CheckBox.styled';

export interface Propscheck{
  label?:string | 'CheckBox',
  id?:string | 'CheckBox', 
  errorMessage?:any | '',
  disabled?: boolean,
  onChange?: (e: any) => void,
  checked?: boolean,
  [x:string]: any;
} 

const HPHCheckbox: React.FC<Propscheck> = ({
  label = 'CheckBox',
  id = 'CheckBox',
  errorMessage = '',
  disabled = false,
  onChange,
  checked = false,
  ...rest
}) : any => {
  return (
      <>
      <StyledDiv >
      <StyledCheckBox id={id}
      disabled={disabled}
      checked={checked} 
      onChange = {onChange}
      errorMessage={errorMessage}
      {...rest}>
      </StyledCheckBox>
      <StyledLabel disabled={disabled}>{label}</StyledLabel>
      <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
      </StyledDiv> 
      </>
  );
};

export { HPHCheckbox } ;