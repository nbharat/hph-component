
import { HPHCheckbox } from "./CheckBox";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id:"CheckBox",
  label: "label",
  disabled: false,
  errorMessage: "errorMsg",
};

describe('Test', () => {
    test("Check box id", () => {
        render(<HPHCheckbox />);
    });

    test("Check box id", () => {
        render(<HPHCheckbox id={Props.id} />);
        const container = document.querySelector('#CheckBox');
        expect(container).toBeTruthy();
    });
    test("Check box label", () => {
        render(<HPHCheckbox id={Props.id} label={Props.label}/>);
        const container = document.querySelector('#CheckBox');
        expect(container).toBeTruthy();
    });
    test("Check box is not disable", () => {
        render(<HPHCheckbox id={Props.id} label={Props.label} disabled={Props.disabled}/>);
        const container = document.querySelector('#CheckBox');
        expect(container).toBeTruthy();
    });

    test("Check box has no error message", () => {
        render(<HPHCheckbox id={Props.id} label={Props.label} disabled={Props.disabled} errorMessage={Props.errorMessage}/>);
        const container = document.querySelector('#CheckBox');
        expect(container).toBeTruthy();
    });
});