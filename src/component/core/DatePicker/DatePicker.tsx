/* eslint-disable import/extensions */
import React from 'react';
import { IconButton } from '../IconButtons/IconButtons';
import { 
  StyledInputWrapper, StyledInputLabel, StyledRequired,
  StyledCalendar,  StyledInputErrorMsg, DatePickerGlobalStyles,
} from '../styled/datePicker.styled';
import {
  HPHRed,
} from '../Colors';

interface Props {
  [x:string]: any;
}

const DatePicker: React.FC<Props> = ({  
  id = 'DatePicker',
  label = 'Label',
  disabled = false,
  required = false,
  helpIcon = false,
  errorMessage = '',
  toolTipText = 'Date format dd/mm/yyyy',
  date,  
  width = '200px',
  placeholder = 'dd/mm/yyyy',
  dateFormat = 'dd/mm/yy',
  onChange,
  ...rest
}): any => <StyledInputWrapper>
  <DatePickerGlobalStyles />
  <StyledInputLabel>
      {label} {required ? <StyledRequired>*</StyledRequired> : ''}
      {helpIcon && <IconButton fileName={'Icon-help'} size={'small'}  
        toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
        toolTipText={toolTipText} {...rest}
      />} 
  </StyledInputLabel>   
  <StyledCalendar         
    disabled={disabled}
    id={id}
    value={date}
    showIcon
    onChange={(e) => onChange && onChange(e.value)}  
    placeholder={placeholder}
    dateFormat={dateFormat}
    style={{ width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }}   
    {...rest} 
  />
  <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
</StyledInputWrapper>;

export { DatePicker };