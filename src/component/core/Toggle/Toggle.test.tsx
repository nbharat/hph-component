import { HPHToggle } from "./Toggle";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id:"Toggle",
  label: "label",
  disabled: false,
 
};

describe('Test', () => {
    test("Toggle id", () => {
        render(<HPHToggle />);
    });

    test("Toggle id", () => {
        render(<HPHToggle id={Props.id} />);
        const container = document.querySelector('#Toggle');
        expect(container).toBeTruthy();
    });
    test("Toggle label", () => {
        render(<HPHToggle id={Props.id} label={Props.label}/>);
        const container = document.querySelector('#Toggle');
        expect(container).toBeTruthy();
    });
    test("Toggle is not disable", () => {
        render(<HPHToggle id={Props.id} label={Props.label} disabled={Props.disabled}/>);
        const container = document.querySelector('#Toggle');
        expect(container).toBeTruthy();
    });

});