import React from 'react';
import { Tooltips } from '../Tooltips/Tooltips';
import { StyledDiv, StyledLabel, StyledInputSwitch, StyledBox } from '../styled/toggle.styled';

export interface ToggleProps{
  label?:string | 'Toggle',
  id?:string | 'Toggle', 
  disabled?: boolean,
  onChange?: (e: any) => void,
  checked?: boolean,
  size?: string | 'small' | 'medium' | 'large',
  tooltipDisabled?:boolean | false,
  toolTipText?: string,
  toolTipPlacement?: | 'top' | 'right' | 'left' | 'bottom',
  toolTipArrow?: boolean | false,
  [x:string]: any;
}


const HPHToggle: React.FC<ToggleProps> = ({
  label = 'Toggle',
  id = 'Toggle',
  disabled = false,
  onChange,
  checked = false,
  tooltipDisabled = false,
  toolTipPlacement = 'bottom',
  toolTipText = 'Toggle',
  size = 'medium',
  ...rest
   
}): any => {
         
  return (
        < >
        <StyledDiv>
            <StyledLabel disabled={disabled}>{label}</StyledLabel>
            <StyledBox>
            <StyledInputSwitch 
              id={id}
              disabled={disabled}
              checked={checked} 
              onChange = {onChange}
              >
              </StyledInputSwitch>
                {!tooltipDisabled && <Tooltips
                size={size}
                toolTiptext={toolTipText}
                toolTipPlacement= {toolTipPlacement}
                toolTipArrow={false}
                {...rest}
            >
            </Tooltips>
          } 
            </StyledBox>
        </StyledDiv>  
        </>
  );
};
export { HPHToggle } ;