import * as React from 'react';
import { StyledBox, StyledDiv, StyledChildren } from '../styled/OverflowingDialog.styled';

export interface DialogProps{
  boxPosition?: string | 'right' | 'left' | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight',
  width?: number | string,
  height?: number | string,
  paddingTop?: number | string,
  paddingBottom?: number | string,
  paddingLeft?: number | string,
  paddingRight?: number | string,
  children?: any,
  id?: string | 'box',
  [x:string]: any;
}
const DialogBox: React.FC<DialogProps> = ({
  width = '150px',
  height = '150px',
  paddingTop = '20px',
  paddingBottom = '20px',
  paddingLeft = '20px',
  paddingRight = '20px',
  boxPosition = 'right',
  children = 'Test',
  id = 'box',
  ...rest
}): any => {  
  return (
    <StyledBox>
      <StyledDiv 
       id={id} 
       boxPosition={boxPosition}
       width={width} 
       height={height} 
       paddingTop={paddingTop} 
       paddingBottom={paddingBottom} 
       paddingLeft={paddingLeft} 
       paddingRight={paddingRight}
       {...rest}>
      {(typeof children === 'string' ?  <StyledChildren dangerouslySetInnerHTML={{ __html: children }} />  :
        <StyledChildren>{ children }</StyledChildren>)}
      </StyledDiv>
    </StyledBox>
  );
};
export { DialogBox } ;