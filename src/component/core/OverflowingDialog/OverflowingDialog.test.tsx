
import { DialogBox } from "./OverflowingDialog";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id:"box",
  width: '100px',
  height: '100px',
  paddingTop: '20px',
  paddingBottom : '20px',
  paddingLeft : '20px',
  paddingRight : '20px',
  boxPosition : 'right',
};

describe('Test', () => {
    test("Dialog box id", () => {
        render(<DialogBox />);
    });

    test("Dialog box id", () => {
        render(<DialogBox id={Props.id} />);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });
    test("Dialog box width", () => {
        render(<DialogBox width={Props.width}/>);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });
    test("Dialog box height", () => {
        render(<DialogBox height={Props.height} />);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });

    test("Dialog box Padding Top", () => {
        render(<DialogBox paddingTop={Props.paddingTop}/>);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });

    test("Dialog box Padding Bottom", () => {
        render(<DialogBox paddingBottom={Props.paddingBottom}/>);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });
    test("Dialog box Padding Left ", () => {
        render(<DialogBox paddingLeft={Props.paddingLeft}/>);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });
    test("Dialog box Padding Right", () => {
        render(<DialogBox paddingRight={Props.paddingRight}/>);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });
    test("Dialog box Box Position", () => {
        render(<DialogBox boxPosition={Props.boxPosition}/>);
        const container = document.querySelector('#box');
        expect(container).toBeTruthy();
    });
});
