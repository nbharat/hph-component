
import { NotificationBar } from "./NotificationBar";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id:'bar',
  width: '150px',
  height: '150px',
  text: 'Lorem ipsum dolor sit amet consectetur!',
  background: 'Alert',
  disableCross: false,
  disableAlert: false,
};

describe('Test', () => {
    test("Notification Bar", () => {
        render(<NotificationBar/>);
    });

    test("Notification Bar id", () => {
        render(<NotificationBar id={Props.id} />);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Notification Bar width", () => {
        render(<NotificationBar width={Props.width} />);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Notification Bar height", () => {
        render(<NotificationBar height={Props.height}/>);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Notification Bar text", () => {
        render(<NotificationBar text={Props.text}/>);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Notification Bar background color", () => {
        render(<NotificationBar background={Props.background} />);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Cross icon of Notification Bar is not disable", () => {
        render(<NotificationBar disableCross={Props.disableCross}/>);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Cross icon of Notification Bar is disable", () => {
        render(<NotificationBar disableCross={true}/>);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Alret icon of Notification Bar is disable", () => {
        render(<NotificationBar disableAlert={Props.disableAlert} />);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });

    test("Alret icon of Notification Bar is not disable", () => {
        render(<NotificationBar disableAlert={true} />);
        const container = document.querySelector('#bar');
        expect(container).toBeTruthy();
    });
});
