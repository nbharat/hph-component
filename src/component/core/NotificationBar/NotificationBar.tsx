import React from 'react';
import { StyledDiv, StyledIcon, StyledChildren, StyledButton, StyledTime, StyledBox, StyledCross } from '../styled/NotificationBar.styled';

export interface BarProps{
  background?: string | 'Alert Red' | 'Warning Sunset Orange' | 'Success Aqua Green',
  width?: number | string,
  height?: number | string,
  text?: string | 'Test',
  id?: string | 'box',
  disable?: boolean,
  onClick?: any,
  Time?: number,
  timer?: number,
  [x:string]: any;
}

const NotificationBar: React.FC<BarProps> = ({
  width = '150px',
  height = '150px',
  text = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nihil rem!',
  background = 'Alert Red',
  id = 'bar',
  disable = false,
  onClick,
  Time = '',
  rest,
}): any => {
  return (
    <>
      <StyledDiv id={id} width={width} height={height} background={background} {...rest}>
        
      <StyledIcon/>

      <StyledChildren> {text} </StyledChildren>

      <StyledBox>
      {disable ? '' : <StyledTime>{Time + 's'}</StyledTime>}
      <StyledButton onClick={onClick}>
      <StyledCross/>
      </StyledButton>
      </StyledBox>

      </StyledDiv>
    </>    
  );
};
export { NotificationBar } ;