import React from 'react';
import { StyledBadge } from '../styled/badge.styled';

export interface BadgeProps {
  element?: any,
  value?: string,
  color?:string | 'Alert-Red' | 'Sky-Blue' | 'Sea-Blue' | 'Horizon-Blue' | 'Aqua-Green' | 'Sunray-Yellow' | 'Sunset-Orange',
  className?: string | 'p-badge',
  size?:string | 'Badge-small' | 'Badge-medium' | 'Badge-large' | 'smallBadge' | 'badgeStyle' | 'largeBadge',
  [x:string]: any;
}

const HPHBadge: React.FC<BadgeProps> = ({
  element,
  value = '1',
  className,
  color = 'Alert-Red',
  size = 'Badge-medium',
  ...rest
}) : any => {
  return (
    <>
    <StyledBadge
      value={value}    
      size={size}
      color={color}
      ref={element}
      {...rest}
    />
    </>
  );
};
export { HPHBadge };