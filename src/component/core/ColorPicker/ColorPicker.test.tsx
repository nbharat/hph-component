
import { ColorPicker } from "./ColorPicker";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id : 'colorPicker',
  label : 'Label',
  disabled : false,
  helpIcon : false,
  toolTipText : 'Helper Message',
};

describe('Test', () => {
    test("color Picker id", () => {
        render(<ColorPicker />);
    });

    test("color Picker id", () => {
        render(<ColorPicker id={Props.id} />);
        const container = document.querySelector('#colorPicker');
        expect(container).toBeNull();
    });
    test("color Picker label", () => {
        render(<ColorPicker label={Props.label}/>);
        const container = document.querySelector('#colorPicker');
        expect(container).toBeNull();
    });
    test("color Picker is not disable", () => {
        render(<ColorPicker disabled={Props.disabled}/>);
        const container = document.querySelector('#colorPicker');
        expect(container).toBeNull();
    });

    test("color Picker is disable", () => {
        render(<ColorPicker disabled={true} />);
        const container = document.querySelector('#colorPicker');
        expect(container).toBeNull();
    });
    test("color Picker has helpIcon", () => {
        render(<ColorPicker helpIcon={true} />);
        const container = document.querySelector('#colorPicker');
        expect(container).toBeNull();
    });
    test("color Picker has no helpIcon", () => {
        render(<ColorPicker helpIcon={Props.helpIcon} />);
        const container = document.querySelector('#colorPicker');
        expect(container).toBeNull();
    });
    test("color Picker toolTipText", () => {
        render(<ColorPicker toolTipText={Props.toolTipText} />);
        const container = document.querySelector('#colorPicker');
        expect(container).toBeNull();
    });
});