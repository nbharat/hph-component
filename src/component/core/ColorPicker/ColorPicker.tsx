import React from 'react';
import { IconButton } from '../IconButtons/IconButtons';
import { StyledInputWrapper, StyledInputLabel, StyledRequired, StyledColorPickerWrapper, StyledColorPicker } from './../styled/inputField.styled';


export interface ColorPickerProps {
  element?: any,
  label?: string,
  helpIcon?:boolean | false,
  required?:boolean | false,
  disabled?:boolean | false,
  onChange?:any;
  [x:string]: any;
}



const ColorPicker: React.FC<ColorPickerProps> = ({
  element,
  className,
  id = 'colorPicker',
  label = 'Label',
  value,
  disabled = false,
  helpIcon = false,
  required = false,
  toolTipText = 'Helper Message',
  onChange,
  ...rest
}) : any => {
  // const [color1, setColor] = useState<any>('1976D2');
  return (
   <>   
      <StyledInputWrapper id={id}>
          <StyledInputLabel>
            {label} {required ? <StyledRequired>*</StyledRequired> : ''}
            {helpIcon ? <IconButton fileName={'Icon-help'} size={'small'}
              toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
              toolTipText={toolTipText} {...rest} /> : ''}
          </StyledInputLabel>
          <StyledColorPickerWrapper>
              <StyledColorPicker
                type='color'
                disabled={disabled}           
                onChange={onChange} 
                value={value}
                // value={color1} onChange={(e) => setColor(e.target.value)}
              />
          </StyledColorPickerWrapper>
          
      </StyledInputWrapper>
   </>
  );
  
};
export { ColorPicker };