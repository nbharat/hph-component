
import { DialogWithOutModal } from './ModelessDialog';
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom';

const Props = {
  id : 'WithOutModal',
  label : 'label',
  subTitle : 'Title',
  modalHeading : 'modal heading',
  overlay : true,
  resizable : true,
  draggable : true,
  };

describe('Test', () => {
    test("Dialog with out modal test", () => {
        render(<DialogWithOutModal />);
    });
    test("Dialog with out modal id test", () => {
        render(<DialogWithOutModal id={Props.id} />);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
    test("Dialog with out modal label test", () => {
        render(<DialogWithOutModal id={Props.id} label={Props.label}/>);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
    test("Dialog with out modal subTitle test", () => {
        render(<DialogWithOutModal id={Props.id} subTitle={Props.subTitle} />);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
    test("Dialog with out modal modalHeading test", () => {
        render(<DialogWithOutModal id={Props.id} modalHeading={Props.modalHeading} />);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
    test("Dialog with out modal positions test", () => {
        render(<DialogWithOutModal id={Props.id} positions={'bottom'} />);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
    test("Dialog with out modal overlay test", () => {
        render(<DialogWithOutModal id={Props.id} overlay={Props.overlay} />);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
    test("Dialog with out modal resizable test", () => {
        render(<DialogWithOutModal id={Props.id} resizable={Props.resizable} />);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
    test("Dialog with out modal draggable test", () => {
        render(<DialogWithOutModal id={Props.id} draggable={Props.draggable} />);

        const container = document.querySelector('#DialogWithOutModal');
        expect(container).toBeNull();
    });
});