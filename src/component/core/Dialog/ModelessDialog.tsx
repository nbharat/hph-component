import React from 'react';
import { HPHButton } from '../Button/Button';
import { StyledFooter, StyleHeader, StyledSubTitle, StyledModalHeading, StyledDiv, StyledDialog, StyledCross } from '../styled/Dialog.styled';

type DialogPositionType = 'center' | 'top' | 'bottom' | 'left' | 'right' | 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right';

export interface DialogWithOutModalProps{
  id?: string,
  label?: string,
  subTitle?: string,
  modalHeading?: string,
  text?: string,
  overlay?: boolean,
  resizable?: boolean,
  draggable?: boolean,
  disabledButton?: boolean,
  positions?: DialogPositionType,
  onFooterButtonClick?: any,
  visible?: any,
  OnHide?: any,
  onClick?: any,
  [x:string]: any;
}

const DialogWithOutModal: React.FC<DialogWithOutModalProps> = ({
  id = 'DialogWithOutModal',
  label = 'Modal With Overlay',
  subTitle = 'SubTitle',
  modalHeading = 'Title',
  positions = 'bottom',
  disabledButton = true,
  overlay = false,
  resizable = false,
  draggable = false,
  onFooterButtonClick,
  visible,
  onClick,
  OnHide,
  text = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laborum quibusdam alias dicta quisquam excepturi mollitia quas voluptatem eum quam temporibus.',
  ...rest
}): any => {

  const renderFooter = () => {
    return (
      <>
      {!disabledButton ? 
        <StyledFooter>
          <HPHButton label='Cancel' size='Small' theme='Secondary' disableBadge={true} onClick={onFooterButtonClick}/>
          <HPHButton label='save' size='Small' theme='Primary' disableBadge={true} onClick={onFooterButtonClick}/>
        </StyledFooter> : ''
    } 
    </>
    );
  };

  const renderHeader = () => {
    return (
        <StyleHeader id={id}>
            <StyledSubTitle>{subTitle}</StyledSubTitle>
            <StyledModalHeading>{modalHeading}</StyledModalHeading>
            {disabledButton ? <StyledCross onClick={onClick}/> : ''}
        </StyleHeader>
    );
  };

  return (
      <StyledDiv id={id} {...rest}>
          <HPHButton label={label} size='Small' theme='Primary' disableBadge={true} onClick={onClick}/>
            <StyledDialog 
            header={renderHeader} 
            position={positions} 
            visible={visible} 
            footer={renderFooter} 
            modal={overlay}
            draggable={draggable}
            resizable={resizable}
            onHide={OnHide}>
              <p>{text}</p>
            </StyledDialog>
      </StyledDiv>
  );
};

export { DialogWithOutModal };