import { DialogModal } from './ModalDialog'
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom';

const Props = {
    id : 'WithOutModal',
    label : 'label',
    subTitle : 'Title',
    modalHeading : 'modal heading',
    overlay : true,
    resizable : true,
    draggable : true,
};  

describe('Test', () => {
    test("Dialog test", () => {
        render(<DialogModal />);
    });
    test("Dialog Modal id test", () => {
        render(<DialogModal id={Props.id} />);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
    test("Dialog Modal label test", () => {
        render(<DialogModal id={Props.id} label={Props.label}/>);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
    test("Dialog Modal subTitle test", () => {
        render(<DialogModal id={Props.id} subTitle={Props.subTitle} />);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
    test("Dialog Modal modalHeading test", () => {
        render(<DialogModal id={Props.id} modalHeading={Props.modalHeading} />);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
    test("Dialog Modal positions test", () => {
        render(<DialogModal id={Props.id} positions={'bottom'} />);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
    test("Dialog Modal overlay test", () => {
        render(<DialogModal id={Props.id} overlay={Props.overlay} />);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
    test("Dialog Modal resizable test", () => {
        render(<DialogModal id={Props.id} resizable={Props.resizable} />);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
    test("Dialog Modal draggable test", () => {
        render(<DialogModal id={Props.id} draggable={Props.draggable} />);

        const container = document.querySelector('#DialogModal');
        expect(container).toBeNull();
    });
});