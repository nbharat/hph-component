
import { NavigationMenu } from "./NavigationMenu";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id:'navigation',
  width: '25px',
  height: '25px',
};

describe('Test', () => {
    test("Navigtion Menu", () => {
        render(<NavigationMenu element={React.createRef()}/>);
    });

    test("Navigtion Menu icon id", () => {
        render(<NavigationMenu element={React.createRef()} id={Props.id} />);
        const container = document.querySelector('#navigation');
        expect(container).toBeTruthy();
    });
    test("Navigtion Menu icon width", () => {
        render(<NavigationMenu element={React.createRef()} id={Props.id} width={Props.width}/>);
        const container = document.querySelector('#navigation');
        expect(container).toBeTruthy();
    });
    test("Navigation Menu icon height", () => {
        render(<NavigationMenu element={React.createRef()} id={Props.id} height={Props.height} />);
        const container = document.querySelector('#navigation');
        expect(container).toBeTruthy();
    });

});
