/* eslint-disable import/extensions */
import React from 'react';
import { SVGIcon } from '../SVGIcon/SVGIcon';
import { getImgUrl } from '../../../common/getImgUrl';
import { StyledIcon, StyledDiv, StyledTieredMenu } from './../styled/navigationMenu.styled';

type TieredMenuAppendToType = 'self' | HTMLElement | undefined | null;

export interface NavigationProps {
  id?: string | 'navigation';
  element?: any,
  items?: any[],
  breakLine?: boolean,
  popup?: boolean;
  style?: object;
  className?: string;
  autoZIndex?: boolean;
  baseZIndex?: number;
  appendTo?: TieredMenuAppendToType;
  transitionOptions?: object;
  [x:string]: any;
}

const NavigationMenu: React.FC<NavigationProps> = ({
  id = 'navigation',
  breakLine = false,
  popup = false,
  style,
  className = '',
  autoZIndex = false,
  baseZIndex = 0,
  appendTo = 'self',
  transitionOptions,
  element,
  items,
  ...rest
}): any => {
  
  return (
      <>
        <StyledDiv>
            <StyledIcon>
                <SVGIcon 
                id={id}
                    src={getImgUrl('Icon-menu')} 
                    width='25px'
                    height='25px'
                />
                <StyledTieredMenu 
                model={items} 
                breakLine={breakLine} 
                popup={popup} 
                style={{ style }} 
                className={className} 
                autoZIndex={autoZIndex} 
                baseZIndex={baseZIndex}
                appendTo={appendTo}
                transitionOptions={transitionOptions}
                ref={element} 
                {...rest} />    
            </StyledIcon>  
        </StyledDiv>
           
      </>
      
  );
   
};

export { NavigationMenu };