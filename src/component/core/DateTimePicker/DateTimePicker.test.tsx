import { DateTimePicker } from "./DateTimePicker";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
  id:"Datetime24",
  label: "label",
  helpIcon: true,
  width: "200px", 
  className: "hph-Slider",
  disabled:false,
  required:true,
  toolTipText: "Date time format dd/mm/yyyy hh:mm",
  errorMessage: "errorMsg",

};
describe('CalendarDateTime', () => {
    test("Calendar Date Time", () => {
        render(<DateTimePicker />);
    });

    test("Calendar Date Time", () => {
        render(<DateTimePicker id={Props.id} />);
        const container = document.querySelector('#Datetime24');              
        expect(container).toBeTruthy();
    });

    test("Calendar Date width", () => {
      render(<DateTimePicker id={Props.id} width={Props.width} />);
      const container = document.querySelector('#Datetime24');              
      expect(container).toBeTruthy();
    });

    test("Calendar Date helpIcon true", () => {
      render(<DateTimePicker id={Props.id} width={Props.width} helpIcon={Props.helpIcon} />);
      const container = document.querySelector('#Datetime24');              
      expect(container).toBeTruthy();
    });

    test("Calendar Date required true", () => {
      render(<DateTimePicker id={Props.id} width={Props.width} helpIcon={Props.helpIcon} required={Props.required} />);
      const container = document.querySelector('#Datetime24');              
      expect(container).toBeTruthy();
    });

    test("Calendar Date errorMessage", () => {
      render(<DateTimePicker id={Props.id} width={Props.width} errorMessage={Props.errorMessage} required={Props.required} />);
      const container = document.querySelector('#Datetime24');              
      expect(container).toBeTruthy();
    });

    test("Calendar Date errorMessage Blank", () => {
      render(<DateTimePicker id={Props.id} width={Props.width} errorMessage={''} />);
      const container = document.querySelector('#Datetime24');              
      expect(container).toBeTruthy();
    });

   
});
