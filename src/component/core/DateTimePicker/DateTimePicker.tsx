/* eslint-disable import/extensions */
import React from 'react';
import { IconButton } from '../IconButtons/IconButtons';
import { StyledInputWrapper, StyledInputLabel, StyledRequired, StyledCalendar, StyledInputErrorMsg, DateTimePickerGlobalStyles } from '../styled/dateTimePicker.styled';
import { HPHRed } from '../Colors';

interface Props {
  [x:string]: any;
}

const DateTimePicker: React.FC<Props> = ({
  id = 'DateTimePicker',
  label = 'Label',
  disabled = false,
  required = false,
  helpIcon = false,
  errorMessage = '',
  toolTipText = 'Date time format dd/mm/yyyy hh:mm',
  date,
  placeholder = 'dd/mm/yyyy hh:mm',
  dateFormat = 'dd/mm/yy',
  timeFormat = 'hh:mm',
  width = '200px',  
  onChange,
  ...rest
}): any => <StyledInputWrapper>
  <DateTimePickerGlobalStyles />
  <StyledInputLabel>
    {label} {required ? <StyledRequired>*</StyledRequired> : ''}
    {helpIcon && <IconButton fileName={'Icon-help'} size={'small'} 
      toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
      toolTipText={toolTipText}
      {...rest}
    />} 
  </StyledInputLabel>   
  <StyledCalendar                      
    disabled={disabled}
    id={id}
    value={date}  
    showTime    
    showIcon  
    placeholder={placeholder}
    dateFormat={dateFormat}    
    timeFormat={timeFormat}
    onChange={(e) => onChange && onChange(e.value)}
    style={{ width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }} 
    {...rest}    
  />                      
<StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
</StyledInputWrapper>;

export { DateTimePicker };