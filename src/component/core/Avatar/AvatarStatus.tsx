import React from 'react';
import { StyledAvatarStatus } from './../styled/avatarStatus.styled';

export interface AvatarStatusProps {
  status?: string | 'away' | 'online' | 'busy',
  className?: string | 'p-badge',
  size?: string | 'xSmall' | 'small' | 'medium' | 'large' | 'xLarge' | 'xxLarge',
  [x:string]: any;
}

const HPHAvatarStatus: React.FC<AvatarStatusProps> = ({
  status = 'online',
  size = 'small',
  className,
  ...rest
}) : any => {
  return (
          <StyledAvatarStatus
          size = {size}
          status ={status}
          {...rest}
          />
          
  );
};
export  { HPHAvatarStatus } ;
