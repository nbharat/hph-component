import React from 'react';
import { HPHAvatarStatus } from './AvatarStatus';
import { StyledButton, StyledDiv, StyledAvatar } from './../styled/avatar.styled';

export interface AvatarProps {
  name?: string | 'Kitty chan',
  size?: string | 'xSmall' | 'small' | 'medium' | 'large' | 'xLarge' | 'xxLarge',
  backgroundColor?: string | '#002E6D',
  className?: string | 'p-avatar',
  status?: string | ' away' | 'online' | 'busy',
  showStatus?: boolean | true,
  imageUrl? : string | '',
  [x:string]: any;
}

const FirstName = (value: string) => {
  let valueReturn = '';
  let valueArray: any = value.split(' ', 2);
  if  ( valueArray != undefined && valueArray.length >= 1 ) {
    for (let i = 0; i < valueArray.length ; i++) {

      valueReturn = valueReturn + valueArray[ i ].substring(0, 1);
    }
  } else {
    valueReturn = valueArray.substring(0, 1);
  }
  return valueReturn;
};

const image = (props) => `${props.imageUrl}`;

const HPHAvatar: React.FC<AvatarProps> = ({
  name = 'kitty chan',
  className,
  backgroundColor = '#002E6D',
  size = 'medium',
  status = 'away',
  imageUrl = '',
  showStatus = true,
  ...rest
}) : any => {
  return (
    <>
    <StyledButton >
    <StyledDiv>
        <StyledAvatar
        size = {size}
        { ...(Array.isArray(imageUrl) && imageUrl.length) || (typeof image === 'string' && image !== '') ? { image: imageUrl  } : { label: FirstName(name), style: { backgroundColor } } }
        >
        </StyledAvatar>
         {showStatus ? 
         <HPHAvatarStatus
         size = {size}
         status ={status}
         {...rest}
         /> : ' ' }
        </StyledDiv>
    </StyledButton>


    </>
  );
};
export  { HPHAvatar } ;