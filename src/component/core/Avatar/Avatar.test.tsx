import { HPHAvatar } from "./Avatar";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
    size: "medium",
    Status: "busy",
    label: 'Bharat',

};

describe('Test', () => {

    test("medium", () => {
        render(<HPHAvatar />);
    });

    test("medium", () => {
        render(<HPHAvatar imageUrl="test.jpg" />);
    });

    test("medium", () => {
        render(<HPHAvatar showStatus={false} />);
    });

    test("medium", () => {
        render(<HPHAvatar name={undefined} />);
    });
    
    test("HSHAvatarStatus is not disabled", () => {
        render(<HPHAvatar disabled={false}/>);

        const container = document.querySelector('.p-avatar');
        expect(container).toBeTruthy();
    });
    test("HSHAvatarStatus is disabled", () => {
        render(<HPHAvatar disabled = {true}/>);

        const container = document.querySelector('.p-avatar');
        expect(container).toBeTruthy();
    });
    test("HSHAvatar label", () => {
        render(<HPHAvatar label={Props.label}/>);

        const container = document.querySelector('.p-avatar');
        expect(container).toBeTruthy();
    });
});

