
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'
import { HPHTable } from './Table';

const Props = {
    id : 'table',
    selectionMode : false,
    rows : '3',
    showPaginator : false,
  };

describe('Test', () => {
    test("Table id", () => {
        render(<HPHTable id={Props.id}/>);
    });
    test("Table selection mode", () => {
        render(<HPHTable selectionMode = {Props.selectionMode}/>);
    });
    test("Table row", () => {
        render(<HPHTable rows = {Props.rows}/>);
    });
    test("Table show Paginator", () => {
        render(<HPHTable showPaginator = {Props.showPaginator}/>);
    });
});
