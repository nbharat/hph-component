import React, { Component } from 'react';
import { Table, TableGlobalStyles, StyledTable, StyledMultiselect, StyledMultiselectBox, StyledDiv, StyledFooter, StyledRefresh, StyledButton, StyledSpan, StyledText, StyledInput, StyledPaginationButton, StyledTemplate, StyledDrag, StyledItem, DateTimePickerGlobalStyles, StyledTag } from '../styled/table.styled';
import { IconButton } from '../IconButtons/IconButtons';
import { HPHButton } from '../Button/Button';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';
import { SVGIcon } from '../SVGIcon/SVGIcon';
import { Tags } from '../Tags/Tags';
import { Dropdown } from 'primereact/dropdown';
import { Calendar } from 'primereact/calendar';
import { InputMask } from 'primereact/inputmask';
import moment from 'moment';
import { ColorPicker } from '../ColorPicker/ColorPicker';

export interface TableProps{
  id?: string | 'table',
  scrollHeight?: number | string,
  data?: any[],
  columns?:any[],
  editable?: boolean | false,
  onClick?: any,
  editMode?:'row' | 'cell',
  selection?: any,
  selectionMode?: boolean,
  width?: number,
  order?: number,
  colorPickerDisabled?:boolean,
  [x:string]: any,
}

class HPHTable extends Component<TableProps, {}, any> {
  state = {
    tagLabel: [''],
    tagDisabled:true,
    columns: this.props.columns || [],
    data:this.props.data || [],
    selectedColumns: this.props.columns || [],
    optionColumns: this.props.columns || [],
    selectedProduct:null,
    currentPage : '1',
    First: '0',
  };

  onCellEditComplete = (e) => {
    let { rowData, newValue, field, originalEvent: event } = e;
    let y = this.state.columns.filter(item => item.field === field)[0];
    if (y.editorType === 'text'){
      if (newValue.trim().length > 0)
        rowData[field] = newValue;
      else
        event.preventDefault();   
    } else {
      rowData[field] = newValue;
    }
  };

  onRowEditComplete = (e:any) => {
    let proDuct =  this.state.data;
    let { newData, index } = e;
    let i = parseInt(index, 10);
    proDuct[i] = newData;
    this.state.data[i] = proDuct[i];
  };
  
  textEditor = (option) => {
    return <InputText 
    type="text" 
    value={option.value} 
    onChange={(e) => option.editorCallback(e.target.value)} />;
  };

  dropdownList = (op)=> {
    let i = op.field;
    if (this.state.data.length > 0) {
      let options:any = [];      
      this.state.data.filter((item1:any)=>{
        options.push({ 
          label: item1[i],
          value: item1[i],
        });
      });
      return options;
    }
  };

  dropdownEditor = (option) => {
    return <Dropdown value={option.value} options={this.dropdownList(option)} optionLabel="label" optionValue="value"
                onChange={(e) => option.editorCallback(e.value)}/>; 
  };

  dateTimeEditor = (opt) => {
    return <Calendar value={opt.value} onChange={(e) => opt.editorCallback(moment(this.formatDate(e.value), 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'))}  dateFormat="dd/mm/yy" placeholder="dd/mm/yyyy hh:mm" showTime readOnlyInput />;
  };

  tableEditor = (option) => {
    let y = this.state.columns.filter(item => item.field === option.field)[0];
    if (y.editorType === 'text'){
      return this.textEditor(option);
    } else if (y.editorType === 'select' && this.props.editMode === 'row'){
      return this.dropdownEditor(option);
    } else if (y.editorType === 'select' && this.props.editMode === 'cell'){
      return this.textEditor(option);
    } else if (y.editorType === 'dateTime'){
      return this.dateTimeEditor(option);
    } else {
      return this.textEditor(option);
    }
  };

  dateFormat = (value) =>{
    var date = new Date(value); 
    let d = date.toLocaleDateString('en-GB', {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
    });
    return d;
  };

  timeFormat = (value) =>{
    var date = new Date(value); 
    let t = date.toLocaleTimeString('en-GB', {  hour: '2-digit', minute: '2-digit' });
    return t;
  };

  formatDate = (value) => {    
    return this.dateFormat(value) + ' ' + this.timeFormat(value);
  };

  dateBodyTemplate = (rowData) => {
    // return this.formatDate(rowData.dateTime);
    return moment(rowData.dateTime).format('DD/MM/YYYY HH:mm');
  };

  colorBodyTemplate = (rowdata) => {
    return <ColorPicker label='' value={rowdata.color} disabled={this.props.colorPickerDisabled}/>;
  };

  deleteBodyTemplate = () => {
    return <IconButton fileName="Icon-trash" size="medium" toolTipArrow={false} toolTipPlacement="right" toolTiptext={'Delete'} style={{ padding:0 }} onClick={this.onDelete}/>;
  };

  dateFilterTemplate(options) {
    return <InputMask mask="9999-99-99 99:99" value={options.value} placeholder="yyyy-mm-dd hh:mm" slotChar="yyyy-mm-dd hh:mm" onChange={(e) => options.filterCallback(e.value)}></InputMask>;
  }

  itemTemplate = (item:any) => {
    return (
      <StyledMultiselectBox onDragOver={(ev: any) => this.onDragOver(ev)} 
      onDragStart = {(e: any) => this.onDragStart(e, item.id)}
      onDrop={(e: any) => this.onDrop(e)}
      id={item.id}
      >
      <StyledTemplate draggable= 'true'>
        <StyledDrag ><SVGIcon fileName="Icon-drag"/></StyledDrag>
        <StyledItem>{item.header}</StyledItem>
      </StyledTemplate>
      </StyledMultiselectBox>
    );
  };

  onReset = () =>{
    let pre = this.state.columns;
    this.setState({ selectedColumns: pre.filter((item)=> item.order = item.id) }); 
    this.setState({ optionColumns : pre.filter((item)=> item.order = item.id) }); 
  };

  footerTemplate = (
    <StyledFooter>
    <StyledRefresh><IconButton fileName="Icon-reset" size="medium" toolTipArrow={false} toolTipPlacement="bottom" toolTiptext={'Reset'} onClick={this.onReset}style={{ padding:0 }}/></StyledRefresh>
    <StyledButton><HPHButton label={'Save Current Setting'} size={'Small'} theme={'Secondary'} onClick={this.props.onClick}/></StyledButton>
    </StyledFooter>
  );
  
  onColumnToggle = (event:any) => {
    let selectedColumn = event.value;
    let orderedSelectedColumns = this.state.columns.filter((col:any) => selectedColumn.some((sCol:any) => sCol.field === col.field));
    this.setState({ selectedColumns:orderedSelectedColumns });
  };
  
  onDelete = (e: any) => {
    let newData = this.state.data;
    let remove = e.currentTarget.closest('tr');
    newData.splice( remove, 1);
    this.setState({ data : newData });
  };

  onDragOver = (ev: any) => {
    ev.preventDefault();
  };

  onDragStart = (ev: any, id: any) => {
    ev.dataTransfer.setData('id', id);
  };

  onDrop = (ev: any) => {
    let id = Number(ev.dataTransfer.getData('id'));

    const dragColumn: any = this.state.selectedColumns.find((column) => column.id === id);
    const dropColumn: any = this.state.selectedColumns.find((column) => column.id === Number(ev.currentTarget.id));

    const dragColumnOrder = dragColumn?.order;
    const dropColumnOrder = dropColumn?.order;

    const newColumnState = this.state.selectedColumns.map((column) => {
      if (column.id === id) {
        column.order = dropColumnOrder;
      }

      if (column.id === Number(ev.currentTarget.id)) {
        column.order = dragColumnOrder;
      }
      return column;
    });
    this.setState({ 
      selectedColumns: newColumnState,
      optionColumns: newColumnState,
    });
  };

  filterApply = (op) => {
    let { columns, tagLabel } = this.state;
    for (let index = 0; index < columns.length; index++) {
      let name = op.field;
      let text = '';
      if (columns[index].editorType === 'dateTime'){
        text = this.formatDate(op.constraints.constraints[0].value);
      } else {
        text = op.constraints.constraints[0].value;
      } 
      let newTag = name + ':' + text;
      let x = columns[index].field;
      if (op.field === x){
        for (let i = 0;i < tagLabel.length;i++){
          if (tagLabel[i].split(':')[0] == op.field){
            tagLabel.splice(i, 1);
          } 
        }
        tagLabel.push(newTag);
        break;
      }
    }
    let datas = this.state.data.filter((item1:any)=>{ 
      return (item1[op.field].trim().toLowerCase().lastIndexOf(op.constraints.constraints[0].value.trim().toLowerCase()) != -1 ? item1 : '');
    });
    this.setState({ data: datas });
  };

  filterClearTemplate = ()=>{
    let datas:any = [];
    this.state.tagLabel.map(item =>{
      if (item != ''){
        this.state.data.filter((item1)=>{
          let d:any = (item1[item.split(':')[0]].trim().toLowerCase().lastIndexOf(item.split(':')[1].trim().toLowerCase()) != -1 ? item1 : '');
          if (d != undefined && d != null && d != ''){
            datas.push(d);
          }
        });
      }
    });
    this.setState({ data: datas });
  };

  onCustomPage = (e:any) => {
    this.setState({ First :e.first });
    this.setState({ currentPage:e.page + 1 });
  };

  onPageInputChange = (event:any) => {
    this.setState({ currentPage: event.target.value });
  };

  onPageInputKeyDown = () => {
    const curPage = parseInt(this.state.currentPage.toString());
    if (curPage < 0 || curPage > this.state.data.length) {
    } else {
      const curfirst = this.state.currentPage ? this.props.rows * (curPage - 1) : 0;
      this.setState({ First: curfirst });
    }
  };

  paginatorTemplate = {
    layout:
    'PrevPageLink PageLinks NextPageLink CurrentPageReport',
    CurrentPageReport: () => {
      return (
        <StyledSpan>
            <StyledText>p.</StyledText>
            <StyledInput type='number'
            value={this.state.currentPage}
            onChange={this.onPageInputChange}/>
            <StyledPaginationButton onClick={this.onPageInputKeyDown}>
              Go
            </StyledPaginationButton>
        </StyledSpan>
      );
    },
  };

  render() {
    const {
      id = 'table',
      width = '100%',
      onClick,
      editable = 'false',
      editMode = 'cell',
      selectionMode = false,
      rows = '10',
      columns,
      showPaginator,
      order,
      ...rest
    } = this.props;

    const { optionColumns, data, selectedColumns, First } = this.state;

    const columnComponents = this.state.selectedColumns.sort((a, b) => a.order - b.order).map((col)=> {
      return <Column
          key={col.field} 
          field={col.field}
          header={col.header} 
          dataType={col.dataType} 
          sortable={col.sortable}
          filter={col.filter}
          showFilterMatchModes={col.showFilterMatchModes} 
          filterMatchMode= 'contains'
          filterPlaceholder={col.filterPlaceholder}
          onFilterApplyClick={this.filterApply}
          filterClear={this.filterClearTemplate}
          showAddButton={col.showAddButton} 
          showFilterMenuOptions={col.showFilterMenuOptions} 
          showClearButton={col.showClearButton}
          onCellEditComplete={this.onCellEditComplete}
          filterElement={col.dataType === 'date' && this.dateFilterTemplate }
          body= {col.dataType === 'date' && this.dateBodyTemplate || col.dataType === 'color' && this.colorBodyTemplate}
          editor={this.props.editable && col.editable ? (option) => this.tableEditor(option) : ''}
        />;
    });

    return (
    <Table width={width}>
    <DateTimePickerGlobalStyles/>
     <TableGlobalStyles />
     <StyledTag>
     {this.state.tagLabel.map(item =>{
       return (
         item != '' ? 
            <Tags label={item} rounded={true} remove={true} onRemove={this.filterClearTemplate} style={{ margin:'10px' }}></Tags>
           : '' 
       );
     })
    }
    </StyledTag>
     <StyledDiv>
     <StyledMultiselect value={selectedColumns} options={optionColumns} optionLabel="header" onChange={this.onColumnToggle} panelFooterTemplate={this.footerTemplate} itemTemplate={this.itemTemplate}/>
        <StyledTable 
           id={id}
           value={data}
           dataKey="id"
           scrollable
           removableSort
           responsiveLayout="scroll"
           filters={null}
           filterDisplay="menu"
           editMode={editMode}
           resizableColumns 
           columnResizeMode="fit"
           onRowEditComplete={this.onRowEditComplete}
           selectionMode='checkbox'
           paginator={showPaginator}
           paginatorTemplate={this.paginatorTemplate}
           rows={rows}
           first={First}
           paginatorLeft={'Total: ' + data.length + ' items'} 
           onPage={this.onCustomPage}
           selection={this.state.selectedProduct} 
           onSelectionChange={(e) => this.setState({ selectedProduct: e.value })}
           {...rest}
        >
          {selectionMode ? <Column selectionMode='multiple'/> : <Column style={{ display: 'none' }}/>}
          {columnComponents}
          <Column  body={this.deleteBodyTemplate}/>
          {editable ? <Column rowEditor ></Column> : <Column style={{ display: 'none' }}/>}
        </StyledTable>
      </StyledDiv>
    </Table>
    );
  }
}
export { HPHTable };