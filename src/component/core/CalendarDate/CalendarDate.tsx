import React from 'react';
import { IconButton } from '../IconButtons/IconButtons';
import { StyledInputWrapper, StyledInputLabel, StyledRequired, StyledCalendar,  StyledInputErrorMsg } from './../styled/calenderDate.styled';
import {
  HPHRed,
} from '../Colors';

interface Props {
  id?: string;
  date?: string ;
  element?: any;
  maxDate?: string;
  dateFormat?: string | 'dd/mm/yy';
  label?: string;
  disabled?:boolean | false,
  helpIcon?:boolean | false,
  required?:boolean | false,
  errorMessage?: string;
  toolTipText?: string;
  className?: string | 'p-badge';
  width?:string | '200px',
  [x:string]: any;
}


const CalendarDate: React.FC<Props> = ({
  
  id = 'mask',
  //placeholder = 'dd / MM / yyyy',
  className,
  label = 'Label',
  disabled = false,
  required = false,
  helpIcon = false,
  errorMessage = 'Error Message',
  toolTipText = 'Date format dd/mm/yyyy',
  date,  
  width = '200px',
  maxDate = '9999-12-31',
  ...rest
}): any => {
  return (
    <>
      <StyledInputWrapper>
        <StyledInputLabel>
            {label} {required ? <StyledRequired>*</StyledRequired> : ''}
            {helpIcon ? <IconButton fileName={'Icon-help'} size={'small'}  
              toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
              toolTipText={toolTipText} {...rest}
            /> : ''} 
        </StyledInputLabel>   
        <StyledCalendar         
          disabled={disabled}
          type = { 'date' }
          id={id}
          value={date}
          max={maxDate}
          onChange={(e) => (date = e.target.value)}  
          style={{ width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }}   
          {...rest} 
        />
        <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
      </StyledInputWrapper>
    </>
  );
};
export { CalendarDate };
