import React from 'react';
import { HPHBadge } from '../Badge/Badge';
import { SVGIcon } from '../SVGIcon/SVGIcon';
import { Tooltips } from '../Tooltips/Tooltips';
import{ StyledButton, sizeObj, sizeObjBadge } from './../styled/iconButtonWithBadge.styled';

export interface IconButtonWithBadgeProps {
  idIcom?: string | 'idIcon',
  size?: string | 'small' | 'medium' | 'large',
  fileName?: string | 'Icon-doc',
  classNameSVG?: string | 'iconEnabledOnHover iconStyle',
  classNameBadge?: string,
  value?: string | '1',
  color?: string | 'Alert-Red' | 'Sky-Blue' | 'Sea-Blue' | 'Horizon-Blue' | 'Aqua-Green' | 'Sunray-Yellow' | 'Sunset-Orange',
  idToolTip?: string | 'iconTooltipID',
  toolTipText?: string | 'Icon-doc',
  classNameTooltip?: string,
  toolTipPlacement?: | 'top' | 'right' | 'left' | 'bottom',
  toolTipArrow?: boolean | true | false,
  disabled?: boolean | false,
  enabledActive?: boolean | false,
  onMouseOver?: (e: any) => void
  onMouseOut?: (e: any) => void,
  onClick?: (e: any) => void,
  [x:string]: any;
}

const fileNameEnableDisable = (disabled: Boolean, enabledActive: Boolean) => {
  return (disabled && enabledActive) || disabled  ? '-disable' : enabledActive  ? '-enable' : '';
};
const IconButtonWithBadge: React.FC<IconButtonWithBadgeProps> = ({
  idIcom = 'idIcon',
  size = 'medium',
  fileName = 'Icon-doc',
  classNameSVG,
  classNameBadge,
  value = '2',
  color = 'Alert-Red',
  idToolTip,
  toolTipPlacement = 'bottom',
  toolTipArrow = true,
  disabled = false,
  enabledActive = false,
  onMouseOver,
  onMouseOut,
  onClick,
  ...rest
}) => {
  return (
        <>
            <StyledButton 
            type='button'
            disabled={disabled}
            disabledIcon={disabled}
            >
                <SVGIcon
                    onClick={onClick}
                    {...enabledActive ? { onMouseOver } : { onMouseOver, onMouseOut }}
                    id={idIcom}
                    fileName={fileName + fileNameEnableDisable(disabled, enabledActive)}
                    width={sizeObj[size]}
                    height={sizeObj[size]}
                    {...rest}
                />
                <HPHBadge value={value}
                    size={sizeObjBadge[size]}
                    color={color}
                    {...rest}
                />
                <Tooltips
                    toolTiptext={fileName}
                    id={idToolTip || 'iconTooltipID'}
                    size={size}
                    toolTipPlacement={toolTipPlacement}
                    toolTipArrow={toolTipArrow}
                    {...rest}
                />
            </StyledButton>
        </>
  );
};
export { IconButtonWithBadge };