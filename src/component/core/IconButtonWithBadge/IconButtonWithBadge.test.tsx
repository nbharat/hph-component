import { IconButtonWithBadge } from "./IconButtonWithBadge";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
    value: "2",
    size: "medium",
    fileName:"TestIcon-doc",
};
describe('Test', () => {
    test("iconEnabledOnHover", () => {
        render(<IconButtonWithBadge {...Props} />);
    });

    test("iconBadgeButton default size", () => {
        render(<IconButtonWithBadge />);
    });

    test("iconBadgeButton enabledActive", () => {
        render(<IconButtonWithBadge enabledActive={true} />);
    });

    test("iconBadgeButton classNameBadge", () => {
        render(<IconButtonWithBadge />);
    });

    test("iconBadgeButton classNameSVG iconEnabledOnFocus", () => {
        render(<IconButtonWithBadge />);
    });

    test("iconEnabledOnHover medium", () => {
        render(<IconButtonWithBadge {...Props} />);

        const container = document.querySelector('.iconStyle');
        if (container) {
            expect(container.className).toBe("medium");
        }
    });
    
});
