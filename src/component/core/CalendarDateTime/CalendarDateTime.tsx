import React from 'react';
import{ StyledInputWrapper, StyledInputLabel, StyledRequired, StyledCalendar, StyledInputErrorMsg} from './../styled/calenderDateTime.styled';
import { IconButton } from '../IconButtons/IconButtons';
import { HPHRed } from '../Colors';

interface Props {
  id?: string;
  date?:any ;
  element?: any;
  dateFormat?: string | 'dd/mm/yy';
  label?: string;
  helpIcon?: boolean | false;
  required?: boolean | false;
  disabled?: boolean | false;
  errorMessage?: string;
  toolTipText?: string;
  className?: string | 'p-badge';
  onChange?(e: any): void;
  width?:string | 'auto',  
  maxDate?:string;
  [x:string]: any;
}

const CalendarDateTime: React.FC<Props> = ({
  element,
  id = 'Datetime24',
  className = '',
  label = 'Label',
  disabled = false,
  required = false,
  helpIcon = false,
  errorMessage = 'Error Message',
  toolTipText = 'Date time format dd/mm/yyyy hh:mm',
  date,
  width = '200px',  
  maxDate = '9999-12-01T00:00',
  ...rest
}): any => {

  return (
    <>
      <StyledInputWrapper>
          <StyledInputLabel>
            {label} {required ? <StyledRequired>*</StyledRequired> : ''}
            {helpIcon ? <IconButton fileName={'Icon-help'} size={'small'} 
              toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
              toolTipText={toolTipText}
              {...rest}
            /> : ''} 
          </StyledInputLabel>   
          <StyledCalendar                      
            disabled={disabled}
            type={ 'datetime-local' }           
            ref={element}
            id={id}
            value={date}            
            max={maxDate}
            onChange={(e) => (date = new Date(e.target.value))}
            style={{ width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }} 
            {...rest}    
          />                      
        <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
      </StyledInputWrapper>
     
    </>
    
  );
 
};
export { CalendarDateTime };
