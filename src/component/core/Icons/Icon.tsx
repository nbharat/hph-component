import React from 'react';
import { IconWrapper, IconInner, IconBox, IconName } from '../styled/icon.styled';

let Images = [];
const ImportAll = (r: any) => {
  return r.keys().map(r);
};
Images = ImportAll(require.context('../../../assets/Icon/', false, /\.(svg)$/));

interface Props {
  alt: string,
}

const Icon: React.FC<Props> = ({  }) => {
  return (
        <>
            {             
                Images.map((image: any) => {                  
           
                  let tl:any = image.default.split('.')[0].split('/')[2];
                  console.log({ tl });
                  if (!tl.includes('disable') && !tl.includes('enable')) {
                    return <IconWrapper> <IconInner>
                      <IconBox><img key={image.key} title={tl} src={image.default} ></img> </IconBox>
                      <IconName>{tl.replace('Icon-', '').replace('__', '')} </IconName></IconInner>
                    </IconWrapper>;
                  }
                })
            }
        </>
  );
};
export { Icon };