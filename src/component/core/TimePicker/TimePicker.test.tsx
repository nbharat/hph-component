
import { TimePicker } from "./TimePicker";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id:"time",
  label: "label",
  helpIcon: true,
  width: "200px",
  disabled: false,
  errorMessage: "errorMsg",
};

describe('Test', () => {
    test("Calender time id", () => {
        render(<TimePicker />);
    });

    test("Calender time id", () => {
        render(<TimePicker id={Props.id} />);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });
    test("Calender time label", () => {
        render(<TimePicker id={Props.id} label={Props.label} />);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });
    test("Calender time help Icon", () => {
        render(<TimePicker id={Props.id} label={Props.label} helpIcon={Props.helpIcon} />);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });

    test("Calender time width", () => {
        render(<TimePicker id={Props.id} label={Props.label} helpIcon={Props.helpIcon} width={Props.width} />);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });
    test("Calender time is not disabled", () => {
        render(<TimePicker id={Props.id} label={Props.label} helpIcon={Props.helpIcon} width={Props.width} disabled={Props.disabled} />);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });
    test("Calender time is disabled", () => {
        render(<TimePicker id={Props.id} label={Props.label} helpIcon={Props.helpIcon} width={Props.width} disabled={true} />);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });
    test("Calender time has error message", () => {
        render(<TimePicker id={Props.id} label={Props.label} helpIcon={Props.helpIcon} width={Props.width} disabled={Props.disabled} errorMessage={Props.errorMessage}/>);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });
    test("Calender time has no error message", () => {
        render(<TimePicker id={Props.id} label={Props.label} helpIcon={Props.helpIcon} width={Props.width} disabled={Props.disabled} errorMessage={''}/>);
        const container = document.querySelector('#time');
        expect(container).toBeTruthy();
    });
});