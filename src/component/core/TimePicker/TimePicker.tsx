/* eslint-disable import/extensions */
import React from 'react';
import { StyledInputWrapper, StyledInputLabel, StyledRequired, StyledInputErrorMsg, StyledCalendar, TimePickerGlobalStyles } from '../styled/timePicker.styled';
import { IconButton } from '../IconButtons/IconButtons';
import { HPHRed } from '../Colors';

interface Props {
  [x:string]: any;
}

const TimePicker: React.FC<Props> = ({
  id = 'time',
  label = 'Label',
  disabled = false,
  required = false,
  helpIcon = false,
  errorMessage = '',
  toolTipText = 'Time hh:mm 24 hours format',
  date,  
  placeholder = 'hh:mm',
  timeFormat = 'hh:mm',
  width = '200px',
  onChange,
  ...rest
}): any => <StyledInputWrapper>
  <TimePickerGlobalStyles />
  <StyledInputLabel>
      {label} {required ? <StyledRequired>*</StyledRequired> : ''}
      {helpIcon ? <IconButton fileName={'Icon-help'} size={'small'} 
        toolTipPlacement={'right'} toolTipArrow={false} disabled={false}
        toolTipText={toolTipText}
        {...rest}
      /> : ''} 
  </StyledInputLabel>   
  <StyledCalendar
    disabled={disabled}
    id={id}
    value={date}
    onChange={(e) => onChange && onChange(e.value)}
    timeOnly
    showTime
    showIcon
    placeholder={placeholder}
    timeFormat={timeFormat}    
    icon="pi pi-clock"
    style={{ 'width':width, ...errorMessage !== '' ? { borderColor: HPHRed } : {} }}  
  />
  <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
</StyledInputWrapper>;

export { TimePicker };