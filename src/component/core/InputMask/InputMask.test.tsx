import { HPHInputMask } from "./InputMask";
import React from 'react'
import '@testing-library/jest-dom'
import {render, fireEvent} from '@testing-library/react'

const setup = () => {
    const mockCallBack = jest.fn(v=>v)
    const cvv = render(<HPHInputMask value={{com: "A02", vsl: "A02", voy: "A02"}} onDataChange={mockCallBack}/>)
    const comInput = cvv.getByTestId('com')
    const vslInput = cvv.getByTestId('vsl')
    const voyInput = cvv.getByTestId('voy')
    return {
        mockCallBack,
        comInput,
        vslInput,
        voyInput,
    }
}

test('onDataChange callback after input com', () => {
    const { mockCallBack, comInput} = setup()
    fireEvent.change(comInput, {target: {value: 'COMP'}})
    expect(mockCallBack).lastReturnedWith({"com": "COMP", "voy": "A02", "vsl": "A02"})
})

test('onDataChange callback after input vsl', () => {
    const { mockCallBack, vslInput} = setup()
    fireEvent.change(vslInput, {target: {value: 'VSL123'}})
    expect(mockCallBack).lastReturnedWith({"com": "A02", "vsl": "VSL123", "voy": "A02"})
})

test('onDataChange callback after input voy', () => {
    const { mockCallBack, voyInput} = setup()
    fireEvent.change(voyInput, {target: {value: 'VOY1234567'}})
    expect(mockCallBack).lastReturnedWith({"com": "A02", "vsl": "A02", "voy": "VOY1234567"})
})

test('auto focus next input (com to vsl)', () => {
    const { comInput, vslInput } = setup()
    fireEvent.change(comInput, {target: {value: 'COMP'}})
    expect(vslInput).toHaveFocus()
})

test('auto focus next input (vsl to voy)', () => {
    const { vslInput, voyInput} = setup()
    fireEvent.change(vslInput, {target: {value: 'VSL123'}})
    expect(voyInput).toHaveFocus()
})

test('default value', () => {
    const { comInput, vslInput, voyInput } = setup()
    expect(comInput).toHaveValue("A02")
    expect(vslInput).toHaveValue("A02")
    expect(voyInput).toHaveValue("A02")
})