import React, { ChangeEvent, useEffect, useState } from 'react';
import { OuterDiv, StyledInputLabel, StyledRequired, InputDiv, ComInput, VslInput, VoyInput, StyledInputErrorMsg } from '../styled/inputMask.styled';
import { HPHSkyBlue, HPHGreyDark } from '../Colors';
export interface ComVslVoyData {
  com?: string;    
  vsl?: string;
  voy?: string;
}
export interface ComVslVoyProps {
  id?: string | 'InputMask',
  value: ComVslVoyData
  onDataChange: (data: ComVslVoyData) => void
  errorMessage?: string
  backgroundMode?: boolean,
  label?: string,
  helpIcon?:boolean | false,
  required?:boolean | false,
  disabled?:boolean | false,
  width?:string,
}

export interface CommonProps {
  isValid: boolean
  disabled: boolean
  backgroundMode?: boolean
}

const HPHInputMask = ({
  id = 'InputMask',
  value = {},
  onDataChange,
  errorMessage = '',
  backgroundMode = false,
  label = 'Label',
  disabled = false,
  width = '300px',
  required = false,
}: ComVslVoyProps) => {
  const [, setData] = useState<ComVslVoyData>({});
  const [isValidationPass, setIsValidationPass] = useState<boolean>(true);

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    let hasValidation = event?.target.validity;
    const isValid = hasValidation ? event?.target.validity.valid : true;
    if (isValid) {
      const newData: ComVslVoyData = { ...value, [event.currentTarget.name]: event.currentTarget.value.toUpperCase() };
      setData(newData);
      onDataChange(newData);
      if (event.currentTarget.maxLength === event.currentTarget.value.length){
        const nextElement = event.currentTarget.nextElementSibling?.nextElementSibling;
        if (nextElement){
          if (nextElement instanceof HTMLInputElement){
            nextElement.focus();
          }
        }
      }
    }
  };

  useEffect(() => {
    setIsValidationPass(errorMessage === '');
  }, [errorMessage]);

  const handleInputFocus = (event: React.FocusEvent<HTMLDivElement>) => {
    if (event.type === 'blur'){
      event.currentTarget.style.borderBottomColor = HPHGreyDark;
    } else if (event.type === 'focus'){
      event.currentTarget.style.borderBottomColor = HPHSkyBlue;
    }
  };

  return (
        <OuterDiv id={id}>
            <StyledInputLabel>
              {label} {required ? <StyledRequired>*</StyledRequired> : ''}
            </StyledInputLabel>
            <InputDiv 
                isValid={isValidationPass} 
                backgroundMode={backgroundMode}
                disabled={disabled}
                onFocus={handleInputFocus} onBlur={handleInputFocus}
                style={{ width}}   
            >
                <ComInput data-testid={'com'} value={value.com} onChange={handleInputChange} name={'com'} maxLength={4} disabled={disabled} />
                <span>/</span>
                <VslInput data-testid={'vsl'} value={value.vsl} onChange={handleInputChange} name={'vsl'} maxLength={6} disabled={disabled} />
                <span>/</span>
                <VoyInput data-testid={'voy'} value={value.voy} onChange={handleInputChange} name={'voy'} maxLength={10} disabled={disabled} />
            </InputDiv>
            <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>           
        </OuterDiv>
  );
};
export { HPHInputMask };
