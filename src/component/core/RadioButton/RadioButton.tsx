import React from 'react';
import { StyledDiv, StyledRadio, StyledLabel, StyledBox } from '../styled/radio.styled';

export interface RadioProps{
  disabled?: boolean,
  orientation?: 'horizontal' | 'vertical',
  onChange?: (e: any) => void,
  checked?: any,
  id?:string | 'radio',
  options?:any[],
  value?: string,
  inputId?: any,
  key?: string,
  name?: string,
  [x:string]: any;
}

const HPHRadio: React.FC<RadioProps> = ({
  disabled, 
  onChange,  
  options = [],
  id = 'radio',
  checked,
  orientation = 'horizontal',
  ...rest
}): any => {
  return (
    <StyledBox orientation={orientation}>
    {
      options.map((option) => {
        return (
         <StyledDiv key={option.key} >
          <StyledRadio 
            id={id}
            inputId={option.inputId}     
            name="category" 
            value={option.name}
            checked = {option.name === checked}
            onChange={onChange}
            disabled={disabled}
            {...rest}>
            </StyledRadio>
          <StyledLabel disabled={disabled}>
            {option.name}
          </StyledLabel>
        </StyledDiv>
        );
      })
    }
    </StyledBox>
    
  );
};
export { HPHRadio } ;