
import { HPHRadio } from "./RadioButton";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
    id: 'radio',
    orientation: "horizontal",
    value: 'Option 1',
    disabled: false,
};

describe('Test', () => {
    test("Radio Button", () => {
        render(<HPHRadio/>);
    });

    test("Radio Button id", () => {
        render(<HPHRadio id={Props.id} />);
        const container = document.querySelector('#radio');
        expect(container).toBeNull();
    });

    test("Radio Button orientation", () => {
        render(<HPHRadio orientation={"horizontal"} />);
        const container = document.querySelector('#radio');
        expect(container).toBeNull();
    });

    test("Radio Button orientation", () => {
        render(<HPHRadio orientation={"vertical"} />);
        const container = document.querySelector('#radio');
        expect(container).toBeNull();
    });

    test("Radio Button value", () => {
        render(<HPHRadio value={Props.value} />);
        const container = document.querySelector('#radio');
        expect(container).toBeNull();
    });
    
    test("Radio Button is not disable", () => {
        render(<HPHRadio disabled={Props.disabled} />);
        const container = document.querySelector('#radio');
        expect(container).toBeNull();
    });

    test("Radio Button is disable", () => {
        render(<HPHRadio disabled={true} />);
        const container = document.querySelector('#radio');
        expect(container).toBeNull();
    });
});
