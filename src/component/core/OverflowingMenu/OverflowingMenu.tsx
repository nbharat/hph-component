import React from 'react';
import { StyledBox, StyledMenu } from '../styled/overflowingMenu.styled';

export interface OverflowingMenuProps {
  id?: string | 'overflow';
  menuItem?: any[],
  width?: number | string,
  height?: number | string,
  position?: | 'right' | 'left' | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight',
  paddingLeft?: string;
  paddingRight?: string;
  paddingTop?: string;
  paddingBottom?: string;
  [x:string]: any;
}

const HPHOverflowingMenu: React.FC<OverflowingMenuProps> = ({ 
  element,
  id = 'overflow',
  menuItem,
  height = 'auto',
  width = 'auto',
  position = 'right',
  paddingLeft = '0rem',
  paddingRight = '0rem',
  paddingTop = '0.938rem',
  paddingBottom = '0.938rem',

  ...rest }): any =>{

  return (
    <>
    <StyledBox>
        <StyledMenu 
            id={id}
            model={menuItem}
            position={position}
            width={width}
            height={height}
            paddingLeft={paddingLeft}
            paddingRight={paddingRight}
            paddingTop={paddingTop}
            paddingBottom={paddingBottom}
            {...rest} >
        </StyledMenu>
    </StyledBox>
    </>
  );
};

export { HPHOverflowingMenu } ;