
import { HPHOverflowingMenu } from "./OverflowingMenu";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'


const Props = {
  id:'overflow',
  width: '100px',
  height: '100px',
  paddingTop: '0.938rem',
  paddingBottom : '0.938rem',
  paddingLeft : '0rem',
  paddingRight : '0rem',
  position:'right',
};

describe('Test', () => {
    test("Overflowing Menu id", () => {
        render(<HPHOverflowingMenu />);
    });

    test("Overflowing Menu id", () => {
        render(<HPHOverflowingMenu id={Props.id} />);
        const container = document.querySelector('#overflow');
        expect(container).toBeNull();
    });
    test("Overflowing Menu width", () => {
        render(<HPHOverflowingMenu id={Props.id} width={Props.width}/>);
        const container = document.querySelector('#overflow');
        expect(container).toBeNull();
    });
    test("Overflowing Menu height", () => {
        render(<HPHOverflowingMenu id={Props.id} height={Props.height} />);
        const container = document.querySelector('#overflow');
        expect(container).toBeNull();
    });

    test("Overflowing Menu Padding Top", () => {
        render(<HPHOverflowingMenu id={Props.id} paddingTop={Props.paddingTop}/>);
        const container = document.querySelector('#overflow');
        expect(container).toBeNull();
    });

    test("Overflowing Menu Padding Bottom", () => {
        render(<HPHOverflowingMenu id={Props.id}  paddingBottom={Props.paddingBottom}/>);
        const container = document.querySelector('#overflow');
        expect(container).toBeNull();
    });
    test("Overflowing Menu Padding Left ", () => {
        render(<HPHOverflowingMenu id={Props.id} paddingLeft={Props.paddingLeft}/>);
        const container = document.querySelector('#overflow');
        expect(container).toBeNull();
    });
    test("Overflowing Menu Padding Right", () => {
        render(<HPHOverflowingMenu id={Props.id} paddingRight={Props.paddingRight}/>);
        const container = document.querySelector('#overflow');
        expect(container).toBeNull();
    });
 
});
