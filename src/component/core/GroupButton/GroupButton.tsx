import React from 'react';
import { StyledDiv, StyledButton, StyledInputErrorMsg, StyledLabel, StyledName, StyledWrapper, StyledIcon } from '../styled/groupButton.styled';

export interface GroupedButtonsProps{
  id?: string | 'GroupButton',
  label?:string | 'Group Button',  
  onChange?: (e: any) => void,
  value?: string,
  disabled?: boolean,
  showIcon?: boolean,
  list?: any[];
  icon?: string,
  multiple?: boolean;
  errorMessage?:any | '',
}

const GroupedButtons: React.FC<GroupedButtonsProps> = ({
  id = 'GroupButton',
  onChange,
  label = 'Group Button',
  value,
  list,
  showIcon = true,
  disabled = false,
  multiple = true,
  errorMessage = '',
}): any => {
  const justifyTemplate = (option) => {
    return (
        <StyledWrapper>
         {showIcon ? <StyledIcon icon={option.icon}/> : ''}
        <StyledName >{option.name}</StyledName>
        </StyledWrapper>
    );
  };
  return (
          <StyledDiv id={id}>
          <StyledLabel disabled={disabled}>{label}</StyledLabel>
          <StyledButton 
          value={value} 
          options={list} 
          itemTemplate={justifyTemplate}
          onChange={onChange}
          disabled={disabled}
          multiple={multiple}
          errorMessage={errorMessage}
          >
          </StyledButton>
          <StyledInputErrorMsg>{errorMessage}</StyledInputErrorMsg>
          </StyledDiv>
  );
};
export { GroupedButtons } ;