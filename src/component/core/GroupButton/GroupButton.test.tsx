import { GroupedButtons } from "./GroupButton";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom';

const Props = {
    id: "GroupButton",
    label: "Group Button",
    multiple: false,
    disabled: false,
    errormessage: 'button error message',
}

describe('Test', () => {
    test(" Group Button id", () => {
        render(<GroupedButtons />);
    });

    test(" Group Button id", () => {
        render(<GroupedButtons id={Props.id} />);
        const container = document.querySelector('#GroupButton');
        expect(container).toBeTruthy();
    });
    test(" Group Button Label", () => {
        render(<GroupedButtons id={Props.id} label={Props.label}/>);
        const container = document.querySelector('#GroupButton');
        expect(container).toBeTruthy();
    });
    test(" Group Button can be multiple select", () => {
        render(<GroupedButtons id={Props.id} multiple={Props.multiple}/>);
        const container = document.querySelector('#GroupButton');
        expect(container).toBeTruthy();
    });
    test(" Group Button is not disable", () => {
        render(<GroupedButtons id={Props.id}  disabled={Props.disabled}/>);
        const container = document.querySelector('#GroupButton');
        expect(container).toBeTruthy();
    });
    test(" Group Button error message", () => {
        render(<GroupedButtons id={Props.id}  errorMessage={''} />);
        const container = document.querySelector('#GroupButton');
        expect(container).toBeTruthy();
    });

});