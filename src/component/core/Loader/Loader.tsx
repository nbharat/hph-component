import * as React from 'react';
import { StyledDiv, StyleLoader, LoaderCircle, LoaderCircle2, LoaderCircle3, StyleSpinner} from '../styled/loader.styled'

export interface LoaderProps {
  id?: string | 'Loader',
  Indicator?: string | 'Spinner' | 'Stripe',
  size?: string | 'Small' | 'Medium' | 'Large',
  [x:string]: any;
}

const Loader: React.FC<LoaderProps> = ({
  id='Loader',
  className,  
  size = 'Small',
  Indicator = 'Stripe',
  ...rest
}) : any => {
  return (
    <>
    { Indicator == 'Stripe' ?
        <StyledDiv id={id} size={size}>        
            <StyleLoader >
                <LoaderCircle></LoaderCircle>
                <LoaderCircle2></LoaderCircle2>
                <LoaderCircle3></LoaderCircle3>
            </StyleLoader>          
        </StyledDiv>
        :
        <StyledDiv id={id} size={size}>
            <StyleSpinner></StyleSpinner>
        </StyledDiv>          
      }
    </>
  );
};

export { Loader };
