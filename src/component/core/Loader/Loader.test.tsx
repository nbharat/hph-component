import { Loader } from '../Loader/Loader'
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
    id: "Loader",
    Indicator:"Stripe",
}

describe('Test', () => {
    test(" Loader id", () => {
        render(<Loader />);
    });

    test(" Loader id", () => {
        render(<Loader id={Props.id} />);
        const container = document.querySelector('#Loader');
        expect(container).toBeTruthy();
    });
    test(" Loader Indicator", () => {
        render(<Loader id={Props.id} Indicator={Props.Indicator}/>);
        const container = document.querySelector('#Loader');
        expect(container).toBeTruthy();
    });
});
