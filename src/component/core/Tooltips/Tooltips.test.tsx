import { Tooltips } from "./Tooltips";
import { render } from '@testing-library/react'
import React from "react";
import '@testing-library/jest-dom';
import '@testing-library/jest-dom'

const Props = {
    toolTiptext:"toolTip",
    id : "toolTip",
    className:"iconTooltip",
    toolTipArrow:true

};
describe('Test', () => {
    test("toolTip", () => {
        render(<Tooltips />);
    });
    test("toolTip", () => {
        render(<Tooltips size="small" sizetooltipplacement="topRight" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="small" sizetooltipplacement="topLeft" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="small" sizetooltipplacement="bottomRight" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="small" sizetooltipplacement="bottomLeft" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" sizetooltipplacement="topRight" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" sizetooltipplacement="topLeft" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" sizetooltipplacement="bottomRight" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" sizetooltipplacement="bottomLeft" />);
    });

    test("toolTip", () => {
        render(<Tooltips size="small" toolTipPlacement="left" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" toolTipPlacement="left" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="large" toolTipPlacement="left" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="small" toolTipPlacement="right" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" toolTipPlacement="right" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="large" toolTipPlacement="right" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="small" toolTipPlacement="bottom" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" toolTipPlacement="bottom" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="large" toolTipPlacement="bottom" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="small" toolTipPlacement="top" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="medium" toolTipPlacement="top" />);
    });
    test("toolTip", () => {
        render(<Tooltips size="large" toolTipPlacement="top" />);
    });

    test("toolTip", () => {
        render(<Tooltips 
            className={Props.className} id={Props.id} />);
        const container = document.querySelector('#toolTip');
       
        if (container) {            
            expect(container).toHaveClass("iconTooltip");
        }
    });
    test("toolTiptext", () => {
        render(<Tooltips 
            toolTiptext={Props.toolTiptext} id={Props.id} />);
        const container = document.querySelector('#toolTip');
      
        if (container) {            
            expect(container).toHaveTextContent("toolTip");
        }
    });
   
});

