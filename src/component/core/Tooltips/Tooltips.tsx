import * as React from 'react';
import { StyledTooltip, sizetoolTipPlacementObj } from './../styled/tooltips.styled';

export interface TooltipsProps {
  id?: string,
  toolTiptext?: string,
  className?: string,
  toolTipArrow?: boolean,
  [x:string]: any;
}


const Tooltips: React.FC<TooltipsProps> = ({
  toolTiptext = 'toolTip',
  toolTipPlacement = 'bottom',
  id,
  size = 'medium',    
  toolTipArrow = true,
  ...rest
}) => {
  return (
        <>
            <StyledTooltip
            toolTipArrow={toolTipArrow}
            size={size}
            toolTipPlacement={toolTipPlacement}
            sizetooltipplacement={sizetoolTipPlacementObj[size + toolTipPlacement]}
            {...rest}
            >
              <span id={id} {...rest}>{toolTiptext}</span>
            </StyledTooltip>
        </>
  );
};
export { Tooltips };